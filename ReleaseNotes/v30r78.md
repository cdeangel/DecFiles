DecFiles v30r78 2022-08-31 
==========================  
 
! 2022-08-28 - Xinchen Dai (MR !1117)  
   Add new decay file  
   + 15204015 : Lb_pKpipi=tightCut  
  
! 2022-08-25 - J Alexander Ward (MR !1116)  
   Modify decay file  
   + 13114016 : Bs_pipimumu=FOURBODYPHSP,DecProdCut  
  
! 2022-08-24 - Qundong Han (MR !1115)  
   Add 4 new decay files  
   + 11104540 : Bd_Xi0Xi0,Lambdapi0=DecProdCut  
   + 11106120 : Bd_XipXim,Lambda0pi=DecProdCut  
   + 13104170 : Bs_LambdaLambda,ppi=DecProdCut  
   + 13104530 : Bs_Xi0Xi0,Lambdapi0=DecProdCut  
  
! 2022-08-09 - Baasansuren Batsukh (MR !1113)  
   Add new decay file  
   + 12197205 : Bu_LcXicprimePi,pKPi,Xicgamma=PHSP,DecProdCut  
  
