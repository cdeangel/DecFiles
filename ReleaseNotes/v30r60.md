DecFiles v30r60 2021-07-07 
==========================  
 
! 2021-07-07 - Maximilien Chefdeville (MR !783)  
   Add 5 new decay files  
   + 11142421 : Bd_Jpsieta,mm,gg=DecProdCut  
   + 11144220 : Bd_Jpsieta,mm,pipig=DecProdCut  
   + 11144460 : Bd_Jpsietap,mm,etapipi=DecProdCut  
   + 11144210 : Bd_Jpsietap,mm,rhog=DecProdCut  
   + 13144220 : Bs_Jpsieta,mm,pipig=DecProdCut  
  
! 2021-07-02 - Titus Mombacher (MR !779)  
   Add 3 new decay files  
   + 14553023 : Bc_Jpsienu,ee=BcVegPy,ffEbert,DiLeptonInAcc,M3.5GeV  
   + 13116010 : Bs_6mu=PHSP,DecProdCut  
   + 13118000 : Bs_8mu=PHSP,DecProdCut  
  
! 2021-06-30 - Adlene Hicheur (MR !778)  
   Add new decay file  
   + 13164252 : Bs_Ds2460pi,Dsstgamma,Dsgamma,KKpi=DecProdCut  
  
! 2021-06-28 - Albert Bursche (MR !776)  
   Add new decay file  
   + 30122002 : exclu_ee,gg=coherent_starlight_with_cuts  
  
! 2021-06-22 - Daniel Joachim Unverzagt (MR !774)  
   Add 3 new decay files  
   + 27165075 : Dst_D0pi,KKpipi=DecProdCut,GenCut  
   + 27165076 : Dst_D0pi,Kpipipi=DecProdCut,GenCut  
   + 27165074 : Dst_D0pi,pipipipi=DecProdCut,GenCut  
  
! 2021-06-21 - Steven R. Blusk (MR !773)  
   Modify 4 decay files  
   + 16467004 : XibStar6360_LbK,Lc3pi-MaxWidth100MeV=TightCut  
   + 16465004 : XibStar6360_LbK,Lcpi-MaxWidth100MeV=TightCut  
   + 16467003 : XibStar6450_LbK,Lc3pi-MaxWidth100MeV=TightCut  
   + 16465003 : XibStar6450_LbK,Lcpi-MaxWidth100MeV=TightCut  
  
! 2021-06-08 - Qiuchan Lu (MR !770)  
   Add new decay file  
   + 11166003 : Bd_Lambdacpipip,Sigmacpi,pKpi=TightCut  
  
! 2021-06-07 - La Wang (MR !769)  
   Modify 2 decay files  
   + 11166030 : Bd_Lcpbarpbarp,TightCut  
   + 11166081 : Bd_Lcpipip,TightCut  
  
! 2021-06-01 - Adrian Casais Vidal (MR !767)  
   Add new decay file  
   + 39112231 : eta_mumugamma=TightCut,gamma  
  
! 2021-05-31 - John Leslie Cobbledick (MR !766)  
   Add new decay file  
   + 27163475 : Dst_D0pi,KSpipi=neut_ks_dec,DecProdCut  
  
! 2021-05-28 - Nathan Philip Jurik (MR !765)  
   Add new decay file  
   + 23103063 : Ds+_pi-pi+K+=phsp,TightCut  
  
! 2021-05-27 - Raul Rabadan (MR !763)  
   Add 3 new decay files  
   + 11146035 : Bd_JpsiphiKpi,mmKK,WithMinP=DecProdCut  
   + 11146032 : Bd_JpsiphiKst,KKmumuKpi,WithMinP=DecProdCut  
   + 12145055 : Bu_Jpsiphipi,mmKK,WithMinP=DecProdCut  
  
! 2021-05-25 - Hanae Tilquin (MR !757)  
   Add new decay file  
   + 13694052 : Bs_DD,Kmunu,KmunuCocktail=TightCut  
  
! 2020-07-30 - Albert Bursche (MR !560)  
   Add new decay file  
   + 30100202 : exclu_axion,gg=coherent_starlight  
  
