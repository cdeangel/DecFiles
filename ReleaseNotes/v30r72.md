DecFiles v30r72 2022-04-12 
==========================  
 
! 2022-04-12 - Mark Smith (MR !998)  
   Add 2 new decay files  
   + 11574002 : Bd_D0pimunu,Kmunu=TightCut  
   + 11574003 : Bd_Dmmunu,Kstmunu=TightCut  
  
! 2022-04-11 - Ziyi Wang (MR !997)  
   Add new decay file  
   + 26104986 : Xic0_OmegamKp,L0K,ppi=phsp,DecProdCut  
  
! 2022-04-08 - Richard Morgan Williams (MR !996)  
   Add new decay file  
   + 12123499 : Bu_phiKst,ee=DecProdCut  
  
! 2022-04-05 - J Alexander Ward (MR !995)  
   Add 2 new decay files  
   + 11114037 : Bd_pipimumu=FOURBODYPHSP,DecProdCut  
   + 13114015 : Bs_pipimumu=FOURBODYPHSP,DecProdCut  
  
! 2022-03-18 - Hanae Tilquin (MR !994)  
   Add 5 new decay files  
   + 13584063 : Bs_D0Kenu,Kenu=DecProdCut  
   + 13574063 : Bs_D0Klnu,Klnu=DecProdCut  
   + 13574064 : Bs_D0Kmunu,Kmunu=DecProdCut  
   + 13524042 : Bs_KKtautau,ee=DecProdCut  
   + 13514063 : Bs_KKtautau,mue=DecProdCut  
  
! 2022-03-15 - Yangjie Su (MR !993)  
   Add 4 new decay files  
   + 15164033 : Lb_LcK,pKK=PHSP,DecProdCut  
   + 15164032 : Lb_LcK,ppipi=PHSP,DecProdCut  
   + 15164003 : Lb_Lcpi,pKK=PHSP,DecProdCut  
   + 15164002 : Lb_Lcpi,ppipi=PHSP,DecProdCut  
  
! 2022-03-07 - Michel De Cian (MR !985)  
   Add 5 new decay files  
   + 13874006 : Bs_Dsmunu,cocktail,PiPiX=TightCut,ForB2RhoMuNu  
   + 12513004 : Bu_f0_980munu,PiPi=TightCut,ISGW2  
   + 12813001 : Bu_f2_1270munu,PiPiX=TightCut,ISGW2  
   + 12513003 : Bu_rho1450munu,PiPi=TightCut,ISGW2  
   + 15874005 : Lb_Lcmunu,cocktail,PiPiX=TightCut,ForB2RhoMuNu  
  
! 2022-01-11 - Hanae Tilquin (MR !951)  
   Add new decay file  
   + 15875062 : Lb_LambdacXmunuCocktail=KpimumuInAcc  
  
! 2021-12-21 - Hanae Tilquin (MR !947)  
   Add 5 new decay files  
   + 11574442 : Bd_D0pimunu,KmunuCocktail=KpimumuInAcc  
   + 11696452 : Bd_DDK,Kpimunu,KpimunuCocktail=KpimumuInAcc  
   + 11696453 : Bd_DDK,Kpimunu,munuCocktail=KpimumuInAcc  
   + 11574052 : Bd_Dmunu,KpimunuCocktail=KpimumuInAcc  
   + 13574065 : Bs_D0Kmunu,pimunu=KpimumuInAcc  
  
