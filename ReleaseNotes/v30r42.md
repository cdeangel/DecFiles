!========================= DecFiles v30r42 2020-02-04 =======================  
  
! 2020-02-03 - Timothy David Evans (MR !410, MR !411)  
   Add 4 new decay files  
   + 12165042 : Bu_D0K,Kpipipi=DecProdCut,AmpGen  
   + 12165043 : Bu_D0K,piKpipi=DecProdCut,AmpGen  
   + 12165054 : Bu_D0pi,Kpipipi=DecProdCut,AmpGen  
   + 12165053 : Bu_D0pi,piKpipi=DecProdCut,AmpGen  
  
! 2020-01-29 - Chishuai Wang (MR !409)  
   Update 6 new decay files  
   + 21173001 : D+_phipi,mm=DecProdCut  
   + 21513016 : D+_taunu,muphi=FromB  
   + 21513017 : D+_taunu,muphi=FromD  
   + 23513026 : Ds_taunu,muphi=FromB  
   + 23513027 : Ds_taunu,muphi=FromD  
   + 31113046 : tau_muphi,KK=FromB  
   Declare event types 21513006, 21513007, 23513006, 23513007 and 31113045 obsolete  
  
! 2020-01-29 - Tommaso Pajero (MR !408)  
   Add 4 new decay files  
   + 11774005 : Bd_DstX,cocktail,D0pi,KK=TightCut  
   + 11774006 : Bd_DstX,cocktail,D0pi,pipi=TightCut  
   + 12365402 : Bu_DstX,cocktail,D0pi,KK=TightCut  
   + 12365403 : Bu_DstX,cocktail,D0pi,pipi=TightCut  
  
! 2020-01-10 - Serena Maccolini (MR !407)  
   Update cuts in the decay files  
   + 21103011 : D+_K-pi+pi+=res,TightCut,ACPKKCuts  
   + 21103101 : D+_Kspi+=phsp,TightCut,ACPKKCuts    
  
! 2019-12-19 - Mark Smith (MR !406)  
   Update cuts in  decay files  
   + 11876005 : Bd_D0ppbarX,Xmunu=TightCut  
   + 12775004 : Bu_D+ppbarX,Xmunu=TightCut  
   + 12813412 : Bu_Delpbarmunu,pX=TightCutpQCD  
   + 12875412 : Bu_Lcpbarmunu,pX=TightCut2  
   + 12877401 : Bu_Lcpipipbarmunu,pX=TightCut2  
   + 12813402 : Bu_pNstmunu,pX=TightCutpQCD  
   Add new decay file  
   + 12572001 : Bu_D0ppmunu=TightCut2  
  
