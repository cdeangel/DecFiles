2020-12-18 DecFiles v30r54
========================== 
  
! 2020-12-17 - Hang Yin (MR !638)  
   Add 5 new decay files  
   + 26575150 : Omegacc_Omegac0munu,Omegapi,LambdaK=GenXicc,DecProdCut  
   + 26576150 : Omegaccc_Omegaccmunu,Omegac0munu,Omegapi,LambdaK=GenXicc,DecProdCut  
   + 16675061 : Xibc+_Lbpi,Lcmunu,pKpi=DecProdCut  
   + 16676062 : Xibc_LbKpi,Lcmunu,pKpi=DecProdCut  
   + 26674060 : Xicc++_Xicmunu,pKpi-res=GenXicc,DecProdCut,t=256fs,WithMinPT,MinDaughterPT200  
  
! 2020-12-16 - Marian Stahl (MR !637, !640)  
   Modify 6 decay files  
   + 25105196 : Lc_XiKpi=AMPGEN,TightCut  
   + 26104187 : Omegac_Omegapi=HELAMP,TightCut  
   + 26106182 : Xic0_OmegaKpi-pi+=HELAMP,TightCut  
   + 26104186 : Xic0_Xi-pi+=HELAMP,TightCut  
   + 26105196 : Xic_XiKpi=AMPGEN,TightCut  
   + 26105195 : Xic_Xipipi=AMPGEN,TightCut  
  
! 2020-12-15 - Andrew George Morris (MR !636)  
   Add 2 new decay files  
   + 11164096 : Bd_Dst-pi+,D0pi,KK=DecProdCut  
   + 11164097 : Bd_Dst-pi+,D0pi,pipi=DecProdCut  
  
! 2020-12-15 - Yuya Shimizu (MR !635)  
   Add 20 new decay files  
   + 11166120 : Bd_D0KK,KSKK=BsqDalitz,DDalitz,TightCut  
   + 11166112 : Bd_D0Kpi,KSKK=BsqDalitz,DDalitz,TightCut  
   + 11166118 : Bd_D0pipi,KSKK=BsqDalitz,DDalitz,TightCut  
   + 11166335 : Bd_Dst0KK,D0gamma,KSKK=BsqDalitz,DDalitz,TightCut  
   + 11166535 : Bd_Dst0KK,D0pi0,KSKK=BsqDalitz,DDalitz,TightCut  
   + 11166331 : Bd_Dst0Kpi,D0gamma,KSKK=BsqDalitz,DDalitz,TightCut  
   + 11166531 : Bd_Dst0Kpi,D0pi0,KSKK=BsqDalitz,DDalitz,TightCut  
   + 11166333 : Bd_Dst0pipi,D0gamma,KSKK=BsqDalitz,DDalitz,TightCut  
   + 11166533 : Bd_Dst0pipi,D0pi0,KSKK=BsqDalitz,DDalitz,TightCut  
   + 13166120 : Bs_D0KK,KSKK=BssqDalitz,DDalitz,TightCut  
   + 13166335 : Bs_Dst0KK,D0gamma,KSKK=BssqDalitz,DDalitz,TightCut  
   + 13166535 : Bs_Dst0KK,D0pi0,KSKK=BssqDalitz,DDalitz,TightCut  
   + 13166331 : Bs_Dst0Kpi,D0gamma,KSKK=BssqDalitz,DDalitz,TightCut  
   + 13166531 : Bs_Dst0Kpi,D0pi0,KSKK=BssqDalitz,DDalitz,TightCut  
   + 15166120 : Lb_D0pK,KSKK=LbsqDalitz,DDalitz,TightCut  
   + 15166118 : Lb_D0ppi,KSKK=LbsqDalitz,DDalitz,TightCut  
   + 15166320 : Lb_Dst0pK,D0gamma,KSKK=LbsqDalitz,DDalitz,TightCut  
   + 15166520 : Lb_Dst0pK,D0pi0,KSKK=LbsqDalitz,DDalitz,TightCut  
   + 15166318 : Lb_Dst0ppi,D0gamma,KSKK=LbsqDalitz,DDalitz,TightCut  
   + 15166518 : Lb_Dst0ppi,D0pi0,KSKK=LbsqDalitz,DDalitz,TightCut  
  
! 2020-12-11 - Thomas Edward Latham (MR !634)  
   Add 4 new decay files  
   + 14165230 : Bc_Bdstarpi+,Bdgamma,Dpi=BcVegPy,DecProdCut  
   + 14145204 : Bc_Bdstarpi+,Bdgamma,JpsiKstar=BcVegPy,DecProdCut  
   + 14265200 : Bc_Bsstarpi+,Bsgamma,Dspi=BcVegPy,DecProdCut  
   + 14145200 : Bc_Bsstarpi+,Bsgamma,Jpsiphi=BcVegPy,DecProdCut  
  
! 2020-12-09 - Lorenzo Capriotti (MR !633)  
   Add new decay file  
   + 12145451 : Bu_psi2SKst,Jpsipipi,mm=TightCut  
  
! 2020-12-09 - Jan Langer (MR !632)  
   Add 4 new decay files  
   + 11196086 : Bd_Dst-Dst+,D0pi-,Kpi=DecProdCut,HELAMP010  
   + 11196085 : Bd_Dst-Dst+,D0pi-,Kpi=DecProdCut,HELAMP100  
   + 13196052 : Bs_Dst-Dst+,D0pi-,Kpi=DecProdCut,HELAMP010  
   + 13196051 : Bs_Dst-Dst+,D0pi-,Kpi=DecProdCut,HELAMP100  
  
! 2020-12-09 - Daniel Johnson (MR !631)  
   Add 4 new decay files  
   + 12199100 : Bu_D0DKS,K3Pi,KPiPi,PiPi=DecProdCut  
   + 12199120 : Bu_D0DKS,K3Pi,KPiPi,PiPi=sqDalitz,DecProdCut  
   + 12199130 : Bu_D0DKS,K3Pi,KPiPi,PiPi=sqDalitz13,DecProdCut  
   + 12199140 : Bu_D0DKS,K3Pi,KPiPi,PiPi=sqDalitz23,DecProdCut  
  
! 2020-12-08 - Yuya Shimizu (MR !630)  
   Add 20 new decay files  
   + 11166077 : Bd_D0KK,Kpipipi=BsqDalitz,DAmpGen,TightCut  
   + 11166071 : Bd_D0Kpi,Kpipipi=BsqDalitz,DAmpGen,TightCut  
   + 11166074 : Bd_D0pipi,Kpipipi=BsqDalitz,DAmpGen,TightCut  
   + 11166277 : Bd_Dst0KK,D0gamma,Kpipipi=BsqDalitz,DAmpGen,TightCut  
   + 11166477 : Bd_Dst0KK,D0pi0,Kpipipi=BsqDalitz,DAmpGen,TightCut  
   + 11166271 : Bd_Dst0Kpi,D0gamma,Kpipipi=BsqDalitz,DAmpGen,TightCut  
   + 11166471 : Bd_Dst0Kpi,D0pi0,Kpipipi=BsqDalitz,DAmpGen,TightCut  
   + 11166274 : Bd_Dst0pipi,D0gamma,Kpipipi=BsqDalitz,DAmpGen,TightCut  
   + 11166474 : Bd_Dst0pipi,D0pi0,Kpipipi=BsqDalitz,DAmpGen,TightCut  
   + 13166077 : Bs_D0KK,Kpipipi=BssqDalitz,DAmpGen,TightCut  
   + 13166277 : Bs_Dst0KK,D0gamma,Kpipipi=BssqDalitz,DAmpGen,TightCut  
   + 13166477 : Bs_Dst0KK,D0pi0,Kpipipi=BssqDalitz,DAmpGen,TightCut  
   + 13166271 : Bs_Dst0Kpi,D0gamma,Kpipipi=BssqDalitz,DAmpGen,TightCut  
   + 13166471 : Bs_Dst0Kpi,D0pi0,Kpipipi=BssqDalitz,DAmpGen,TightCut  
   + 15166077 : Lb_D0pK,Kpipipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166074 : Lb_D0ppi,Kpipipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166277 : Lb_Dst0pK,D0gamma,Kpipipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166477 : Lb_Dst0pK,D0pi0,Kpipipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166274 : Lb_Dst0ppi,D0gamma,Kpipipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166474 : Lb_Dst0ppi,D0pi0,Kpipipi=LbsqDalitz,DAmpGen,TightCut  
  
! 2020-12-08 - Yuya Shimizu (MR !629)  
   Add 20 new decay files  
   + 11166078 : Bd_D0KK,piKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166072 : Bd_D0Kpi,piKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166075 : Bd_D0pipi,piKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166278 : Bd_Dst0KK,D0gamma,piKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166478 : Bd_Dst0KK,D0pi0,piKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166272 : Bd_Dst0Kpi,D0gamma,piKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166472 : Bd_Dst0Kpi,D0pi0,piKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166275 : Bd_Dst0pipi,D0gamma,piKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166475 : Bd_Dst0pipi,D0pi0,piKpipi=BsqDalitz,DAmpGen,TightCut  
   + 13166078 : Bs_D0KK,piKpipi=BssqDalitz,DAmpGen,TightCut  
   + 13166278 : Bs_Dst0KK,D0gamma,piKpipi=BssqDalitz,DAmpGen,TightCut  
   + 13166478 : Bs_Dst0KK,D0pi0,piKpipi=BssqDalitz,DAmpGen,TightCut  
   + 13166272 : Bs_Dst0Kpi,D0gamma,piKpipi=BssqDalitz,DAmpGen,TightCut  
   + 13166472 : Bs_Dst0Kpi,D0pi0,piKpipi=BssqDalitz,DAmpGen,TightCut  
   + 15166078 : Lb_D0pK,piKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166075 : Lb_D0ppi,piKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166278 : Lb_Dst0pK,D0gamma,piKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166478 : Lb_Dst0pK,D0pi0,piKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166275 : Lb_Dst0ppi,D0gamma,piKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166475 : Lb_Dst0ppi,D0pi0,piKpipi=LbsqDalitz,DAmpGen,TightCut  
  
! 2020-12-08 - Yuya Shimizu (MR !628)  
   Add 20 new decay files  
   + 11166119 : Bd_D0KK,KSpipi=BsqDalitz,DDalitz,TightCut  
   + 11166111 : Bd_D0Kpi,KSpipi=BsqDalitz,DDalitz,TightCut  
   + 11166117 : Bd_D0pipi,KSpipi=BsqDalitz,DDalitz,TightCut  
   + 11166334 : Bd_Dst0KK,D0gamma,KSpipi=BsqDalitz,DDalitz,TightCut  
   + 11166534 : Bd_Dst0KK,D0pi0,KSpipi=BsqDalitz,DDalitz,TightCut  
   + 11166330 : Bd_Dst0Kpi,D0gamma,KSpipi=BsqDalitz,DDalitz,TightCut  
   + 11166530 : Bd_Dst0Kpi,D0pi0,KSpipi=BsqDalitz,DDalitz,TightCut  
   + 11166332 : Bd_Dst0pipi,D0gamma,KSpipi=BsqDalitz,DDalitz,TightCut  
   + 11166532 : Bd_Dst0pipi,D0pi0,KSpipi=BsqDalitz,DDalitz,TightCut  
   + 13166119 : Bs_D0KK,KSpipi=BssqDalitz,DDalitz,TightCut  
   + 13166334 : Bs_Dst0KK,D0gamma,KSpipi=BssqDalitz,DDalitz,TightCut  
   + 13166534 : Bs_Dst0KK,D0pi0,KSpipi=BssqDalitz,DDalitz,TightCut  
   + 13166330 : Bs_Dst0Kpi,D0gamma,KSpipi=BssqDalitz,DDalitz,TightCut  
   + 13166530 : Bs_Dst0Kpi,D0pi0,KSpipi=BssqDalitz,DDalitz,TightCut  
   + 15166119 : Lb_D0pK,KSpipi=LbsqDalitz,DDalitz,TightCut  
   + 15166117 : Lb_D0ppi,KSpipi=LbsqDalitz,DDalitz,TightCut  
   + 15166319 : Lb_Dst0pK,D0gamma,KSpipi=LbsqDalitz,DDalitz,TightCut  
   + 15166519 : Lb_Dst0pK,D0pi0,KSpipi=LbsqDalitz,DDalitz,TightCut  
   + 15166317 : Lb_Dst0ppi,D0gamma,KSpipi=LbsqDalitz,DDalitz,TightCut  
   + 15166517 : Lb_Dst0ppi,D0pi0,KSpipi=LbsqDalitz,DDalitz,TightCut  
  
! 2020-12-08 - Yuya Shimizu (MR !627)  
   Add 20 new decay files  
   + 11166079 : Bd_D0KK,pipipipi=BsqDalitz,DPHSP,TightCut  
   + 11166073 : Bd_D0Kpi,pipipipi=BsqDalitz,DPHSP,TightCut  
   + 11166076 : Bd_D0pipi,pipipipi=BsqDalitz,DPHSP,TightCut  
   + 11166279 : Bd_Dst0KK,D0gamma,pipipipi=BsqDalitz,DPHSP,TightCut  
   + 11166479 : Bd_Dst0KK,D0pi0,pipipipi=BsqDalitz,DPHSP,TightCut  
   + 11166273 : Bd_Dst0Kpi,D0gamma,pipipipi=BsqDalitz,DPHSP,TightCut  
   + 11166473 : Bd_Dst0Kpi,D0pi0,pipipipi=BsqDalitz,DPHSP,TightCut  
   + 11166276 : Bd_Dst0pipi,D0gamma,pipipipi=BsqDalitz,DPHSP,TightCut  
   + 11166476 : Bd_Dst0pipi,D0pi0,pipipipi=BsqDalitz,DPHSP,TightCut  
   + 13166079 : Bs_D0KK,pipipipi=BssqDalitz,DPHSP,TightCut  
   + 13166279 : Bs_Dst0KK,D0gamma,pipipipi=BssqDalitz,DPHSP,TightCut  
   + 13166479 : Bs_Dst0KK,D0pi0,pipipipi=BssqDalitz,DPHSP,TightCut  
   + 13166273 : Bs_Dst0Kpi,D0gamma,pipipipi=BssqDalitz,DPHSP,TightCut  
   + 13166473 : Bs_Dst0Kpi,D0pi0,pipipipi=BssqDalitz,DPHSP,TightCut  
   + 15166079 : Lb_D0pK,pipipipi=LbsqDalitz,DPHSP,TightCut  
   + 15166076 : Lb_D0ppi,pipipipi=LbsqDalitz,DPHSP,TightCut  
   + 15166279 : Lb_Dst0pK,D0gamma,pipipipi=LbsqDalitz,DPHSP,TightCut  
   + 15166479 : Lb_Dst0pK,D0pi0,pipipipi=LbsqDalitz,DPHSP,TightCut  
   + 15166276 : Lb_Dst0ppi,D0gamma,pipipipi=LbsqDalitz,DPHSP,TightCut  
   + 15166476 : Lb_Dst0ppi,D0pi0,pipipipi=LbsqDalitz,DPHSP,TightCut  
  
! 2020-12-08 - Yiduo Shang (MR !624)  
   Add new decay file  
   + 16296040 : Xib0_Xic+Ds,pKpi,KKpi=DecProdCut  
  
! 2020-12-05 - Michel De Cian (MR !619)  
   Add 5 new decay files  
   + 12513404 : Bu_etamunu,pipiX=TightCut,ISGW2  
   + 12813405 : Bu_etaprimemunu,pipiX=TightCut,ISGW2  
   + 12813403 : Bu_omegamunu,pipiX=TightCut,BCL  
   + 12511402 : Bu_pi0munu=TightCut,BCL  
   + 12513002 : Bu_rhomunu=TightCut,BCL  
  
! 2020-11-25 - Michael Kent Wilkinson (MR !614)  
   Add 7 new decay files  
   + 11876200 : Bd_DsstDst,gDsgD,KKpimunuX=cocktail,mu3hInAcc  
   + 13874200 : Bs_Dsmunu,KKpi=cocktail,hqet2,mu3hInAcc  
   + 13874201 : Bs_DsstDsst,gDsgDs,KKpimunuX=cocktail,mu3hInAcc  
   + 12875603 : Bu_DsstDst,gDsgD0,KKpimunuX=cocktail,mu3hInAcc  
   + 23103005 : Ds_KKpi=res,FromB  
   + 23103006 : Ds_KKpi=res,NotFromB  
   + 15874300 : Lb_DsstLc,DsmunuX=cocktail,mu3hInAcc  
   Modify 2 decay files  
   + 11874200 : Bd_Dstp2400munu,DsKS0,KKpi=mu3hInAcc  
   + 12875000 : Bu_Dst02400munu,DsK,KKpi=mu3hInAcc  
  
! 2020-09-12 - Hang Yin (MR !575)  
   Add 8 new decay files  
   + 40124000 : Higgs_ZZ_eeee  
   + 40124010 : Higgs_ZZ_mumuee  
   + 40114050 : Higgs_ZZ_mumumumu  
   + 42124000 : ZZ_eeee  
   + 42124010 : ZZ_mumuee  
   + 42114000 : ZZ_mumumumu  
   + 42122005 : Z_ee_PowHeg40GeV  
   + 42112005 : Z_mumu_PowHeg40GeV  
  
