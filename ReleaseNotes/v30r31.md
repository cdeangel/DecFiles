  
!========================= DecFiles v30r31 2019-06-19 =======================  
  
! 2019-06-12 - Victor Daussy-Renaudin (MR !303)  
   New decay files  
   + 11166311 : Bd_Dst0rho0,D0gamma,KSKK=TightCut,NoNeutralCut.dec  
   + 11166321 : Bd_Dst0rho0,D0gamma,KSpipi=TightCut,NoNeutralCut.dec  
   + 11166710 : Bd_Dst0rho0,D0pi0,KSKK=TightCut,NoNeutralCut.dec  
   + 11166720 : Bd_Dst0rho0,D0pi0,KSpipi=TightCut,NoNeutralCut.dec  
   + 12165322 : Bu_Dst0K,D0gamma,KSKK=TightCut,NoNeutralCut.dec  
   + 12165332 : Bu_Dst0K,D0gamma,KSpipi=TightCut,NoNeutralCut.dec  
   + 12165760 : Bu_Dst0K,D0pi0,KSKK=TightCut,NoNeutralCut.dec  
   + 12165761 : Bu_Dst0K,D0pi0,KSpipi=TightCut,NoNeutralCut.dec  
   + 12165342 : Bu_Dst0pi,D0gamma,KSKK=TightCut,NoNeutralCut.dec  
   + 12165352 : Bu_Dst0pi,D0gamma,KSpipi=TightCut,NoNeutralCut.dec  
   + 12165762 : Bu_Dst0pi,D0pi0,KSKK=TightCut,NoNeutralCut.dec  
   + 12165763 : Bu_Dst0pi,D0pi0,KSpipi=TightCut,NoNeutralCut.dec  
   + 12165702 : Bu_Dst0rho+,D0gamma,KSKK=TightCut,NoNeutralCut.dec  
   + 12165711 : Bu_Dst0rho+,D0gamma,KSpipi=TightCut,NoNeutralCut.dec  
   + 12165764 : Bu_Dst0rho+,D0pi0,KSKK=TightCut,NoNeutralCut.dec  
   + 12165766 : Bu_Dst0rho+,D0pi0,KSpipi=TightCut,NoNeutralCut.dec  
  
! 2019-06-17 - Kyung Eun Kim (MR !304)  
   Add decay file  
   + 16876010 : Xibst0_Xibpi,Xicmunu=cocktail,pKpi=TightCut  
  
! 2019-06-04 - Mikkel Bjorn (MR !296)  
   Add new dec files  
   + 12165599: Bu_D0Kst+,KSpipi,Kpi0=DecProdCut  
   + 12165321: Bu_Dst0K,D0gamma,KSKK=TightCut,LooserCuts  
   + 12165331: Bu_Dst0K,D0gamma,KSpipi=TightCut,LooserCuts  
   + 12165341: Bu_Dst0pi,D0gamma,KSKK=TightCut,LooserCuts  
   + 12165351: Bu_Dst0pi,D0gamma,KSpipi=TightCut,LooserCuts  
   + 12165565: Bu_Dst0K,D0pi0,KSKK=TightCut,LooserCuts  
   + 12165567: Bu_Dst0K,D0pi0,KSpipi=TightCut,LooserCuts  
   + 12165568: Bu_Dst0pi,D0pi0,KSKK=TightCut,LooserCuts  
   + 12165569: Bu_Dst0pi,D0pi0,KSpipi=TightCut,LooserCut  
  
! 2019-06-12 - Liupan An (MR !302)  
   Add 2 decfiles:  
   + 17163211: Bst_Bugamma,Dpi,Kpi=DecProdCut.dec  
   + 17143211: Bst_Bugamma,JpsiK,mm=DecProdCut.dec  
  
! 2019-06-07 - Carla Marin (MR !300)  
   Add decfile  
   + 13102601 : Bs_f1420gamma,KKpi0=HighPtGamma,DecProdCut.dec  
  
! 2019-06-12 - Mark Whitehead (MR !301)  
   Add new decay file  
   + 12573080 : Bu_D0pi,pimunu=DecProdCut.dec  
  
! 2019-06-06 - Raul Rabadan (MR !299)  
   Add new decfile  
   + 12297080 : Bu_DstDspi,D0Pi,KKPi,Dpi,nrDs=DecProdCut,WithMinP3.dec   
  
! 2019-06-05 Adam Morris  
   Modify 4 decfiles:  
   + 11896612 : Bd_DstXc,Xc2hhhNneutrals,upto5prongs=DecProdCut.dec  
   + 11996412 : Bd_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut.dec  
   + 12997612 : B+_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut.dec  
   + 13996612 : Bs_DstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut.dec  
   Looser cuts in line with Run 2 stripping  
   Remove Myrho and My{eta,etap,omega,phi}gg definitions as being redundant with particles in DECAY.DEC  
   Fix B0 -> D_2*- Ds X branching fractions in 11996412  
   Remove 4 decfiles:  
   - 11896611 : Bd_DstXc,Xc2hhhNneutrals,upto5prongs=DecProdCut,TightCut.dec  
   - 11996413 : Bd_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut,TightCut.dec  
   - 12997613 : B+_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut,TightCut.dec  
   - 13996611 : Bs_DstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut,TightCut.dec  
   No need for TightCut versions if stripping cuts are looser  
   Retire event types 11896611 and 13996611  
   11996413 and 12997613 were never used for productions  
  
! 2019-06-05 - Marco Pappagallo (MR !297)  
   Add 5 decfiles for Omegab->Omegac** pi  
   + 16165030 : Xib_Xicpipi=DecProdCut.dec  
   + 16165233 : Omegab_Omegac0stpi_PPChange,tau=250fs=DecProdCut.dec  
   + 16265431 : Omegab_Omegac0rho_PPChange,tau=250fs=DecProdCut.dec  
   + 16165038 : Omegab_Omegac0pi_PPChange,tau=250fs=DecProdCut.dec  
   + 16165039 : Omegab_Omegac0K_PPChange,tau=250fs=DecProdCut.dec  
  
! 2019-05-30 - Simone Stracka (MR !294)  
   Committed 4 new files:  
   + 21103210: D+_etaprimepi,rhogamma=TightCut.dec  
   + 21103220: D+_etaprimeK,rhogamma=TightCut.dec  
   + 21103280: D+_etapi,pipigamma=TightCut.dec  
   + 21103270: D+_etaK,pipigamma=TightCut.dec  
  
   Modified 4 existing files - the usage of the LoKi descriptor arrows has been corrected (replacing -> arrows with => arrows):  
   + 23103211: Ds+_etaprimepi,rhogamma=TightCut.dec  
   + 23103221: Ds+_etaprimeK,rhogamma=TightCut.dec  
   + 23103280: Ds+_etapi,pipigamma=TightCut.dec  
   + 23103270: Ds+_etaK,pipigamma=TightCut.dec  
   EventTypes 23103211 and 23103221 replace 23103210 and 23103220  
   EventTypes 23103270 and 23103280 are kept (no existing production)  
  
! 2019-05-30 - Yanxi Zhang (MR !293)  
   Add new decay file  
   + 11336000 : Bd_chic0Kpi,chic0=4bodycocktail,DecProdCut.dec  
  
! 2019-01-10 - Marcos Romero Lamas (MR !194)  
   Add new decay file  
   + 13114010 : Bs_mumuKK,mm=DecProdCut.dec  
  
! 2019-05-22 - Liang Sun (MR !289)  
   Add new decay file  
   + 27685001 : Dst_D0pi,Kpipienu=DecProdCut.dec  
  
! 2019-05-28 - Wenqian Huang (MR !291)  
   Add new decay file  
   + 14542008 : Bc_JpsiTauNu,pipipipi0nu=DecProdCut,BcVegPy,taulababar_Update.dec  
  
2019-05-29 - Maria Poliakova (MR !292)  
   Added 1 new decay file  
   + 17514081 : Bs2st_BuK,rhomunu=DecProdCut.dec  
  
! 2019-05-21 - Michel De Cian (MR !287)  
   Add new decay file  
   +  11144009: Bd_Jpsirho0,mm,oneMuonInAcc=TightCuts.dec   
  

