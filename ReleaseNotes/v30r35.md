
  !========================= DecFiles v30r35 2019-08-19 =======================  
  
! 2019-08-14 - Joao Coelho (MR !342)  
   Use EvtGen native PHSP model instead of Pythia one
   + 15876430 : Lb_Lcpipimunu,2hX=cocktail,mu4hinAcc     
  
! 2019-08-09 - Jan-Marc Basels (MR !341)  
   Add 4 new decay files  
   + 11574489 : Bd_Dstenu,Dpi0,Kstmunu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 11574488 : Bd_Dstmunu,Dpi0,Kstenu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 13574211 : Bs_Dsstenu,Dsgamma,phimunu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 13574210 : Bs_Dsstmunu,Dsgamma,phienu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   Update decay files with new form-factors
   + 13574090 : Bs_Dsmunu,phienu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 13574091 : Bs_Dsenu,phimunu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 13574088 : Bs_Dsmunu,Kstenu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 13574089 : Bs_Dsenu,Kstmunu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 11574086 : Bd_Dmunu,Kstenu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 11574087 : Bd_Denu,Kstmunu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
  
! 2019-08-09 - Victor Renaudin (MR !340)  
   Add 4 new decay files  
   + 11166133 : Bd_Dst-K+,D0pi,KSKK=TightCut  
   + 11166132 : Bd_Dst-K+,D0pi,KSpipi=TightCut  
   + 11166143 : Bd_Dst-pi+,D0pi,KSKK=TightCut  
   + 11166142 : Bd_Dst-pi+,D0pi,KSpipi=TightCut  
  
! 2019-08-07 - Alison Maria Tully (MR !339)  
   Add 2 new decay files  
   + 12112002 : Bu_Ktaumu,3pi=DecProdCut,TightCutFixed,tauolababar,phsp  
   + 12112003 : Bu_Ktaumu,3pipi0=DecProdCut,TightCutFixed,tauola8,phsp  
   Fix LoKi cuts decay descriptor for
   + 12112000 : Bu_Ktaumu,3pipi0=DecProdCut,TightCut,tauola8,phsp  
   + 12112001 : Bu_Ktaumu,3pipi0=DecProdCut,TightCut,tauola8,phsp  
  
! 2019-08-07 - Benjamin Audurier (MR !337)  
   Include special Pythia8 settings for onia production for Special tool in incl_X38721++,Jpsirho,mm=DecProdCut.dec  
  
! 2019-08-01 - Jolanta Brodzicka (MR !336)  
   Add 16 new decay files  
   + 26184420 : Sc++_Lcpi,peepi,phsp=TightCut  
   + 26174420 : Sc++_Lcpi,pmumupi,phsp=TightCut  
   + 26184422 : Sc++_Lcpi,pphipi,ee=TightCut  
   + 26174422 : Sc++_Lcpi,pphipi,mumu=TightCut  
   + 26184400 : Sc0_Lcpi,peepi,phsp=TightCut  
   + 26174400 : Sc0_Lcpi,pmumupi,phsp=TightCut  
   + 26184402 : Sc0_Lcpi,pphipi,ee=TightCut  
   + 26174402 : Sc0_Lcpi,pphipi,mumu=TightCut  
   + 26184440 : Scst++_Lcpi,peepi,phsp=TightCut  
   + 26174440 : Scst++_Lcpi,pmumupi,phsp=TightCut  
   + 26184442 : Scst++_Lcpi,pphipi,ee=TightCut  
   + 26174442 : Scst++_Lcpi,pphipi,mumu=TightCut  
   + 26184430 : Scst0_Lcpi,peepi,phsp=TightCut  
   + 26174430 : Scst0_Lcpi,pmumupi,phsp=TightCut  
   + 26184432 : Scst0_Lcpi,pphipi,ee=TightCut  
   + 26174432 : Scst0_Lcpi,pphipi,mumu=TightCut  
  
! 2019-08-01 - Jolanta Brodzicka (MR !335)  
   Add 10 new decay files  
   + 25123401 : Lc_peepi,phsp=OS,TightCut,FromB  
   + 25123400 : Lc_peepi,phsp=OS,TightCut  
   + 25113401 : Lc_pmumupi,phsp=OS,TightCut,FromB  
   + 25113400 : Lc_pmumupi,phsp=OS,TightCut  
   + 25123412 : Lc_pomegapi,ee=TightCut  
   + 25113412 : Lc_pomegapi,mumu=TightCut  
   + 25123411 : Lc_pphipi,ee=TightCut  
   + 25113411 : Lc_pphipi,mumu=TightCut  
   + 25123413 : Lc_prhopi,ee=TightCut  
   + 25113413 : Lc_prhopi,mumu=TightCut  
  
! 2019-07-28 - Santiago Gomez Arias (MR !332)  
   Add 7 new decay files  
   + 14543032 : Bc_Jpsitaunu,ppmununu=BcVegPy,TightCut  
   + 14543211 : Bc_chic0munu,pp=BcVegPy,TightCut  
   + 14543212 : Bc_chic1munu,pp=BcVegPy,TightCut  
   + 14543213 : Bc_chic2munu,pp=BcVegPy,TightCut  
   + 14543031 : Bc_etactaunu,ppmununu=BcVegPy,TightCut  
   + 14543214 : Bc_hcmunu,pp=BcVegPy,TightCut  
   + 14545021 : Bc_psi2Smunu,pp=BcVegPy,TightCut  
  
! 2019-07-26 - Louis Lenard Gerken (MR !330)  
   Add 11 new decay files  
   + 11196064 : Bd_DsDs,3pi3pi=bothDDALITZ,DecProdCut_pCut1600MeV  
   + 11196061 : Bd_DsDs,KKpi3pi=bothDDALITZ,DecProdCut_pCut1600MeV  
   + 11196062 : Bd_DsDs,KKpiKpipi=bothDDALITZ,DecProdCut_pCut1600MeV  
   + 11196063 : Bd_DsDs,Kpipi3pi=bothDDALITZ,DecProdCut_pCut1600MeV  
   + 11196065 : Bd_DsDs,KpipiKpipi=bothDDALITZ,DecProdCut_pCut1600MeV  
   + 11196003 : Bd_DsDs=DDALITZ,DecProdCut_pCut1600MeV  
   + 13196064 : Bs_DsDs,3pi3pi=bothDDALITZ,DecProdCut_pCut1600MeV  
   + 13196061 : Bs_DsDs,KKpi3pi=bothDDALITZ,DecProdCut_pCut1600MeV  
   + 13196062 : Bs_DsDs,KKpiKpipi=bothDDALITZ,DecProdCut_pCut1600MeV  
   + 13196063 : Bs_DsDs,Kpipi3pi=bothDDALITZ,DecProdCut_pCut1600MeV  
   + 13196065 : Bs_DsDs,KpipiKpipi=bothDDALITZ,DecProdCut_pCut1600MeV  
  
