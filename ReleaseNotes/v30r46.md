!========================= DecFiles v30r46 2020-06-04 =======================  
  
! 2020-06-03 - Michal Kreps (MR !517)  
   Add check to decparser that ChargCong of aliased particles uses only aliased particles.  
   Fix decay files  
   + 11166010 : Bd_D-a1+,D0pi-  
   + 11514020 : Bd_a1+mu-nu,rho0pi=DecProdCut,Tightcuts  
   + 11104045 : Bd_a1+pi-,rho0pi=DecProdCut  
   + 13144055 : Bs_psi2Spipi,mm  
  
! 2020-06-03 - Edward Brendan Shields (MR !516)  
   Add new decay file  
   + 12875532 : Bu_D0munu,KSKK=res,cocktail,TightCut2,BRcorr1  
  
! 2020-06-03 - Edward Brendan Shields (MR !515)  
   Add new decay file  
   + 11876133 : Bd_Dstmunu,KSKK=res,cocktail,hqet,TightCut,LooserCuts2,BRcorr1  
  
! 2020-06-03 - Edward Brendan Shields (MR !514)  
   Add new decay file  
   + 27165904 : Dst_D0pi,KSKK=res,TightCut,LooserCuts  
  
! 2020-06-02 - Vitalii Lisovskyi (MR !513)  
   Fix 3 decay files for charge conjugation  
   + 14553013 : Bc_Jpsienu,mm=BcVegPy,DecProdCut  
   + 14553014 : Bc_Jpsimunu,ee=BcVegPy,DecProdCut  
   + 12153012 : Bu_psi2SK,ee=DecProdCut  
  
! 2020-06-01 - Liang Sun (MR !512)  
   Add new decay file  
   + 27265400 : Dst_D0pi,Kpipipipi0=cocktail,DecProdCut  
  
! 2020-05-29 - Guillaume Max Pietrzyk (MR !511)  
   Add 3 new decay files  
   + 27163904 : Dst_D0pi,KK=TightCut,3  
   + 27163903 : Dst_D0pi,Kpi=TightCut,3  
   + 27163905 : Dst_D0pi,pipi=TightCut,3  
  
! 2020-05-28 - Harris Conan Bernstein (MR !510)  
   Add 8 new decay files  
   + 11298010 : Bd_DDKst0,3piX=cocktail,TightCut  
   + 11298410 : Bd_DDKst0,3piXmisid=cocktail,TightCut  
   + 11202011 : Bd_Ksttautau,3pi3pi0=DecProdCut,TightCut,tauolababar  
   + 13298611 : Bs_DsDKst0,3piX=cocktail,TightCut  
   + 13298411 : Bs_DsDKst0,3piXmisid=cocktail,TightCut  
   + 12297411 : Bu_D0DKst0,3piX=cocktail,TightCut  
   + 12297412 : Bu_D0DKst0,3piXmisid=cocktail,TightCut  
   + 15298111 : Lb_LcDKst0,3piX=cocktail,TightCut  
  
! 2020-05-25 - Bernd Kristof Mumme (MR !509)  
   Add 18 new decay files  
   + 14312006 : Bc_MuMajoranaNeutrino2MuX,m=1600MeV,t=1000ps,OS,DecProdCut  
   + 14312004 : Bc_MuMajoranaNeutrino2MuX,m=1600MeV,t=100ps,OS,DecProdCut  
   + 14312002 : Bc_MuMajoranaNeutrino2MuX,m=1600MeV,t=10ps,OS,DecProdCut  
   + 14312016 : Bc_MuMajoranaNeutrino2MuX,m=2000MeV,t=1000ps,OS,DecProdCut  
   + 14312014 : Bc_MuMajoranaNeutrino2MuX,m=2000MeV,t=100ps,OS,DecProdCut  
   + 14312012 : Bc_MuMajoranaNeutrino2MuX,m=2000MeV,t=10ps,OS,DecProdCut  
   + 14372026 : Bc_MuMajoranaNeutrino2MuX,m=3000MeV,t=1000ps,OS,DecProdCut  
   + 14372024 : Bc_MuMajoranaNeutrino2MuX,m=3000MeV,t=100ps,OS,DecProdCut  
   + 14372022 : Bc_MuMajoranaNeutrino2MuX,m=3000MeV,t=10ps,OS,DecProdCut  
   + 14372036 : Bc_MuMajoranaNeutrino2MuX,m=4000MeV,t=1000ps,OS,DecProdCut  
   + 14372034 : Bc_MuMajoranaNeutrino2MuX,m=4000MeV,t=100ps,OS,DecProdCut  
   + 14372032 : Bc_MuMajoranaNeutrino2MuX,m=4000MeV,t=10ps,OS,DecProdCut  
   + 14372046 : Bc_MuMajoranaNeutrino2MuX,m=5000MeV,t=1000ps,OS,DecProdCut  
   + 14372044 : Bc_MuMajoranaNeutrino2MuX,m=5000MeV,t=100ps,OS,DecProdCut  
   + 14372042 : Bc_MuMajoranaNeutrino2MuX,m=5000MeV,t=10ps,OS,DecProdCut  
   + 14372056 : Bc_MuMajoranaNeutrino2MuX,m=5500MeV,t=1000ps,OS,DecProdCut  
   + 14372054 : Bc_MuMajoranaNeutrino2MuX,m=5500MeV,t=100ps,OS,DecProdCut  
   + 14372052 : Bc_MuMajoranaNeutrino2MuX,m=5500MeV,t=10ps,OS,DecProdCut  
  
! 2020-05-25 - Bernd Kristof Mumme (MR !508)  
   Add 18 new decay files  
   + 14312005 : Bc_MuMajoranaNeutrino2MuX,m=1600MeV,t=1000ps,SS,DecProdCut  
   + 14312003 : Bc_MuMajoranaNeutrino2MuX,m=1600MeV,t=100ps,SS,DecProdCut  
   + 14312001 : Bc_MuMajoranaNeutrino2MuX,m=1600MeV,t=10ps,SS,DecProdCut  
   + 14312015 : Bc_MuMajoranaNeutrino2MuX,m=2000MeV,t=1000ps,SS,DecProdCut  
   + 14312013 : Bc_MuMajoranaNeutrino2MuX,m=2000MeV,t=100ps,SS,DecProdCut  
   + 14312011 : Bc_MuMajoranaNeutrino2MuX,m=2000MeV,t=10ps,SS,DecProdCut  
   + 14372025 : Bc_MuMajoranaNeutrino2MuX,m=3000MeV,t=1000ps,SS,DecProdCut  
   + 14372023 : Bc_MuMajoranaNeutrino2MuX,m=3000MeV,t=100ps,SS,DecProdCut  
   + 14372021 : Bc_MuMajoranaNeutrino2MuX,m=3000MeV,t=10ps,SS,DecProdCut  
   + 14372035 : Bc_MuMajoranaNeutrino2MuX,m=4000MeV,t=1000ps,SS,DecProdCut  
   + 14372033 : Bc_MuMajoranaNeutrino2MuX,m=4000MeV,t=100ps,SS,DecProdCut  
   + 14372031 : Bc_MuMajoranaNeutrino2MuX,m=4000MeV,t=10ps,SS,DecProdCut  
   + 14372045 : Bc_MuMajoranaNeutrino2MuX,m=5000MeV,t=1000ps,SS,DecProdCut  
   + 14372043 : Bc_MuMajoranaNeutrino2MuX,m=5000MeV,t=100ps,SS,DecProdCut  
   + 14372041 : Bc_MuMajoranaNeutrino2MuX,m=5000MeV,t=10ps,SS,DecProdCut  
   + 14372055 : Bc_MuMajoranaNeutrino2MuX,m=5500MeV,t=1000ps,SS,DecProdCut  
   + 14372053 : Bc_MuMajoranaNeutrino2MuX,m=5500MeV,t=100ps,SS,DecProdCut  
   + 14372051 : Bc_MuMajoranaNeutrino2MuX,m=5500MeV,t=10ps,SS,DecProdCut  
  
! 2020-05-25 - Bernd Kristof Mumme (MR !507)  
   Add 15 new decay files  
   + 11372006 : Bd_MuXMajoranaNeutrino2MuX,m=1600MeV,t=1000ps,OS,DecProdCut  
   + 11372004 : Bd_MuXMajoranaNeutrino2MuX,m=1600MeV,t=100ps,OS,DecProdCut  
   + 11372002 : Bd_MuXMajoranaNeutrino2MuX,m=1600MeV,t=10ps,OS,DecProdCut  
   + 11372016 : Bd_MuXMajoranaNeutrino2MuX,m=2000MeV,t=1000ps,OS,DecProdCut  
   + 11372014 : Bd_MuXMajoranaNeutrino2MuX,m=2000MeV,t=100ps,OS,DecProdCut  
   + 11372012 : Bd_MuXMajoranaNeutrino2MuX,m=2000MeV,t=10ps,OS,DecProdCut  
   + 11373026 : Bd_MuXMajoranaNeutrino2MuX,m=3000MeV,t=1000ps,OS,DecProdCut  
   + 11373024 : Bd_MuXMajoranaNeutrino2MuX,m=3000MeV,t=100ps,OS,DecProdCut  
   + 11373022 : Bd_MuXMajoranaNeutrino2MuX,m=3000MeV,t=10ps,OS,DecProdCut  
   + 11373036 : Bd_MuXMajoranaNeutrino2MuX,m=4000MeV,t=1000ps,OS,DecProdCut  
   + 11373034 : Bd_MuXMajoranaNeutrino2MuX,m=4000MeV,t=100ps,OS,DecProdCut  
   + 11373032 : Bd_MuXMajoranaNeutrino2MuX,m=4000MeV,t=10ps,OS,DecProdCut  
   + 11373046 : Bd_MuXMajoranaNeutrino2MuX,m=5000MeV,t=1000ps,OS,DecProdCut  
   + 11373044 : Bd_MuXMajoranaNeutrino2MuX,m=5000MeV,t=100ps,OS,DecProdCut  
   + 11373042 : Bd_MuXMajoranaNeutrino2MuX,m=5000MeV,t=10ps,OS,DecProdCut  
  
! 2020-05-25 - Bernd Kristof Mumme (MR !506)  
   Add 12 new decay files  
   + 11372005 : Bd_MuXMajoranaNeutrino2MuX,m=1600MeV,t=1000ps,SS,DecProdCut  
   + 11372003 : Bd_MuXMajoranaNeutrino2MuX,m=1600MeV,t=100ps,SS,DecProdCut  
   + 11372001 : Bd_MuXMajoranaNeutrino2MuX,m=1600MeV,t=10ps,SS,DecProdCut  
   + 11372015 : Bd_MuXMajoranaNeutrino2MuX,m=2000MeV,t=1000ps,SS,DecProdCut  
   + 11372013 : Bd_MuXMajoranaNeutrino2MuX,m=2000MeV,t=100ps,SS,DecProdCut  
   + 11372011 : Bd_MuXMajoranaNeutrino2MuX,m=2000MeV,t=10ps,SS,DecProdCut  
   + 11373035 : Bd_MuXMajoranaNeutrino2MuX,m=4000MeV,t=1000ps,SS,DecProdCut  
   + 11373033 : Bd_MuXMajoranaNeutrino2MuX,m=4000MeV,t=100ps,SS,DecProdCut  
   + 11373031 : Bd_MuXMajoranaNeutrino2MuX,m=4000MeV,t=10ps,SS,DecProdCut  
   + 11373045 : Bd_MuXMajoranaNeutrino2MuX,m=5000MeV,t=1000ps,SS,DecProdCut  
   + 11373043 : Bd_MuXMajoranaNeutrino2MuX,m=5000MeV,t=100ps,SS,DecProdCut  
   + 11373041 : Bd_MuXMajoranaNeutrino2MuX,m=5000MeV,t=10ps,SS,DecProdCut  
  
! 2020-05-25 - Bernd Kristof Mumme (MR !505)  
   Add 9 new decay files  
   + 13372006 : Bs_MuXMajoranaNeutrino2MuX,m=1600MeV,t=1000ps,OS,DecProdCut  
   + 13372004 : Bs_MuXMajoranaNeutrino2MuX,m=1600MeV,t=100ps,OS,DecProdCut  
   + 13372002 : Bs_MuXMajoranaNeutrino2MuX,m=1600MeV,t=10ps,OS,DecProdCut  
   + 13372016 : Bs_MuXMajoranaNeutrino2MuX,m=2000MeV,t=1000ps,OS,DecProdCut  
   + 13372014 : Bs_MuXMajoranaNeutrino2MuX,m=2000MeV,t=100ps,OS,DecProdCut  
   + 13372012 : Bs_MuXMajoranaNeutrino2MuX,m=2000MeV,t=10ps,OS,DecProdCut  
   + 13372026 : Bs_MuXMajoranaNeutrino2MuX,m=3000MeV,t=1000ps,OS,DecProdCut  
   + 13372024 : Bs_MuXMajoranaNeutrino2MuX,m=3000MeV,t=100ps,OS,DecProdCut  
   + 13372022 : Bs_MuXMajoranaNeutrino2MuX,m=3000MeV,t=10ps,OS,DecProdCut  
  
! 2020-05-25 - Bernd Kristof Mumme (MR !504)  
   Add 9 new decay files  
   + 13372005 : Bs_MuXMajoranaNeutrino2MuX,m=1600MeV,t=1000ps,SS,DecProdCut  
   + 13372003 : Bs_MuXMajoranaNeutrino2MuX,m=1600MeV,t=100ps,SS,DecProdCut  
   + 13372001 : Bs_MuXMajoranaNeutrino2MuX,m=1600MeV,t=10ps,SS,DecProdCut  
   + 13372015 : Bs_MuXMajoranaNeutrino2MuX,m=2000MeV,t=1000ps,SS,DecProdCut  
   + 13372013 : Bs_MuXMajoranaNeutrino2MuX,m=2000MeV,t=100ps,SS,DecProdCut  
   + 13372011 : Bs_MuXMajoranaNeutrino2MuX,m=2000MeV,t=10ps,SS,DecProdCut  
   + 13372025 : Bs_MuXMajoranaNeutrino2MuX,m=3000MeV,t=1000ps,SS,DecProdCut  
   + 13372023 : Bs_MuXMajoranaNeutrino2MuX,m=3000MeV,t=100ps,SS,DecProdCut  
   + 13372021 : Bs_MuXMajoranaNeutrino2MuX,m=3000MeV,t=10ps,SS,DecProdCut  
  
! 2020-05-25 - Bernd Kristof Mumme (MR !503)  
   Add 15 new decay files  
   + 12372006 : Bu_MuXMajoranaNeutrino2MuX,m=1600MeV,t=1000ps,OS,DecProdCut  
   + 12372004 : Bu_MuXMajoranaNeutrino2MuX,m=1600MeV,t=100ps,OS,DecProdCut  
   + 12372002 : Bu_MuXMajoranaNeutrino2MuX,m=1600MeV,t=10ps,OS,DecProdCut  
   + 12372016 : Bu_MuXMajoranaNeutrino2MuX,m=2000MeV,t=1000ps,OS,DecProdCut  
   + 12372014 : Bu_MuXMajoranaNeutrino2MuX,m=2000MeV,t=100ps,OS,DecProdCut  
   + 12372012 : Bu_MuXMajoranaNeutrino2MuX,m=2000MeV,t=10ps,OS,DecProdCut  
   + 12372026 : Bu_MuXMajoranaNeutrino2MuX,m=3000MeV,t=1000ps,OS,DecProdCut  
   + 12372024 : Bu_MuXMajoranaNeutrino2MuX,m=3000MeV,t=100ps,OS,DecProdCut  
   + 12372022 : Bu_MuXMajoranaNeutrino2MuX,m=3000MeV,t=10ps,OS,DecProdCut  
   + 12372436 : Bu_MuXMajoranaNeutrino2MuX,m=4000MeV,t=1000ps,OS,DecProdCut  
   + 12372434 : Bu_MuXMajoranaNeutrino2MuX,m=4000MeV,t=100ps,OS,DecProdCut  
   + 12372432 : Bu_MuXMajoranaNeutrino2MuX,m=4000MeV,t=10ps,OS,DecProdCut  
   + 12372446 : Bu_MuXMajoranaNeutrino2MuX,m=5000MeV,t=1000ps,OS,DecProdCut  
   + 12372444 : Bu_MuXMajoranaNeutrino2MuX,m=5000MeV,t=100ps,OS,DecProdCut  
   + 12372442 : Bu_MuXMajoranaNeutrino2MuX,m=5000MeV,t=10ps,OS,DecProdCut  
  
! 2020-05-25 - Bernd Kristof Mumme (MR !502)  
   Add 15 new decay files  
   + 12372005 : Bu_MuXMajoranaNeutrino2MuX,m=1600MeV,t=1000ps,SS,DecProdCut  
   + 12372003 : Bu_MuXMajoranaNeutrino2MuX,m=1600MeV,t=100ps,SS,DecProdCut  
   + 12372001 : Bu_MuXMajoranaNeutrino2MuX,m=1600MeV,t=10ps,SS,DecProdCut  
   + 12372015 : Bu_MuXMajoranaNeutrino2MuX,m=2000MeV,t=1000ps,SS,DecProdCut  
   + 12372013 : Bu_MuXMajoranaNeutrino2MuX,m=2000MeV,t=100ps,SS,DecProdCut  
   + 12372011 : Bu_MuXMajoranaNeutrino2MuX,m=2000MeV,t=10ps,SS,DecProdCut  
   + 12372025 : Bu_MuXMajoranaNeutrino2MuX,m=3000MeV,t=1000ps,SS,DecProdCut  
   + 12372023 : Bu_MuXMajoranaNeutrino2MuX,m=3000MeV,t=100ps,SS,DecProdCut  
   + 12372021 : Bu_MuXMajoranaNeutrino2MuX,m=3000MeV,t=10ps,SS,DecProdCut  
   + 12372435 : Bu_MuXMajoranaNeutrino2MuX,m=4000MeV,t=1000ps,SS,DecProdCut  
   + 12372433 : Bu_MuXMajoranaNeutrino2MuX,m=4000MeV,t=100ps,SS,DecProdCut  
   + 12372431 : Bu_MuXMajoranaNeutrino2MuX,m=4000MeV,t=10ps,SS,DecProdCut  
   + 12372445 : Bu_MuXMajoranaNeutrino2MuX,m=5000MeV,t=1000ps,SS,DecProdCut  
   + 12372443 : Bu_MuXMajoranaNeutrino2MuX,m=5000MeV,t=100ps,SS,DecProdCut  
   + 12372441 : Bu_MuXMajoranaNeutrino2MuX,m=5000MeV,t=10ps,SS,DecProdCut  
  
! 2020-05-20 - Dmitry Golubkov (MR !501)  
   Add new decay file  
   + 14145005 : Bc_psi2SKKpi,mm=BcVegPy,DecProdCut  
  
! 2020-05-20 - Albert Bursche (MR !500)  
   Add 3 new decay files  
   + 24142007 : exclu_Jpsi,mm=coherent_starlight_evtGen  
   + 24142009 : exclu_Jpsi,mm=coherent_starlight_evtGen_longitudinal  
   + 24142008 : exclu_Jpsi,mm=coherent_starlight_evtGen_transverse  
  
! 2020-05-20 - Albert Bursche (MR !499)  
   Add new decay file  
   + 30100201 : exclu_f2_1270,gg=coherent_starlight_evtGen  
  
! 2020-05-14 - Lorenzo Capriotti (MR !498)  
   Add new decay file  
   + 12145405 : Bu_KOmegaJpsi,pi0pi+pi-=PHSP,mm=TightCut,THREEBODYPHSP  
  
! 2020-05-13 - Maximilien Chefdeville (MR !497)  
   Add new decay file  
   + 12143403 : Bu_JpsiKst,mm,Kpi0,sametrkcut=TightCut  
  
! 2020-05-13 - Luke George Scantlebury Smead (MR !496)  
   Add 29 new decay files  
   + 11896602 : Bd_D1Dsst,Dstpi0,D0pi,Kpi=TightCut  
   + 11896400 : Bd_D2stDs,Dstpi0,D0pi,Kpi=TightCut  
   + 11896600 : Bd_D2stDsst,Dstpi0,D0pi,Kpi=TightCut  
   + 11896407 : Bd_DstD0K,D0pi,Kpi=TightCut  
   + 11698403 : Bd_DstD0Kst,D0pi,Kpi=TightCut  
   + 11698000 : Bd_DstDKst0,D0pi,Kpi=TightCut  
   + 11898400 : Bd_DstDprimes1,D0pi,Kpi=TightCut  
   + 11896404 : Bd_DstDs,D0pi,Kpi=TightCut  
   + 11896403 : Bd_DstDs0st,D0pi,Kpi=TightCut  
   + 11896603 : Bd_DstDs1,D0pi,Kpi=TightCut  
   + 11896604 : Bd_DstDsst,D0pi,Kpi=TightCut  
   + 11896406 : Bd_DstDst0K,D0pi,Kpi=TightCut  
   + 11698402 : Bd_DstDst0Kst,D0pi,Kpi=TightCut  
   + 11698401 : Bd_DstDstK0,D0pi,Kpi=TightCut  
   + 11698400 : Bd_DstDstKst0,D0pi,Kpi=TightCut  
   + 11364401 : Bd_Dsta1,D0pi,Kpi=TightCut  
   + 11366400 : Bd_Dstomegapi,D0pi,Kpi=TightCut  
   + 11166401 : Bd_Dstpipipipi0,D0pi,Kpi=TightCut  
   + 11168000 : Bd_Dstpipipipipi,D0pi,Kpi=TightCut  
   + 12897413 : Bu_D0DstK0,D0pi,Kpi=TightCut  
   + 12897402 : Bu_D10Ds,Dstpi,D0pi,Kpi=TightCut  
   + 12897602 : Bu_D10Dsst,Dstpi,D0pi,Kpi=TightCut  
   + 12897400 : Bu_D2st0Ds,Dstpi,D0pi,Kpi=TightCut  
   + 12897600 : Bu_D2st0Dsst,Dstpi,D0pi,Kpi=TightCut  
   + 12897401 : Bu_Dprime10Ds,Dstpi,D0pi,Kpi=TightCut  
   + 12897601 : Bu_Dprime10Dsst,Dstpi,D0pi,Kpi=TightCut  
   + 12897407 : Bu_Dst0DstK0,D0pi,Kpi=TightCut  
   + 12897406 : Bu_Dst0DstKst0,D0pi,Kpi=TightCut  
   + 12167004 : Bu_Dstpipipipi,D0pi,Kpi=TightCut  
  
! 2020-05-13 - Benedetto Gianluca Siddi (MR !495)  
   Add 2 new decay files  
   + 21263005 : incl_b=D+,Kpipi,3pi=DDALITZ,DecProdCut,ExtraParticles  
   + 23903000 : incl_b=Ds,KKpi,3pi=DDALITZ,DecProdCut,ExtraParticles  
  
! 2020-05-13 - Adam Morris (MR !494)  
   Just a minor change to the error message in an `assert` statement in `test/run_gauss.py` to make it clearer what the problem is with configuration of TightCuts   
  
! 2020-05-12 - Vanya Belyaev (MR !493)  
   Add new decay file  
   + 28496040 : X3876_DDstar=TightCut  
  
! 2020-05-09 - Mengzhen Wang (MR !492)  
   Add 2 new decay files  
   + 16468041 : Xibstar6327_LbKpi,Lb=cocktail,DecProdCut  
   + 16468040 : Xibstar6330_LbKpi,Lb=cocktail,DecProdCut  
  
! 2020-05-05 - Gary Robertson (MR !491)  
   Modify generator level cuts in decay files  
   + 26197071 : Pc4400,Sigma_c++D-,Lcpi,pkpi=TightCut,InAcc  
   + 26196041 : Pc4400,Sigma_c++D0bar,Lcpi,pkpi=TightCut,InAcc  
   + 26197072 : Pc4500,Sigma_c++Dst-,Lcpi,pkpi=TightCut,InAcc  
   + 26197073 : Pc4800,Sigma_c++D-,Lcpi,pkpi=TightCut,InAcc
   + 26196042 : Pc4800,Sigma_c++D0bar,Lcpi,pkpi=TightCut,InAcc
   + 26197074 : Pc4800,Sigma_c++Dst-,Lcpi,pkpi=TightCut,InAcc
   + 26197075 : Pc5200,Sigma_c++D-,Lcpi,pkpi=TightCut,InAcc
   + 26196043 : Pc5200,Sigma_c++D0bar,Lcpi,pkpi=TightCut,InAcc
   + 26197076 : Pc5200,Sigma_c++Dst-,Lcpi,pkpi=TightCut,InAcc
   + 26197077 : Pc5600,Sigma_c++D-,Lcpi,pkpi=TightCut,InAcc
   + 26196044 : Pc5600,Sigma_c++D0bar,Lcpi,pkpi=TightCut,InAcc
   + 26197078 : Pc5600,Sigma_c++Dst-,Lcpi,pkpi=TightCut,InAcc  
  
! 2020-04-18 - Jacco Andreas De Vries (MR !472)  
   Update decay model to speed up generation  
   + 26103091 : Xic_pKpi=phsp,TightCutv2  
  
