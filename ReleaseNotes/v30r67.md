DecFiles v30r67 2021-11-15 
==========================  
 
! 2021-11-11 - Ganrong Wang (MR !903)  
   Add 4 new decay files  
   + 11144442 : Bd_Jpsia20,mm,pipipi=DecProdCut  
   + 11144405 : Bd_Jpsiomega,mm=PHSP,DecProdCut  
   + 13144405 : Bs_Jpsiomega,mm=PHSP,DecProdCut  
   + 13144406 : Bs_Jpsiomega,mm=SVV,DecProdCut  
  
! 2021-11-10 - Carmen Giugliano (MR !902)  
   Add 3 new decay files  
   + 13563200 : Bs_DsTauNu,KKPi,PiPiPiPi0=TightCut,tauolababar,pi0notreqinacc  
   + 13763400 : Bs_DsstTauNu,KKPi,PiPiPiPi0=TightCut,tauolababar,pi0notreqinacc  
   + 13863600 : Bs_DsststTauNu,KKPi,PiPiPiPi0=TightCut,tauolababar,pi0notreqinacc  
  
! 2021-11-10 - Harry Victor Cliff (MR !901)  
   Add 2 new decay files  
   + 13144018 : Bs_Jpsiphi,eemm=CPV,update2016,DecProdCut  
   + 13144017 : Bs_Jpsiphi,mmee=CPV,update2016,DecProdCut  
  
! 2021-11-10 - Haodong Mao (MR !900, !904)  
   Add new decay file  
   + 13142223 : Bs_Jpsigamma,mm=HighPtGamma,TightCut  
  
! 2021-11-05 - Slava Matiunin (MR !899)  
   Add new decay file  
   + 12243220 : Bu_X3872K,psig=TightCuts  
  
! 2021-11-05 - Scott Edward Ely (MR !898)  
   Add 3 new decay files  
   + 15694000 : Lb_LcDs,pKpi,taumunu=TightCut  
   + 15876040 : Lb_Lcpipimunu,pKpi=TightCut  
   + 15874050 : Lb_Lctaunu,pKpi,mununu,Lb2Baryonlnu=TightCut  
  
! 2021-11-04 - Slava Matiunin (MR !897)  
   Add 2 new decay files  
   + 11444410 : Bd_Psi2SKX=TightCut  
   + 12445000 : Bu_Psi2SKX=TightCut  
  
! 2021-11-04 - Seophine Stanislaus (MR !896)  
   Add 2 new decay files  
   + 11166134 : Bd_Dst-K+,D0pi,KSpipi=TightCut,LooserCuts  
   + 11166145 : Bd_Dst-pi+,D0pi,KSpipi=TightCut,LooserCuts  
  
! 2021-10-29 - Slava Matiunin (MR !895)  
   Add 2 new decay files  
   + 11244010 : Bd_Psi2SKpi=TightCut  
   + 12243410 : Bu_Psi2SKpi=TightCut  
  
! 2021-10-26 - Jolanta Brodzicka (MR !893)  
   Add new decay file  
   + 27263477 : Dst_D0pi,Kpieta=TightCut,tighter,Coctail  
  
! 2021-10-25 - Niladri Sahoo (MR !888)  
   Add 14 new decay files  
   + 16155132 : Omegab_JpsiOmega,ee,LambdaK=phsp,TightCut  
   + 16125131 : Omegab_Omegaee,LambdaK=phsp,TightCut  
   + 16155133 : Omegab_psi2SOmega,ee,LambdaK=phsp,TightCut  
   + 16154542 : Xib0_JpsiXi0,ee,Lambdapi0=phsp,DecProdCut  
   + 16154541 : Xib0_JpsiXi0,ee,Lambdapi0=phsp,TightCut  
   + 16124541 : Xib0_Xi0ee,Lambdapi=phsp,DecProdCut  
   + 16124540 : Xib0_Xi0ee,Lambdapi=phsp,TightCut  
   + 16154543 : Xib0_psi2SXi0,ee,Lambdapi0=phsp,DecProdCut  
   + 16154544 : Xib0_psi2SXi0,ee,Lambdapi0=phsp,TightCut  
   + 16144542 : Xib0_psi2SXi0,mm,Lambdapi0=phsp,DecProdCut  
   + 16144543 : Xib0_psi2SXi0,mm,Lambdapi0=phsp,TightCut  
   + 16155130 : Xib_JpsiXi,ee,Lambdapi=TightCut  
   + 16125130 : Xib_Xiee,Lambdapi=phsp,TightCut  
   + 16155131 : Xib_psi2SXi,ee,Lambdapi=TightCut  
  
  
