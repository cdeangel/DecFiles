# EventType: 15696042
#
# Descriptor: [Lambda_b0 -> (Lambda_c+ -> p+ K- mu+ nu_mu) (Ds- -> (phi(1020) -> K- K+) mu- anti-nu_mu)]cc
#
# NickName: Lb_LambdacDs,pKmunu,phimunu=KKmumuInAcc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalPlain.addTool( LoKi__GenCutTool,'TightCut')
# kkmumuInAcc = Generation().SignalPlain.TightCut
# kkmumuInAcc.Decay = '[^(Lambda_b0 ==> K+ K- ^mu+ ^mu- nu_mu nu_mu~ {X} {X})]CC'
# kkmumuInAcc.Preambulo += [
#     'inAcc        = (in_range(0.01, GTHETA, 0.400))',
#     'twoKaonsInAcc = (GNINTREE( ("K+"==GID) & inAcc) >= 1) & (GNINTREE( ("K-"==GID) & inAcc) >= 1)'
#     ]
# kkmumuInAcc.Cuts = {
#     '[mu+]cc'   : 'inAcc',
#     '[Lambda_b0]cc'   : 'twoKaonsInAcc'
#     }
#
# EndInsertPythonCode
#
# Documentation: Lambda_b0 -> Lambda_c+ Ds- decays with Lambda_c+ -> p+ K- mu+ nu_mu and Ds- -> phi(1020) mu- anti-nu_mu, KKmumu in acceptance
# EndDocumentation
#
# CPUTime: 1 min
# PhysicsWG: RD
# Tested: Yes
# Responsible: H. Tilquin
# Email: hanae.tilquin@cern.ch
# Date: 20211012
#
Alias         MyLambda_c+          Lambda_c+
Alias         Myanti-Lambda_c-     anti-Lambda_c-
ChargeConj    MyLambda_c+          Myanti-Lambda_c-
#
Alias         MyLambda1520         Lambda(1520)0
Alias         Myanti-Lambda1520    anti-Lambda(1520)0
ChargeConj    MyLambda1520         Myanti-Lambda1520
#
Alias         MyD_s+               D_s+
Alias         MyD_s-               D_s-
ChargeConj    MyD_s+               MyD_s-
#
Alias         MyPhi                phi
ChargeConj    MyPhi                MyPhi
#
Decay Lambda_b0sig
  1.000       MyLambda_c+          MyD_s-         PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyD_s+
  0.500       MyPhi                mu+  nu_mu     ISGW2;
Enddecay
CDecay MyD_s-
#
Decay MyLambda_c+
  0.0350      p+         K-        mu+  nu_mu     PHSP;
  0.0220      MyLambda1520         mu+  nu_mu     PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyPhi
  1.000       K+         K-                       VSS;
Enddecay
#
Decay MyLambda1520
  1.000       p+         K-                       PHSP;
Enddecay
CDecay Myanti-Lambda1520
#
End
#
