# EventType: 11166091
# 
# Descriptor: [B_0 -> (D- -> K+ pi- pi-) (phi -> K- K+) pi+]cc
# 
# NickName: Bd_DPhipi,Kpipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool , 'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[Beauty ==> (D- ==> ^K+ ^pi- ^pi-) (phi(1020) ==> ^K+ ^K-) ^pi+]CC"
# tightCut.Preambulo += [ "from LoKiCore.functions import in_range",
#                           "from GaudiKernel.SystemOfUnits import  GeV, mrad",
#                         ]
# tightCut.Cuts      =    {
# '[pi+]cc'   : " in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV )",
# '[K-]cc'   : "  in_range( 0.010 , GTHETA , 0.400 )  & ( GPT > 250 * MeV )"
#    }
# EndInsertPythonCode
#
# Documentation: B0 -> D- phi pi with D -> K pi pi, with tight cuts. Includes resonances in D decay
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# CPUTime: <1min
# Responsible: Matt Rudolph
# Email: matthew.scott.rudolph@cern.ch
# Date: 20221010
#
#
Alias      MyD-     D-
Alias      MyD+     D+
ChargeConj MyD+     MyD-
#
Alias      MyPhi      phi
ChargeConj MyPhi      MyPhi
#
#
Decay B0sig
  1.000    MyD-     MyPhi    pi+    PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyD-
  1.000    K+         pi-       pi-    D_DALITZ;
Enddecay
CDecay MyD+
#
Decay MyPhi
  1.000    K+         K-              VSS;
Enddecay
End
