# EventType: 15104372
#
# Descriptor: [Beauty -> p+ pi- (KS0 -> pi+ pi-) gamma ]cc
#
# NickName: Lb_Ksppi-gamma=TightCut,mKshhCut,PHSP 
#
# Cuts: LoKi::GenCutTool/TightCut 
# CPUTime: 2 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation() 
# gen.SignalPlain.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/mKshhCut"
# evtgendecay.addTool( LoKi__GenCutTool ,'mKshhCut')
# evtgendecay.mKshhCut.Decay = '[^(Beauty => p+ pi- KS0 gamma)]CC'
# evtgendecay.mKshhCut.Cuts  = {'[Lambda_b0]cc' : ' mKshhCut '}
# evtgendecay.mKshhCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "mKshhCut   = ( GMASS(CS('[(Beauty => ^p+ pi- KS0 gamma)]CC'),CS('[(Beauty => p+ ^pi- KS0 gamma)]CC'), CS('[(Beauty => p+ pi- ^KS0 gamma)]CC')) < 2.8 * GeV)"]
#
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[Beauty => ^p+ ^pi- (KS0 => ^pi+ ^pi-) ^gamma]CC'
# tightCut.Cuts      =    {
#     '[pi+]cc'        : ' inAcc' , 
#     '[p+]cc'         : ' inAcc' , 
#     'gamma'          : ' goodPhoton'} 
#
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) " , 
#     "goodPhoton = ( GPT > 1.5 * GeV ) & inAcc" ]
#
# EndInsertPythonCode
#
# Documentation: Bkgd for Kspipigamma, all in PHSP, pi pi in acceptance, m(Ksppi) < 2.8 GeV
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20190106
#
Alias      MyK0s  K_S0
ChargeConj MyK0s  MyK0s
#
Decay Lambda_b0sig
  1.000   p+  pi-    MyK0s      gamma         PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyK0s
  1.000   pi+         pi-       PHSP;
Enddecay
#
End
