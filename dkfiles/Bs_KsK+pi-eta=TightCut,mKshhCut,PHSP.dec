# EventType: 13104513
#
# Descriptor: [Beauty -> K+ pi- (KS0 -> pi+ pi-) (eta -> gamma gamma)]cc
#
# NickName: Bs_KsK+pi-eta=TightCut,mKshhCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation() 
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/mKshhCut"
# evtgendecay.addTool( LoKi__GenCutTool ,'mKshhCut')
# evtgendecay.mKshhCut.Decay = '[^(Beauty => K+ pi- KS0 eta)]CC'
# evtgendecay.mKshhCut.Cuts  = {'[B_s0]cc' : ' (mKshhCut & gammaPTCut) '}
# evtgendecay.mKshhCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "mKshhCut   = (GMASS(CS('[(Beauty => ^K+ pi- KS0 eta)]CC'),CS('[(Beauty => K+ ^pi- KS0 eta)]CC'), CS('[(Beauty => K+ pi- ^KS0 eta)]CC')) < 2.3 * GeV)",
#     "gammaPTCut   = ( (GCHILD(GPT,CS('[(Beauty => K+ pi- KS0 (eta => ^gamma gamma)) ]CC')) > 1.5 * GeV) | ( GCHILD(GPT,CS('[(Beauty => K+ pi- KS0 (eta => gamma ^gamma) )]CC')) > 1.5 * GeV) )"]
#
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[Beauty => ^K+ ^pi- (KS0 => ^pi+ ^pi-) (eta => ^gamma ^gamma)]CC'
# tightCut.Cuts      =   {
#     '[K+]cc'        : ' inAcc' , 
#     '[pi-]cc'       : ' inAcc' , 
#     'gamma'         : ' inAcc' }
#
# tightCut.Preambulo += [
#     "CS         = LoKi.GenChild.Selector",
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) "]
#
# EndInsertPythonCode
#
# Documentation: Bkgd for Kspipigamma, all in PHSP, pi pi in acceptance, at least one high PT photon
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20191031
#
Alias      MyK0s  K_S0
ChargeConj MyK0s  MyK0s
#
Alias      Myeta        eta
ChargeConj Myeta        Myeta
#
Decay B_s0sig
  1.000   K+  pi-    MyK0s      Myeta         PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyK0s
  1.000   pi+         pi-       PHSP;
Enddecay
#
Decay Myeta
  1.000        gamma      gamma           PHSP;
Enddecay
#
End
