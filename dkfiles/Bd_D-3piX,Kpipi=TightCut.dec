# EventType: 11466421
#
# Descriptor: {[B~0 --> (D+ => K- pi+ pi+) pi- pi+ pi- ... ]cc}
#
# NickName: Bd_D-3piX,Kpipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal = generation.SignalRepeatedHadronization
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut = signal.TightCut
# tightCut.Decay = '[^( (Beauty & LongLived) --> ^(D+ => K- pi+ pi+) pi- pi+ ...) ]CC'
# tightCut.Preambulo += [
#      'from GaudiKernel.SystemOfUnits import MeV'
#     ,'inAcc = ( 0 < GPZ )  &  ( 200 * MeV < GPT ) & ( 1200 * MeV < GP ) & in_range ( 1.8 , GETA , 5.0 ) & in_range ( 0.005 , GTHETA , 0.400 )'
#     ,"goodDp = GINTREE(( 'D+'  == GABSID ) & (GP>12000*MeV) & (GPT>1500*MeV) & ( GNINTREE(( 'K-' == GABSID ) & ( GPT > 1400*MeV ) & ( GP > 5000*MeV ) & inAcc, HepMC.descendants) == 1 ) & ( GNINTREE(( 'pi+' == GABSID ) & ( GPT > 200*MeV ) & ( GP > 1600*MeV ) & inAcc, HepMC.descendants) == 2 ))"
#     ,"nPiB = GCOUNT(('pi+' == GABSID) & inAcc & ( GNINTREE ( ('KS0' == GABSID) | ('KL0' == GABSID), HepMC.parents)==0 ), HepMC.descendants)"
#     ,"goodB = ( ( GNINTREE(  ( 'D+'==GABSID ) , HepMC.descendants) == 1 ) &  ( nPiB >= 5) )"
# ]
# tightCut.Cuts = {
#     '[D+]cc': 'goodDp',
#     '[B0]cc': 'goodB'
#     }
#
# EndInsertPythonCode
#
# Documentation: Generic B~0 -> D+ pi- pi+ pi- X decay file for B2XTauNu analyses.
# EndDocumentation
#
# CPUTime: <1 min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Antonio Romero Vidal
# Email: antonio.romero@usc.es
# Date: 20220301
#
Alias           theD_1+_D+         D_1+
Alias           theD_1-_D-         D_1-
ChargeConj      theD_1+_D+         theD_1-_D-
#
Alias      theD-         D-
Alias      theD+         D+
ChargeConj theD-         theD+
#
Alias           theD*-_D-        D*-
Alias           theD*+_D+        D*+
ChargeConj      theD*-_D-        theD*+_D+
#
Alias      Mya_1-             a_1-
Alias      Mya_1+             a_1+
ChargeConj Mya_1+             Mya_1-
#
Alias      Mya_1-_3pi         a_1-
Alias      Mya_1+_3pi         a_1+
ChargeConj Mya_1+_3pi         Mya_1-_3pi
#
Alias      Mya_1-_2pi0        a_1-
Alias      Mya_1+_2pi0        a_1+
ChargeConj Mya_1+_2pi0        Mya_1-_2pi0
#
Alias      Mya_10             a_10
ChargeConj Mya_10             Mya_10
#
Alias      Myrho0             rho0
ChargeConj Myrho0             Myrho0
#
Alias      Myrho-             rho-
Alias      Myrho+             rho+
ChargeConj Myrho+             Myrho-
#
Alias      Myf_2              f_2
ChargeConj Myf_2              Myf_2
#
Alias      Myomega            omega
ChargeConj Myomega            Myomega
#
Alias      MyK*-              K*-
Alias      MyK*+              K*+
ChargeConj MyK*-              MyK*+
#
Alias      MyK_2*-            K_2*-
Alias      MyK_2*+            K_2*+
ChargeConj MyK_2*-            MyK_2*+
#
Alias           Myeta2pi      eta
ChargeConj      Myeta2pi      Myeta2pi
#
Alias           Myetap2pi     eta'
ChargeConj      Myetap2pi     Myetap2pi
#
Decay anti-B0sig
#
# 3pi
#
0.01275    Mya_1-_3pi theD+                SVS;
0.002125   theD+      Myrho0    pi-        PHSP;
0.0017     theD+      Myf_2     pi-        PHSP;
0.00034    theD_1+_D+ pi-                  SVS;
#
0.0057375  theD*+_D+  Mya_1-_3pi           SVV_HELAMP 0.200 0.0 0.866 0.0 0.458 0.0;
0.00104125 theD*+_D+  Myf_2     pi-        PHSP;
0.0009775  theD*+_D+  Myrho0    pi-        PHSP;
0.00040375 theD*+_D+  pi+       pi-    pi- PHSP;
#
# 3pi pi0's
#
0.0025  theD+      Myomega   pi-                             PHSP; # 0.0028*0.893;
0.00071 theD*+_D+  Myomega   pi-                             PHSP; # (0.00246+-0.00018)*0.893*0.323;
#
0.0120  theD+      Mya_1-_3pi        pi0                     PHSP;
0.0060  theD+      Myrho0    pi-     pi0                     PHSP;
0.0060  theD+      pi-       pi+     pi-     pi0             PHSP;
0.0420  theD+      Myomega   pi-     pi0                     PHSP;
#
0.0040  theD*+_D+  Mya_1-_3pi        pi0                     PHSP;
0.0020  theD*+_D+  Myrho0    pi-     pi0                     PHSP;
0.0020  theD*+_D+  pi-       pi+     pi-     pi0             PHSP;
0.0140  theD*+_D+  Myomega   pi-     pi0                     PHSP;
#
#
0.0120  theD+      Mya_1-_3pi        pi0     pi0             PHSP;
0.0060  theD+      Myrho0    pi-     pi0     pi0             PHSP;
0.0060  theD+      pi-       pi+     pi-     pi0    pi0      PHSP;
0.0420  theD+      Myomega   pi-     pi0     pi0             PHSP;
#
0.0040  theD*+_D+  Mya_1-_3pi        pi0     pi0             PHSP;
0.0020  theD*+_D+  Myrho0    pi-     pi0     pi0             PHSP;
0.0020  theD*+_D+  pi-       pi+     pi-     pi0    pi0      PHSP;
0.0140  theD*+_D+  Myomega   pi-     pi0     pi0             PHSP;
#
# 5pi
#
0.0032  theD+      Mya_1-_3pi        pi+     pi-             PHSP;
0.0016  theD+      Myrho0            pi-     pi+     pi-     PHSP;
0.0016  theD+      pi-       pi+     pi-     pi+     pi-     PHSP;
#
0.00080 theD*+_D+  Mya_1-_3pi        pi+     pi-             PHSP;
0.00040 theD*+_D+  Myrho0    pi-     pi+     pi-             PHSP;
0.00040 theD*+_D+  pi-       pi+     pi-     pi+     pi-     PHSP;
#
# 5pi pi0
#
0.0080  theD+      Mya_1-_3pi        pi+     pi-     pi0            PHSP;
0.0040  theD+      Myrho0            pi-     pi+     pi-     pi0    PHSP;
0.0040  theD+      pi-       pi+     pi-     pi+     pi-     pi0    PHSP;
0.0040  theD+      Myomega   pi-     pi+     pi-                    PHSP;
0.0040  theD+      Myomega   pi-     pi+     pi-     pi0            PHSP;
#
0.0020  theD*+_D+  Mya_1-_3pi        pi+     pi-     pi0            PHSP;
0.0010  theD*+_D+  Myrho0    pi-     pi+     pi-     pi0            PHSP;
0.0010  theD*+_D+  pi-       pi+     pi-     pi+     pi-     pi0    PHSP;
0.0010  theD*+_D+  Myomega   pi-     pi+     pi-                    PHSP;
0.0010  theD*+_D+  Myomega   pi-     pi+     pi-     pi0            PHSP;
#
#
0.0080  theD+      Mya_1-_3pi        pi+     pi-     pi0     pi0           PHSP;
0.0040  theD+      Myrho0            pi-     pi+     pi-     pi0    pi0    PHSP;
0.0040  theD+      pi-       pi+     pi-     pi+     pi-     pi0    pi0    PHSP;
0.0040  theD+      Myomega   pi-     pi+     pi-     pi0     pi0           PHSP;
#
0.0020  theD*+_D+  Mya_1-_3pi        pi+     pi-     pi0     pi0           PHSP;
0.0010  theD*+_D+  Myrho0    pi-     pi+     pi-     pi0     pi0           PHSP;
0.0010  theD*+_D+  pi-       pi+     pi-     pi+     pi-     pi0    pi0    PHSP;
0.0010  theD*+_D+  Myomega   pi-     pi+     pi-     pi0     pi0           PHSP;
#
#
# 7pi
#
0.00010 theD+      Mya_1-_3pi        pi+     pi-     pi+    pi-          PHSP;
0.00010 theD+      Myrho0            pi-     pi+     pi-    pi+    pi-   PHSP;
0.00010 theD+      pi-       pi+     pi-     pi+     pi-    pi+    pi-   PHSP;
#
0.000050 theD*+_D+ Mya_1-_3pi        pi+     pi-     pi+     pi-             PHSP;
0.000016 theD*+_D+ Myrho0    pi-     pi+     pi-     pi+     pi-             PHSP;
0.000016 theD*+_D+ pi-       pi+     pi-     pi+     pi-     pi+     pi-     PHSP;
#
# 7pi pi0
#
0.00020 theD+      Mya_1-_3pi        pi+     pi-     pi+    pi-    pi0          PHSP;
0.00010 theD+      Myrho0            pi-     pi+     pi-    pi+    pi-   pi0    PHSP;
0.00010 theD+      pi-       pi+     pi-     pi+     pi-    pi+    pi-   pi0    PHSP;
0.00010 theD+      Myomega   pi-     pi+     pi-     pi+    pi-                 PHSP;
0.00010 theD+      Myomega   pi-     pi+     pi-     pi+    pi-    pi0          PHSP;
#
0.000100 theD*+_D+  Mya_1-_3pi      pi+     pi-     pi+     pi-    pi0          PHSP;
0.000032 theD*+_D+  Myrho0  pi-     pi+     pi-     pi+     pi-    pi0          PHSP;
0.000032 theD*+_D+  pi-     pi+     pi-     pi+     pi-     pi+    pi-   pi0    PHSP;
0.000032 theD*+_D+  Myomega pi-     pi+     pi-     pi+     pi-                 PHSP;
0.000032 theD*+_D+  Myomega pi-     pi+     pi-     pi+     pi-    pi0          PHSP;
#
#
0.00020 theD+      Mya_1-_3pi        pi+     pi-     pi+    pi-    pi0   pi0           PHSP;
0.00010 theD+      Myrho0            pi-     pi+     pi-    pi+    pi-   pi0    pi0    PHSP;
0.00010 theD+      pi-       pi+     pi-     pi+     pi-    pi+    pi-   pi0    pi0    PHSP;
0.00010 theD+      Myomega   pi-     pi+     pi-     pi+    pi-    pi0   pi0           PHSP;
#
0.000100 theD*+_D+  Mya_1-_3pi      pi+     pi-     pi+     pi-    pi0   pi0           PHSP;
0.000032 theD*+_D+  Myrho0  pi-     pi+     pi-     pi+     pi-    pi0   pi0           PHSP;
0.000032 theD*+_D+  pi-     pi+     pi-     pi+     pi-     pi+    pi-   pi0    pi0    PHSP;
0.000032 theD*+_D+  Myomega pi-     pi+     pi-     pi+     pi-    pi0   pi0           PHSP;
#
#
Enddecay
CDecay B0sig
#
Decay Myf_2
  1.00    pi+    pi-          TSS ;
Enddecay
#
Decay Myrho0
1.000   pi+    pi-          VSS;
Enddecay
#
Decay Myrho-
1.000    pi- pi0                         VSS;
Enddecay
CDecay Myrho+
#
Decay Mya_1+
0.4920   Myrho0 pi+                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
0.5080   Myrho+ pi0                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-
#
Decay Mya_1+_3pi
0.4920   Myrho0 pi+                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
#0.5080   Myrho+ pi0                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-_3pi
#
Decay Mya_1+_2pi0
1.000   Myrho+ pi0          VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-_2pi0
#
Decay Mya_10
0.5000   Myrho- pi+                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
0.5000   Myrho+ pi-                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
#
Decay Myomega
0.893 pi-     pi+     pi0                             OMEGA_DALITZ; # (0.893+-0.006);
Enddecay
#
Decay MyK_2*-
0.0299565   MyK*- pi+ pi-                     PHSP; # 0.0450 x 0.6657;
Enddecay
CDecay MyK_2*+
#
Decay MyK*-
0.6657      anti-K0   pi-                        VSS;
Enddecay
CDecay MyK*+
#
Decay Myetap2pi
# 0.80897
0.426       pi+     pi-     eta                               PHSP; # (0.426 +- 0.7);
0.06358     pi0     pi0     Myeta2pi                          PHSP; # (0.228 +- 0.008) x 0.27888 (eta -> 2pi X);
0.289       Myrho0  gamma                                     SVP_HELAMP  1.0 0.0 1.0 0.0; # (0.289 +- 0.005);
0.0238027   Myomega gamma                             SVP_HELAMP  1.0 0.0 1.0 0.0; # (0.0262 +- 0.0013) x 0.9085 (omega -> 2pi X);
#0.000109    gamma   mu-     mu+                               PHOTOS   PHSP; # (0.000109 +- 0.000027);
#0.000473    gamma   e-      e+                                PHOTOS   PHSP; # (0.000473 +- 0.000030);
0.00361     pi+     pi-     pi0                               PHSP;  # (0.00361 +- 0.00017);
0.0024      pi+     pi-     e+      e-                        PHSP;  # (0.0024 +0.0013-0.0010);
Enddecay
#
Decay Myeta2pi
# 0.27888
0.2292      pi-     pi+     pi0                             ETA_DALITZ; # (0.2292 +- 0.0028);
0.0422      gamma   pi-     pi+                             PHSP; # (0.0422 +- 0.0008);
#0.0069      gamma   e+      e-                              PHSP; # (0.0069 +- 0.0004);
#0.00031     gamma   mu+     mu-                             PHSP; # (0.00031 +- 0.00004);
#0.000268    pi+     pi-     e+      e-                      PHSP; # (0.000268 +- 0.000011);
#0.0000058   mu+     mu-                                     PHSP; # (0.0000058 +- 0.0000008);
Enddecay
#
Decay theD-
1.0       K+         pi-        pi-                D_DALITZ;
Enddecay
CDecay theD+
#
Decay theD*-_D-
#0.323
0.307     theD-     pi0                            VSS; # 0.307 +- 0.005;
0.016     theD-     gamma                          VSP_PWAVE; # 0.016 +- 0.004;
Enddecay
CDecay theD*+_D+
#
Decay theD_1+_D+
# 0.1076
#0.1076  theD*+_D+ pi0                              VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0; # 0.3333 x 0.323 (D*+ -> D+ X);
1.000   theD+   pi+       pi-       PHSP;
Enddecay
CDecay theD_1-_D-
#
End
