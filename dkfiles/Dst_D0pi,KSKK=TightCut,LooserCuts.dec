# EventType: 27165902
#
# Descriptor: [D*+ -> (D0 -> (K_S0 -> pi+ pi-) K+ K-) pi+]cc
#
# NickName: Dst_D0pi,KSKK=TightCut,LooserCuts
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '^[ D*(2010)+ => ^( D0 => ^( KS0 => ^pi+ ^pi- ) ^K+ ^K- ) ^pi+]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter ',
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) ',
#     'goodD0    = ((GPT > 1800 * MeV) & (GTIME > 0.02 * millimeter))',
#     'kaoncuts  = ( GNINTREE( ("K+" == GABSID ) & (GP > 2000 * MeV) , 4) > 1.5 )',
#     'pioncuts  = ( GNINTREE( ("pi+" == GABSID ) & (GP > 1000 * MeV) , 4) > 1.5 )',
#     'goodKS    = ( GFAEVX( abs( GVZ  ) , 0 ) < 2400.0 * millimeter )',
#     'goodDst   = ( (GPT > 1900 * MeV) & (GPT < (3*GP/10)) & (GPT > (7*GP/300 - 7/3))) ',
#     'trigger   = ( GNINTREE( (("pi+" == GABSID) | ("K+" == GABSID)) & (GPT > 1000 * MeV ) & (GP > 2700 * MeV) , 4)  > 0.5) ',
# ]
# tightCut.Cuts      =    {
#     '[pi+]cc'   : 'inAcc',
#     '[K+]cc'    : 'inAcc',
#     '[D0]cc'    : 'goodD0 & pioncuts & trigger & kaoncuts',
#     '[D*(2010)+]cc' : 'goodDst',
#     'KS0'       : 'goodKS',
#     }
# EndInsertPythonCode
#
# Documentation: Inclusive production of D*+. D* is forced to decay to D0 pi+, then D0 to (KS K+ K-) as phase space,
# then KS to (pi+ pi-) as phase space. Decay products in acceptance.
# 
# EndDocumentation
#
# CPUTime: <1min
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Kamil Fischer
# Email: kamil.fischer@physics.ox.ac.uk
# Date: 20191209
#
Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0
Alias myK_S0  K_S0
ChargeConj myK_S0 myK_S0
#
Decay D*+sig
  1.000 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig
#
Decay MyD0
  1.000  myK_S0 K+  K-        PHSP;
Enddecay
CDecay MyantiD0
#
Decay myK_S0
1.000     pi+  pi-                      PHSP;
Enddecay
#
End
#
