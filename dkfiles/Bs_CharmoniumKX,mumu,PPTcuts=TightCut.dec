# EventType: 13444022
#
# Descriptor: [B_s0 -> (Charmonium -> mu+ mu- X) K+ X]cc 
# 
# NickName: Bs_CharmoniumKX,mumu,PPTcuts=TightCut
# 
# Cuts: LoKi::GenCutTool/TightCut 
# 
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool 
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool(LoKi__GenCutTool,'TightCut') 
# tightCut = Generation().SignalRepeatedHadronization.TightCut 
# tightCut.Decay     = "^( (Beauty & LongLived) --> ( ( J/psi(1S) | psi(2S) ) --> mu+ mu- ...) [K+]cc ... )"
# tightCut.Preambulo = [
#    "from GaudiKernel.SystemOfUnits import  MeV, GeV", 
#    "inAcc        = in_range ( 0.010 , GTHETA , 0.400 ) &  in_range ( 1.9 , GETA , 5.1 )" ,
#    "muon_kine    = ( GP > 5 * GeV ) & ( GPT > 1   * GeV   ) " , 
#    "kaon_kine    = ( GP > 7 * GeV ) & ( GPT > 300 * MeV   ) " ,
#    "has_mu_plus  = GINTREE ( ( 'mu+' == GID ) & muon_kine & inAcc ) " ,
#    "has_mu_minus = GINTREE ( ( 'mu-' == GID ) & muon_kine & inAcc) " ,
#    "has_K_plus   = GINTREE ( ( 'K+'  == GID ) & kaon_kine & inAcc) " ,
#    "has_K_minus  = GINTREE ( ( 'K-'  == GID ) & kaon_kine & inAcc) " ,
#  ]
# tightCut.Cuts = {
#    'Beauty' : '(has_mu_plus & has_K_minus) | (has_mu_minus & has_K_plus) | (has_mu_minus & has_K_minus) | (has_mu_plus & has_K_plus)'
#  } 
# EndInsertPythonCode 
# 
# Documentation: Inclusive high momentum Kmu events from Bs -> Charmonium K X decays, only one muon in acceptance
# EndDocumentation 
# 
# PhysicsWG: B2SL 
# Tested: Yes 
# CPUTime:< 1min
# Responsible: Svende Braun
# Email: svende.braun@cern.ch 
# Date: 20190409
#
Define Hp 0.49 
Define Hz 0.775 
Define Hm 0.4 
Define pHp 2.50 
Define pHz 0.0 
Define pHm -0.17
#
Alias MyJ/psi J/psi 
ChargeConj MyJ/psi MyJ/psi 
# 
Alias Mychi_c1 chi_c1 
ChargeConj Mychi_c1 Mychi_c1 
#
Alias Mychi_c0 chi_c0
ChargeConj Mychi_c0 Mychi_c0
#
Alias Mychi_c2 chi_c2
ChargeConj Mychi_c2 Mychi_c2
#
Alias      Myphi   phi 
ChargeConj Myphi   Myphi
#
Alias      Myf'_2 f'_2
ChargeConj Myf'_2 Myf'_2
# 
Alias Mypsi(2S) psi(2S) 
ChargeConj Mypsi(2S) Mypsi(2S) 
#
Decay Mychi_c1
0.3430	MyJ/psi		gamma				VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ;
Enddecay
#
Decay Mychi_c2
0.1900  MyJ/psi		gamma				PHSP ;
Enddecay
#
Decay Mychi_c0
0.0140	MyJ/psi		gamma				SVP_HELAMP 1.0 0.0 1.0 0.0 ;
Enddecay
#
Decay Myphi   
0.4920  K+ K-						PHSP; 
Enddecay
#
Decay Myf'_2   
0.4435  K+ K-						PHSP; 
Enddecay
#
Decay MyJ/psi
1.0000  mu+        mu-					PHOTOS VLL ;
Enddecay
#
Decay Mypsi(2S)
0.0080  mu+ mu-						PHOTOS VLL;
0.3467  MyJ/psi    pi+        pi-			PHOTOS VVPIPI ; 
0.1823  MyJ/psi    pi0        pi0                    	VVPIPI ;   
0.0337  MyJ/psi    eta                               	PARTWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ; 
0.0979  Mychi_c0   gamma                             	PHSP ; 
0.0975  Mychi_c1   gamma                             	VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ; 
0.0952  Mychi_c2   gamma                             	PHSP ; 
Enddecay
#
Decay B_s0sig
0.00108 MyJ/psi    Myphi				PVV_CPLH 0.02 1 Hp pHp Hz pHz Hm pHm;
0.00079 MyJ/psi    K+ 		K- 	 		PHSP ;
0.00093 MyJ/psi    K0 		K- 	 pi+  		PHSP ; 			
0.00026 MyJ/psi    Myf'_2		 		PHSP ; 				
0.00054 Mypsi(2S)  Myphi 				PVV_CPLH 0.02 1 Hp pHp Hz pHz Hm pHm; 
0.000204 Mychi_c1  Myphi 				SVV_HELAMP Hp pHp Hz pHz Hm pHm;
Enddecay 
CDecay anti-B_s0sig 
# 
End
#
