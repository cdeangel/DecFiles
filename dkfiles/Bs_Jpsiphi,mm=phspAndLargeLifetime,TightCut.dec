# EventType: 13144016
#
# Descriptor: [B_s0 -> (J/psi(1S) -> mu+ mu-) (phi(1020) -> K+ K-)]cc
#
# NickName: Bs_Jpsiphi,mm=phspAndLargeLifetime,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = '[^(B_s0 => ^(J/psi(1S) => ^mu+ ^mu-) ^(phi(1020) => ^K+ ^K-))]CC'
#
# tightCut.Cuts      =    {
#     '[B_s0]cc' : ' goodB  ' , 
#     '[K+]cc'   : ' goodKaon ' , 
#     '[mu+]cc'  : ' goodMuon ' 
#     }
# tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import ns, GeV, MeV, mrad',
#    'from GaudiKernel.PhysicalConstants import c_light',
#    'inAcc     = in_range ( 0.01 , GTHETA , 0.400 ) ',
#    'goodB     = ( GCTAU < 16e-3 * ns * c_light)', 
#    'goodKaon  = inAcc', 
#    'goodMuon  = inAcc' 
#     ]
# EndInsertPythonCode
#
# Documentation:
# Includes radiative, enlarging lifetime by 20 times, dgamma=0, PHSP mode
# EndDocumentation
#
# PhysicsWG: B2Ch
# Tested: Yes
# Responsible: Wenhua Hu 
# Email: wenhua.hu@cern.ch
# Date: 20181102
# CPUTime: < 1 min
#
# Re-define lifetimes (overrules ParticlePropertyTable)
# ParticleValue: "B_s0H  99996  530  0.0  5.36677  32.150e-12  B_s0H  0  0.00", "B_s0L  99997  350  0.0  5.36677  32.150e-12  B_s0L  0  0.00"
#
#
Alias      MyJ/psi  J/psi
Alias      MyPhi    phi
ChargeConj MyJ/psi  MyJ/psi
ChargeConj MyPhi    MyPhi
#
Decay B_s0sig
  1.000         MyJ/psi     MyPhi        PHSP;
#
Enddecay
Decay anti-B_s0sig
  1.000         MyJ/psi     MyPhi        PHSP;
Enddecay
#
Decay MyJ/psi
  1.000         mu+         mu-          PHSP;
Enddecay
#
Decay MyPhi
  1.000         K+          K-           PHSP;
Enddecay
End
