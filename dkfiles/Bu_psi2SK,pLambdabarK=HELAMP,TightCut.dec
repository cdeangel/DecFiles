# EventType: 12335151
# 
# Descriptor: [ B+ -> K+ (psi(2S) -> p+ (Lambda~0 -> p~- pi+) K-)]cc 
# 
# NickName: Bu_psi2SK,pLambdabarK=HELAMP,TightCut
#
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[B+ ==> ^K+ (Meson ==> ^X+ (Baryon => ^X- ^X+) ^X-)]CC"
# tightCut.Preambulo += [
# "from GaudiKernel.SystemOfUnits import MeV",
# "InAcc = in_range ( 0.005 , GTHETA , 0.400 )",
# "goodKpi  = ( GP > 1000 * MeV ) & ( GPT > 100 * MeV ) & InAcc",
# "goodp    = ( GP > 5000 * MeV ) & ( GPT > 200 * MeV ) & InAcc"
# ]
# tightCut.Cuts = {
# '[pi+]cc' : "goodKpi",
# '[K+]cc'  : "goodKpi",
# '[p+]cc'  : "goodp"
# }
#
# EndInsertPythonCode
# 
# Documentation:
#                 Lambda0 forced into p pi.
#                 Lambda0 -> p pi helicity amplitude from 2019 combination in PRL 123, 182301.
#                 All charged final state tracks are required to be within the LHCb acceptance.
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Liang Sun
# Email: lsun@cern.ch
# Date: 20230301
#
#
Alias      Mypsi2S psi(2S)
ChargeConj Mypsi2S Mypsi2S

Alias       MyLambda      Lambda0
Alias       MyantiLambda  anti-Lambda0
ChargeConj  MyLambda      MyantiLambda

#
Decay B+sig
  1.000     Mypsi2S          K+                 SVS; 
Enddecay
CDecay B-sig
#
Decay MyLambda
  1.000     p+            pi-                HELAMP 0.9276 0.0 0.3735 0.0;
Enddecay
CDecay MyantiLambda

Decay Mypsi2S
  0.500     p+  MyantiLambda    K-  PHSP;
  0.500     anti-p-  MyLambda    K+  PHSP;
Enddecay

#
End
#
