# EventType: 15865011
# Descriptor: [Lambda_b0 -> (Lambda_c(2595)+ -> (Lambda_c+ -> p+ K- pi+) pi+ pi-)  tau- anti-nu_tau ]cc
#
# NickName: Lb_Lc2593taunu,pKpi=cocktail,tau3pi,DecProdCut,tauolababar
#
# Cuts: DaughtersInLHCb
# CutsOptions: NeutralThetaMin 0. NeutralThetaMax 10.
#
# CPUTime: 1min
#
# Documentation: Lb->Lc3pi decay taking into account both resonances for the Lc
# and the 3pi part. Two modes using Lc* resonances are also added.
# This decfile is designed to provide the most suitable production for the
# normalisation channel of Lb->Lctaunu, tau->3pi(pi0)nu_tau.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Guy Wormser
# Email: wormser@lal.in2p3.fr
# Date: 20180109
#


Alias      MyLambda_c+             Lambda_c+
Alias      Myanti-Lambda_c-        anti-Lambda_c-
ChargeConj MyLambda_c+             Myanti-Lambda_c-
#
Alias      MySigma_c++             Sigma_c++
Alias      Myanti-Sigma_c--        anti-Sigma_c--
ChargeConj MySigma_c++             Myanti-Sigma_c--
#
Alias      MySigma_c0             Sigma_c0
Alias      Myanti-Sigma_c0        anti-Sigma_c0
ChargeConj MySigma_c0             Myanti-Sigma_c0
#
#
Alias      MySigma_c*++             Sigma_c*++
Alias      Myanti-Sigma_c*--        anti-Sigma_c*--
ChargeConj MySigma_c*++             Myanti-Sigma_c*--
#
Alias      MySigma_c*0             Sigma_c*0
Alias      Myanti-Sigma_c*0        anti-Sigma_c*0
ChargeConj MySigma_c*0             Myanti-Sigma_c*0
#
Alias      MyLambda_c(2593)+       Lambda_c(2593)+
Alias      Myanti-Lambda_c(2593)-  anti-Lambda_c(2593)-
ChargeConj MyLambda_c(2593)+       Myanti-Lambda_c(2593)-
#
Alias      MyK*0                   K*0
Alias      Myanti-K*0              anti-K*0
ChargeConj MyK*0                   Myanti-K*0
#
Alias      MyLambda(1520)0         Lambda(1520)0
Alias      Myanti-Lambda(1520)0    anti-Lambda(1520)0
ChargeConj MyLambda(1520)0         Myanti-Lambda(1520)0
#
Alias      Mya_1-                  a_1-
Alias      Mya_1+                  a_1+
ChargeConj Mya_1+                  Mya_1-
#
Alias      Myf_2                   f_2
ChargeConj Myf_2                   Myf_2
#
#
Alias      Mytau-                  tau-
Alias      Mytau+                  tau+
ChargeConj Mytau+                  Mytau-
#
#
Decay Lambda_b0sig
  0.63    MyLambda_c(2593)+      Mytau-   anti-nu_tau     PHOTOS   Lb2Baryonlnu  1 1 1 1; 


Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
# Lc->pKpi:
  0.02800 p+                 K-      pi+   PHSP;
  0.016   p+                 Myanti-K*0    PHSP;
  0.00860 Delta++            K-            PHSP;
  0.00414 MyLambda(1520)0    pi+           PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#

Decay MyLambda_c(2593)+

0.096585366 MySigma_c++ pi-                                   PHSP; #[Reconstructed PDG2011]
0.096585366 MySigma_c0 pi+                                    PHSP; #[Reconstructed PDG2011]
0.190000000 MyLambda_c+ pi+     pi-                           PHSP; #[Reconstructed PDG2011]
0.240000000 MySigma_c*++ pi-                                  PHSP;  #[New mode added] #[Reconstructed PDG2011]
0.240000000 MySigma_c*0 pi+                                   PHSP;
Enddecay

CDecay Myanti-Lambda_c(2593)-
#
Decay MySigma_c++
1. MyLambda_c+ pi+                           PHSP; #[Reconstructed PDG2011]
Enddecay
CDecay Myanti-Sigma_c--
#
Decay MySigma_c0
1. MyLambda_c+ pi-                         PHSP; #[Reconstructed PDG2011]
Enddecay
CDecay Myanti-Sigma_c0
#
Decay MySigma_c*++
1. MyLambda_c+ pi+                           PHSP; #[Reconstructed PDG2011]
Enddecay
CDecay Myanti-Sigma_c*--
#
Decay MySigma_c*0
1. MyLambda_c+ pi-                           PHSP; #[Reconstructed PDG2011]
Enddecay
CDecay Myanti-Sigma_c*0
#
Decay MyK*0
  0.6667 K+                  pi-           VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Mya_1+
  1.000   rho0               pi+           VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-
#
Decay Myf_2
  1.000  pi+                 pi-           TSS ;
Enddecay
#
# Tauola steering options
Define TauolaCurrentOption 1
Define TauolaBR1 1.0 
# tau -> pi- pi+ pi- nu_tau
Decay Mytau-
 1. TAUOLA 5;
Enddecay
CDecay Mytau+
#
End
