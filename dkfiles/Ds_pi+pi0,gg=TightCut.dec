# EventType: 23101401
#
# Descriptor: [D_s- -> pi- ( pi0 -> gamma gamma )]cc 
#
# NickName: Ds_pi+pi0,gg=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '^[ D_s+ -> ^pi+ ( pi0 -> gamma gamma )]CC'
# tightCut.Cuts      =    {
#     '[pi+]cc'  : ' inAcc & piCuts',
#     '[D_s+]cc' : ' Dcuts ' }
# tightCut.Preambulo += [
#     'inAcc = in_range ( 0.005, GTHETA, 0.400 ) ' , 
#     'piCuts = ( (GPT > 200 * MeV) & ( GP > 600 * MeV))',
#     'Dcuts = (GPT > 1000 * MeV)' ]
# EndInsertPythonCode
#
# Documentation: Inclusive production of Ds-. Ds- forced to decay to pi- pi0 as phase space, then pi0 to gamma gamma. Used to look at signal when a single gamma converts into e+ e- (Geant) inside LHCb. Generator level cuts on pi and D meson.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Tom Hadavizadeh
# Email: tom.hadavizadeh@cern.ch
# Date: 20170302
#
Alias      MyPi0        pi0
ChargeConj MyPi0        MyPi0
#
Decay D_s-sig
  1.00   MyPi0   pi-    PHSP;
Enddecay
CDecay D_s+sig
#
Decay MyPi0
  1.00  gamma gamma     PHSP;
Enddecay
#
End
#
