# EventType: 11496002
# 
# Descriptor:  [B0 ==> ( D*+ => ( D0 => K- pi+ ) pi+ ) ( D~0 => K+ pi- ) K- ]CC 
# 
# NickName: Bd_Dst+D0KX=TightCuts,mD0D0 
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: The decay B0 -> ( D*+D~0, D*+D~*0 ) K- with D0-> K- pi+
#                with the tight generator cuts  
#                the mass of D0D~0 system is required to be <3.8GeV 
# EndDocumentation
#
# Sample: SignalRepeatedHadronization
#
#InsertPythonCode:
# 
# from Configurables import LoKi__GenCutTool, ToolSvc
# from Gauss.Configuration import *
# generation = Generation()
# signal     = generation.SignalRepeatedHadronization
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut            = signal.TightCut
# tightCut.Decay      = '^[ ( B0 | B~0 ) ==> ^( D0 => ^K- ^pi+ ) ^( D~0 => ^K+ ^pi- ) ^K- pi+ ]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV',
#     'inAcc          =  in_range ( 0.005 , GTHETA , 0.400 )   ' ,
#     'inEta          =  in_range ( 1.95  , GETA   , 5.050 )   ' ,
#     'goodTrack      =  inAcc & inEta & ( GPT > 190 * MeV )   ' ,
#     'mass_DD        =  GMASS ( "D0" == GID , "D~0" ==  GID ) ' ,
# ]
# tightCut.Cuts       =    {
#     '[pi+]cc'       : 'goodTrack & in_range ( 3 * GeV , GP , 200 * GeV ) ' ,
#     '[K+]cc'        : 'goodTrack & in_range ( 3 * GeV , GP , 200 * GeV ) ' ,
#     '[D0]cc'        : 'in_range ( 1.9 , GY , 4.6) ' ,
#     '[B0]cc'        : 'in_range ( 1.9 , GY , 4.6) & ( mass_DD < 3.8 * GeV ) ' ,
#     }
# # Generator efficiency histos:
# tightCut.XAxis = ( "GPT/GeV" , 0.0 , 25.0 , 25 )
# tightCut.YAxis = ( "GY"      , 2.0 ,  4.5 , 10 )
# print tightCut 
# EndInsertPythonCode
#
# PhysicsWG:   Onia 
# Tested:      Yes
# Responsible: Vanya BELYAEV
# Email:       Ivan.Belyaev@cern.ch
# Date:        20190707
# CPUTime:     10min


Alias           My-D0           D0
Alias           My-anti-D0      anti-D0
ChargeConj      My-D0           My-anti-D0

Alias           My-D*0          D*0
Alias           My-anti-D*0     anti-D*0
ChargeConj      My-D*0          My-anti-D*0

Alias           My-D*+          D*+
Alias           My-D*-          D*-
ChargeConj      My-D*+          My-D*-

Decay B0sig
  0.50          My-D*+ My-anti-D0   K-  PHSP ;
  0.50          My-D*+ My-anti-D*0  K-  PHSP ;
Enddecay
CDecay anti-B0sig

Decay My-D0
  1.000         K-     pi+            PHSP ; 
Enddecay
CDecay My-anti-D0

Decay My-D*0
  0.353         My-D0  gamma          VSP_PWAVE ; 
  0.647         My-D0  pi0            VSS ; 
Enddecay
CDecay My-anti-D*0

Decay My-D*+
  1.000         My-D0  pi+            VSS ; 
Enddecay
CDecay My-D*-

#
End
#

