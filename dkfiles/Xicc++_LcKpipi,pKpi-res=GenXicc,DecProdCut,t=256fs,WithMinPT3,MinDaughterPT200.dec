# EventType:  26266052
#
# Descriptor: [ Xi_cc++ -> (Lambda_c+ -> p K- pi+) K- pi+ pi+ ]cc
#
# NickName: Xicc++_LcKpipi,pKpi-res=GenXicc,DecProdCut,t=256fs,WithMinPT3,MinDaughterPT200
#
# Production: GenXicc
#
# Cuts: XiccDaughtersInLHCbAndWithMinPT
#
# CutsOptions: MinXiccPT 3000*MeV MinDaughterPT 200*MeV
#
# ParticleValue: "Xi_cc++                506        4422   2.0      3.62140000      2.560000e-13                    Xi_cc++        4422      0.00000000", "Xi_cc~--               507       -4422  -2.0      3.62140000      2.560000e-13               anti-Xi_cc--       -4422      0.00000000" 
#
# CPUTime: < 1 min
#
# Documentation: 
# Xicc++ decay to Lambda_c+ K- pi+ pi+ by phase space model, with the Lambda_c+ decaying to p K- pi+.
# Lambda_c+ decays via a 4-component pseudoresonant model,
# the mass of Xi_cc++ is set to 3.6214 GeV, the lifetime is set to 256 fs.
# All daughters of Xicc are required to be in the acceptance of LHCb and with PT>200 MeV
# and the Xicc++ PT is required to be larger than 3000 MeV/c. 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Yixiong Zhou
# Email: yixiong.zhou@cern.ch
# Date: 20181216
#
Alias MyLambda_c+ Lambda_c+
Alias Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+ Myanti-Lambda_c-
#
# Define Lambda(1520)0
Alias      MyLambda(1520)0       Lambda(1520)0
Alias      Myanti-Lambda(1520)0  anti-Lambda(1520)0
ChargeConj MyLambda(1520)0       Myanti-Lambda(1520)0
#
# Define K*0
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
# Define Delta++
Alias      MyDelta++      Delta++
Alias      Myanti-Delta-- anti-Delta--
ChargeConj MyDelta++      Myanti-Delta--
#
Decay Xi_cc++sig
  1.00   MyLambda_c+  K-	pi+    pi+     PHSP;
Enddecay
CDecay anti-Xi_cc--sig
#
Decay MyLambda_c+
    0.02800         p+      K-      pi+          PHSP;
    0.01065         p+      Myanti-K*0           PHSP;
    0.00860         MyDelta++ K-                 PHSP;
    0.00414         MyLambda(1520)0 pi+          PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyK*0
    0.6657      K+  pi-                          VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyLambda(1520)0
  0.23   p+     K-                             PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
Decay MyDelta++
  1.000 p+ pi+ PHSP;
Enddecay
CDecay Myanti-Delta--
#      
End
