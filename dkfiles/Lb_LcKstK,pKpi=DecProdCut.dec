# EventType: 15266068
# 
# Descriptor: [Lambda_b0 -> K- (K*0 -> K+ pi-) (Lambda_c+ -> p+ K- pi+)]cc 
# 
# NickName: Lb_LcKstK,pKpi=DecProdCut 
#
# Cuts: DaughtersInLHCb 
#
# Documentation: Lb -> Lc K K pi with Lc -> p K pi, decay products in acceptance. Includes K*0 resonance in Lambda_c decay. Lambda-b decays to 100% in Lambda_c. Lb decay through the K* resonance.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: <1min
# Responsible: Mengzhen Wang 
# Email: mengzhen.wang@cern.ch
# Date: 20180607
#
Alias      MyLambda_c+          Lambda_c+
Alias      Myanti-Lambda_c-     anti-Lambda_c-
ChargeConj MyLambda_c+          Myanti-Lambda_c-
#
Alias      MyK*0                K*0
Alias      Myanti-K*0           anti-K*0
ChargeConj MyK*0                Myanti-K*0
#
Alias      MyDelta++            Delta++
Alias      Myanti-Delta--       anti-Delta--
ChargeConj MyDelta++            Myanti-Delta--
#
Alias      MyLambda(1520)0      Lambda(1520)0
Alias      Myanti-Lambda(1520)0 anti-Lambda(1520)0
ChargeConj MyLambda(1520)0      Myanti-Lambda(1520)0
#
Decay Lambda_b0sig
 1.00 MyLambda_c+	K-  MyK*0	PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
 0.17 MyDelta++	K-				PHSP;
 0.21 Myanti-K*0	p+			PHSP;
 0.08 MyLambda(1520)0	pi+			PHSP;
 0.54 p+		K-	pi+		PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyK*0
 1.00 K+	pi-				VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyDelta++
 1.00 p+	pi+				PHSP;
Enddecay
CDecay Myanti-Delta--
#
Decay MyLambda(1520)0
 1.00 p+	K-				PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
End
