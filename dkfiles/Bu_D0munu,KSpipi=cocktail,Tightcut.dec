# This is the decay file for the decay B+ => (D~0 -> (KS0 -> pi+ pi-) pi+ pi-) mu+ nu_mu
# 
# EventType: 12875524
#
# Descriptor: [B+ => (D~0 -> (KS0 -> pi+ pi-) pi+ pi-) nu_mu mu+]cc
#
# NickName: Bu_D0munu,KSpipi=cocktail,Tightcut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = '[B+ --> (D~0 => (KS0 => pi+ pi-) pi+ pi-) mu+ ...]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, centimeter',
#     'inAcc             = in_range (0.005, GTHETA, 0.400)'  ,
#     'inEta             = in_range (1.85 , GETA  , 5.1  )'  ,
#     'pions             = "pi+" == GABSID '                 ,
#     'pionInAcc         = pions & inAcc & inEta'            ,
#     'Bdecay_vertexZ    = GFAEVX( GVZ, 0)'                  ,
#     'Bprod_vertexZ     = GFAPVX( GVZ, 0)'                  ,
#     'goodBvx           = ( Bdecay_vertexZ - Bprod_vertexZ ) > 1.6 * millimeter' ,
#     'goodKS            = (GP > 1.9   * MeV ) & (GPT > 150 * MeV )' ,
#     'goodD0pions       = GNINTREE  ( pionInAcc & ( GP > 1.90 * GeV ) , 1 ) == 2 ',
#     'goodKSpions       = GNINTREE  ( pionInAcc & ( GP > 2.50 * GeV ) , 1 ) == 2 ',
#     'decayBeforeTT     = GFAEVX ( GVZ , 0 ) < 240 * centimeter ',
#     'goodMuon          = ( GP > 2.9 * GeV) & ( GPT > 700 * MeV) & inAcc & inEta ',
#     'hasKs             = GINTREE ( ( "KS0" == GID ) & goodKSpions & decayBeforeTT )' ,
#     'has2pions         = GNINTREE  ( pionInAcc & ( GP > 1.90 * GeV ) , 1 ) == 2 '        ,
#     'goodD0            = ( GP > 12 * GeV ) & (GPT > 1.9 * GeV ) & has2pions & hasKs '     ,
#     'hasD0             = GINTREE ( ( "D0"  ==  GABSID ) & goodD0   )' ,
#     'hasMu             = GINTREE ( ( "mu+" ==  GABSID ) & goodMuon )' ,
#     'goodB             = GBEAUTY & hasD0 & hasMu & goodBvx  '
# ]
# tightCut.Cuts      =    {
#     '[B+]cc'         : 'goodB'
#     }
# EndInsertPythonCode
#
# Documentation: Sum of D~0 mu+ anti-nu_mu X;D0 forced into KSpipi, tight cuts
# EndDocumentation 
#
# CPUTime: < 10 min
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Martha Hilton
# Email: martha.hilton@cern.ch
# Date: 20220124
#
##############
Alias myK_S0  K_S0
ChargeConj myK_S0 myK_S0
#
Alias      MyD0         D0
Alias      MyAntiD0     anti-D0
ChargeConj MyD0         MyAntiD0
#
Alias      MyD*-        D*-
Alias      MyD*+        D*+
ChargeConj MyD*-        MyD*+
#
Alias      MyD*0        D*0
Alias      MyAntiD*0    anti-D*0
ChargeConj MyD*0        MyAntiD*0
#
Alias      MyD_10         D_10
Alias      MyAntiD_10     anti-D_10
ChargeConj MyD_10         MyAntiD_10
#
Alias      MyD_1+         D_1+
Alias      MyD_1-         D_1-
ChargeConj MyD_1+         MyD_1-
#
Alias      MyD_0*+         D_0*+
Alias      MyD_0*-         D_0*-
ChargeConj MyD_0*+         MyD_0*-
#
Alias      MyD_0*0         D_0*0
Alias      MyAntiD_0*0     anti-D_0*0
ChargeConj MyD_0*0         MyAntiD_0*0
#
Alias      MyD'_10         D'_10
Alias      MyAntiD'_10     anti-D'_10
ChargeConj MyD'_10         MyAntiD'_10
#
Alias      MyD'_1+         D'_1+
Alias      MyD'_1-         D'_1-
ChargeConj MyD'_1+         MyD'_1-
#
Alias      MyD_2*+         D_2*+
Alias      MyD_2*-         D_2*-
ChargeConj MyD_2*+         MyD_2*-
#
Alias      MyD_2*0         D_2*0
Alias      MyAntiD_2*0     anti-D_2*0
ChargeConj MyD_2*0         MyAntiD_2*0
#
Decay B-sig
  2.3500   MyD0     mu-  anti-nu_mu        PHOTOS  ISGW2;               
  5.6600   MyD*0    mu-  anti-nu_mu        PHOTOS  ISGW2;               
  0.2500   MyD_0*0     mu-  anti-nu_mu     PHOTOS  ISGW2;               
  0.2700   MyD'_10     mu-  anti-nu_mu     PHOTOS  ISGW2;               
  0.3030   MyD_10      mu-  anti-nu_mu     PHOTOS  ISGW2;               
  0.1010   MyD_2*0     mu-  anti-nu_mu     PHOTOS  ISGW2;               
  0.1700   MyD0  pi+ pi-  mu-  anti-nu_mu  PHOTOS  PHSP;       
  0.0800   MyD*0  pi+ pi-  mu-  anti-nu_mu PHOTOS  PHSP;       
  0.6000   MyD*+  pi-  mu-  anti-nu_mu     PHOTOS  GOITY_ROBERTS;       
Enddecay
CDecay B+sig
#
SetLineshapePW MyD_1+ MyD*+ pi0 2
SetLineshapePW MyD_1- MyD*- pi0 2
SetLineshapePW MyD_10 MyD*+ pi- 2
SetLineshapePW MyAntiD_10 MyD*- pi+ 2
#
SetLineshapePW MyD_2*+ MyD*+ pi0 2
SetLineshapePW MyD_2*- MyD*- pi0 2
SetLineshapePW MyD_2*0 MyD*+ pi- 2
SetLineshapePW MyAntiD_2*0 MyD*- pi+ 2
#
# Force Ks -> pi+ pi- to save generating unhelpful events:
Decay myK_S0
1.000     pi+  pi-                      PHSP;
Enddecay
#
Decay MyD0
  1.000   myK_S0  pi+ pi-                              PHOTOS PHSP;
Enddecay
CDecay MyAntiD0
#
Decay MyD*0
  0.619   MyD0  pi0                            PHOTOS VSS;
  0.381   MyD0  gamma                          PHOTOS VSP_PWAVE;
Enddecay
CDecay MyAntiD*0
#
Decay MyD*+
  0.6770    MyD0  pi+                          PHOTOS VSS;
Enddecay
CDecay MyD*-
#
Decay MyD_0*+ 
  0.533     MyD0  pi+                          PHOTOS PHSP;
  0.0271     MyD*+ pi0 pi0                     PHOTOS PHSP;
  0.0542     MyD*+ pi+ pi-                     PHOTOS PHSP;
  0.080     MyD*0 pi+ pi0                      PHOTOS PHSP;
Enddecay
CDecay MyD_0*-
#
Decay MyD_0*0
  0.267     MyD0  pi0                          PHSP;
  0.040     MyD*0 pi0 pi0                      PHOTOS PHSP;
  0.080     MyD*0 pi+ pi-                      PHOTOS PHSP;
  0.0542     MyD*+ pi- pi0                     PHOTOS PHSP;
Enddecay
CDecay MyAntiD_0*0
#
Decay MyD'_1+
  0.1693    MyD*+ pi0                          PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
  0.500     MyD*0 pi+                          PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
  0.104     MyD0 pi+ pi0                       PHOTOS PHSP;
Enddecay
CDecay MyD'_1-
#
Decay MyD'_10
  0.250    MyD*0 pi0                           PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
  0.3385    MyD*+ pi-                          PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
  0.052     MyD0 pi0 pi0                       PHOTOS PHSP;
  0.104     MyD0 pi+ pi-                       PHOTOS PHSP;
Enddecay
CDecay MyAntiD'_10
#
Decay MyD_1+
  0.1354    MyD*+ pi0                          PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
  0.400    MyD*0 pi+                           PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
  0.1146    MyD_0*0 pi+                        PHOTOS PHSP;
  0.0903   MyD_0*+ pi0                         PHOTOS PHSP;
Enddecay
CDecay MyD_1-
#
Decay MyD_10
  0.2708   MyD*+ pi-                           PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
  0.200    MyD*0 pi0                           PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
  0.1806    MyD_0*+ pi-                        PHOTOS PHSP;
  0.0573    MyD_0*0 pi0                        PHSP;
Enddecay
CDecay MyAntiD_10
#
Decay MyD_2*+
  0.0587    MyD*+ pi0                          PHOTOS TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
  0.173    MyD*0 pi+                           PHOTOS TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
  0.2667    MyD0  pi+                          PHOTOS TSS;
  0.0647    MyD_0*0 pi+                        PHOTOS PHSP;
  0.0509    MyD_0*+ pi0                        PHOTOS PHSP;
  0.0027     MyD*+ pi0 pi0                     PHOTOS PHSP;
  0.0054    MyD*+ pi+ pi-                      PHOTOS PHSP;
  0.008     MyD*0 pi+ pi0                      PHOTOS PHSP;
  0.032     MyD0 pi+ pi0                       PHOTOS PHSP;
Enddecay
CDecay MyD_2*-
#
Decay MyD_2*0
  0.1173    MyD*+ pi-                          PHOTOS TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
  0.0867    MyD*0 pi0                          TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
  0.133    MyD0  pi0                           TSS;
  0.1019    MyD_0*+ pi-                        PHOTOS PHSP;
  0.0323    MyD_0*0 pi0                        PHSP;
  0.004     MyD*0 pi0 pi0                      PHOTOS PHSP;
  0.008     MyD*0 pi+ pi-                      PHOTOS PHSP;
  0.0054     MyD*+ pi- pi0                     PHOTOS PHSP;
  0.016     MyD0 pi0 pi0                       PHOTOS PHSP;
  0.032     MyD0 pi+ pi-                       PHOTOS PHSP;
Enddecay
CDecay MyAntiD_2*0
#
End
#
