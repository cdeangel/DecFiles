# EventType: 27184410
#
# Descriptor: [D*0 -> (D0 -> K- pi+) (pi0 -> e+ e- gamma)]cc
#
# NickName: Dst0_D0pi0,Kpi=TightCut,e+e-g=Dalitz
#
#
# Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
#
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
#tightCut = gen.SignalPlain.TightCut
#tightCut.Decay = '^[D*(2007)0 => ^(D0 ==> ^K- ^pi+) ^(pi0 ==> ^e+ ^e- ^gamma)]CC'
#
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import MeV" ,
#     "from LoKiCore.functions import in_range"
# ]
#tightCut.Cuts = {
#   '[D*(2007)0]cc'   : '(GP >  6000 * MeV)',
#   '[D0]cc'          : '(GP >  3000 * MeV)',
#   '[K-]cc'          : '(GP >  1500 * MeV) & (GPT > 50 * MeV) & (in_range(0.010, GTHETA, 0.400))',
#   '[pi+]cc'         : '(GP >  1500 * MeV) & (GPT > 50 * MeV) & (in_range(0.010, GTHETA, 0.400))',
#   '[e+]cc'          : '(GP >  1500 * MeV) & (GPT > 50 * MeV) & (in_range(0.010, GTHETA, 0.400))',
#   }
#
#EndInsertPythonCode
#
# Documentation: D*0 -> D0pi0 decay file, with pi0 -> e+ e- gamma (Dalitz), asking the charged final state particles to be in the acceptance and have enough momentum to make it into long tracks.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Michel De Cian
# Email: michel.de.cian@cern.ch
# Date: 20190522 
# CPUTime: < 1min
#
Alias      Myanti-D0   anti-D0
Alias      MyD0        D0
ChargeConj MyD0        Myanti-D0
Alias      MyPi0       pi0
ChargeConj MyPi0       MyPi0
##
Decay D*0sig
1. MyD0  MyPi0    VSS;
Enddecay
CDecay anti-D*0sig
#
Decay MyD0
1.     K-  pi+        PHSP;
Enddecay
CDecay Myanti-D0
#
Decay MyPi0
1.  e+ e- gamma      PI0_DALITZ;
Enddecay
#
End

