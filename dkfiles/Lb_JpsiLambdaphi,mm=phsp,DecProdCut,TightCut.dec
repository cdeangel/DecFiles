# EventType: 15146135
#
# Descriptor: [Lambda_b0 ->  (J/psi(1S) -> mu+ mu-) (phi -> K+ K-) (Lambda0 -> p+ pi-) ]cc
#
# NickName: Lb_JpsiLambdaphi,mm=phsp,DecProdCut,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: < 1 min
#
# Documentation: Phase space decay for Lambda_b0 to Jpsi Lambda phi. Decay products in acceptance, and L0(Di)Muon TOS emulation on the Jpsi
# Lambda0 is forced into p pi
# EndDocumentation
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Alessio Piucci
# Email: alessio.piucci@cern.ch
# Date: 20180627
#
# InsertPythonCode:
## 
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *                                                                                                                                                                     
# gen = Generation() 
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
## 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = '^[Lambda_b0 ==>  ^(J/psi(1S) ==> ^mu+ ^mu-) (phi(1020) ==> ^K+ ^K-) (Lambda0 ==> ^p+ ^pi-) ]CC'
##
# tightCut.Cuts = {
#     '[p+]cc'    : ' good_proton ' ,
#     '[pi-]cc'   : ' good_pion ' ,
#     '[K+]cc'    : ' good_kaon ' ,
#     '[mu+]cc'   : ' good_muon ' ,
#     '[J/psi(1S)]cc' : '( l0muon | l0dimuon )'
#    }
##
# tightCut.Preambulo += [
#    "from GaudiKernel.SystemOfUnits import GeV",
#    "inAcc_charged = in_range ( 0.010 , GTHETA , 0.400 )" ,
#    "inEta         = in_range ( 1.8   , GETA   , 5.1   )" ,
#
#    "good_pion   = ('pi+' == GABSID) & inAcc_charged" ,
#    "good_proton = ('p+' == GABSID) & inAcc_charged" ,
#    "good_kaon   = ('K+' == GABSID) & inAcc_charged" ,
#    "good_muon   = ('mu+' == GABSID) & inAcc_charged & inEta" ,
#
#    "l0muon    = GINTREE(good_muon &  (GPT > 1.6 * GeV))",
#    "l0dimuon  = (2 == GNINTREE(good_muon)) & (GCHILD(GPT, 'mu+' == GID) * GCHILD(GPT, 'mu-' == GID) > 2.1 * GeV * GeV)"
#    ]
# EndInsertPythonCode

Alias      MyJpsi J/psi
ChargeConj MyJpsi MyJpsi

Alias      MyLambda          Lambda0
Alias      Myanti-Lambda     anti-Lambda0
ChargeConj MyLambda          Myanti-Lambda

Alias      Myphi   phi
ChargeConj Myphi   Myphi

#
Decay Lambda_b0sig
  1.000    MyJpsi MyLambda Myphi    PHSP;
Enddecay
CDecay anti-Lambda_b0sig

Decay MyJpsi
  1.000   mu+ mu-                    VLL;
Enddecay

Decay MyLambda
  1.000   p+ pi-                     PHSP;
Enddecay
CDecay Myanti-Lambda
  
Decay Myphi
  1.000     K+ K-                    PHSP;
Enddecay
#
End
#

