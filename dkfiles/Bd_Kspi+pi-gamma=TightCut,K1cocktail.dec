# EventType: 11204300 
#
# Descriptor: [B0 -> (K_10 -> (K_S0 -> pi+ pi-) (rho0 -> pi+ pi-)) gamma]cc
#
# NickName: Bd_Kspi+pi-gamma=TightCut,K1cocktail
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool as GenCutTool 
#
# gen = Generation() 
# gen.SignalRepeatedHadronization.setProp('MaxNumberOfRepetitions', 5000)
# gen.SignalRepeatedHadronization.addTool ( GenCutTool , 'TightCut' ) 
# 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[(Beauty => (KS0 => ^pi+ ^pi-) ^pi+ ^pi- ^gamma), (Beauty => (KS0 => ^pi- ^pi+) ^pi- ^pi+ ^gamma), (Beauty => (K_1(1270)0 => (K*(892)+ => (KS0 => ^pi+ ^pi-) ^pi+) ^pi-) ^gamma), (Beauty => (K_1(1270)~0 => (K*(892)- => (KS0 => ^pi- ^pi+) ^pi-) ^pi+) ^gamma), (Beauty => (K_1(1270)0 => (KS0 => ^pi+ ^pi-) (rho(770)0 => ^pi+ ^pi-)) ^gamma), (Beauty => (K_1(1270)~0 => (KS0 => ^pi- ^pi+) (rho(770)0 => ^pi- ^pi+)) ^gamma)]'
# tightCut.Cuts      =    {
#     '[pi+]cc'        : ' inAcc' , 
#     '[K+]cc'         : ' inAcc' , 
#     'gamma'          : ' goodPhoton' } 
#
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter',
#     'inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     'goodPhoton = ( GPT > 1.5 * GeV ) & inAcc' ]
#
# EndInsertPythonCode
#
# Documentation: for TD-CPV KsPiPig, cocktail of K1-> rho0 Ks and K* pi, in acceptance, with gamma PT > 1.5
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20180527
#
Alias      MyK_10       K_10
Alias      Myanti-K_10  anti-K_10
ChargeConj MyK_10       Myanti-K_10
#
Alias      MyK*-      K*-
Alias      MyK*+      K*+
ChargeConj MyK*+      MyK*-
#
Alias      MyK0s  K_S0
ChargeConj MyK0s  MyK0s
#
Alias      Myrho0     rho0
ChargeConj Myrho0     Myrho0
#

LSNONRELBW Myrho0
BlattWeisskopf Myrho0 0.0
Particle Myrho0 0.775 0.15
ChangeMassMin Myrho0 0.2
ChangeMassMax Myrho0 2.0

LSNONRELBW MyK_10
BlattWeisskopf MyK_10 0.0
Particle MyK_10 1.272 0.15
ChangeMassMin MyK_10 0.5
ChangeMassMax MyK_10 3.6

LSNONRELBW Myanti-K_10
BlattWeisskopf Myanti-K_10 0.0
Particle Myanti-K_10 1.272 0.15
ChangeMassMin Myanti-K_10 0.5
ChangeMassMax Myanti-K_10 3.6

LSNONRELBW MyK*-
BlattWeisskopf MyK*- 0.0
Particle MyK*- 0.892 0.06
ChangeMassMin MyK*- 0.5
ChangeMassMax MyK*- 3.5

LSNONRELBW MyK*+
BlattWeisskopf MyK*+ 0.0
Particle MyK*+ 0.892 0.06
ChangeMassMin MyK*+ 0.5
ChangeMassMax MyK*+ 3.5


Decay B0sig
 0.34   MyK0s pi+ pi- gamma PHSP;
 0.66   MyK_10   gamma     PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyK_10
  0.5   MyK*+    pi-       PHSP;
  0.5   MyK0s    Myrho0       PHSP;
Enddecay
CDecay Myanti-K_10
#
Decay MyK*+
  1.0   MyK0s    pi+       PHSP;
Enddecay
CDecay MyK*-
#
Decay MyK0s
  1.0   pi+      pi-       PHSP;
Enddecay
#
Decay Myrho0
  1.0   pi+      pi-       PHSP;
Enddecay
#

End
