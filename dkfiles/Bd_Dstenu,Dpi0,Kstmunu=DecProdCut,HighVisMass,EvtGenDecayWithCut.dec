# EventType: 11574489
#
# Descriptor: [B0 -> (D*- -> (D- -> (K*0 -> K+ pi-) mu- anti-nu_mu) pi0) e+ nu_e]cc
#
# NickName: Bd_Dstenu,Dpi0,Kstmunu=DecProdCut,HighVisMass,EvtGenDecayWithCut
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
# #
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(B0 => (D*(2010)- => (D- => (K*(892)0 => K+ pi-) mu- nu_mu~) pi0) e+ nu_e)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[B0]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += [
#     "visMass = ( ( GMASS ( 'e-' == GABSID , 'mu-' == GABSID, 'K+' == GABSID, 'pi+' == GABSID ) ) > 4200 * MeV )",
# ]
# EndInsertPythonCode
#
# Documentation: background for B0 -> K* e mu LFV search
# selected to have a visible mass larger than 4.2 GeV using EvtGenDecayWithCutTool
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Jan-Marc Basels
# Email: jan-marc.basels@cern.ch
# Date: 20190809
# CPUTime: < 1 min
#

Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0

Alias      MyD*-      D*-
Alias      MyD*+      D*+
ChargeConj MyD*-      MyD*+

Alias      MyD-       D-
Alias      MyD+       D+
ChargeConj MyD-       MyD+

Decay B0sig
  #HQET2 parameter as for B->D*lnu, taken from Spring 2019 HFLAV averages:
  #https://hflav-eos.web.cern.ch/hflav-eos/semi/spring19/html/ExclusiveVcb/exclBtoDstar.html
  1.000    MyD*- e+ nu_e              PHOTOS HQET2 1.122 0.921 1.270 0.852; #rho^2 (ha1 unchanged) R1 and R2 as of HFLAG Spring 2019, normalisation factor ha1 has no impact on kinematics
Enddecay
CDecay anti-B0sig
#
Decay MyD*-
  1.000    MyD- pi0                   PHOTOS VSS;
Enddecay
CDecay MyD*+
#
Decay MyD-
  1.000    MyK*0 mu- anti-nu_mu       PHOTOS ISGW2;
Enddecay
CDecay MyD+
#
Decay MyK*0
  1.000    K+ pi-                     PHOTOS VSS;
Enddecay
CDecay Myanti-K*0
#
End
#
