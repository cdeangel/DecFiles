# EventType: 12565012
#
# Descriptor: [B+ -> (Lambda_c(2595)+ -> (Lambda_c+ -> p+ K- pi+) pi+ pi-) H_30 ]cc
#
# NickName: Bplus_PsiDMLambdac2595,Lambdac2595_pipiLambdacplus,Lambdacplus_pKpi=FullGenEvtCut,mPsiDM=2000MeV
#
# Cuts: None
# FullEventCuts: LoKi::FullGenEventCut/BplustoDM
#
# Documentation:
#   Decay a B+ to a Lambda_c(2595)+ -> pipiLambda_c (Lambda_c -> pKpi) and a redefined H_30 for our need, acting the latter as stable Dark Matter candidate of 2.0 GeV.
# EndDocumentation
#
# PhysicsWG: Exotica
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Saul Lopez
# Email: saul.lopez.solino@cern.ch
# Date: 20210930
#
#
# InsertPythonCode:
# from Configurables import LHCb__ParticlePropertySvc, LoKi__GenCutTool
# LHCb__ParticlePropertySvc().Particles = [
# "H_30     89       36      0.0     2.000000        1.000000e+16    A0      36      0.00"
# ]
# ## Generator level cuts:
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool( LoKi__FullGenEventCut, "BplustoDM" )
# tracksInAcc = Generation().BplustoDM
# tracksInAcc.Code = " count ( isGoodB ) > 0 "
# ### - HepMC::IteratorRange::descendants   4
# tracksInAcc.Preambulo += [ "from GaudiKernel.SystemOfUnits import GeV, mrad"
#                          , "inAcc = in_range(1.9, GETA, 5.0)" 
#                          , "isGoodKaon     = ( ( GPT > 0.25*GeV ) & inAcc & ( 'K+' == GABSID ) )"
#                          , "isGoodPiC      = ( ( GPT > 0.25*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodPi2595   = ( ( GPT > 0.10*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodP        = ( ( GPT > 0.25*GeV ) & inAcc & ( 'p+' == GABSID ) )"
#                          , "isGoodLc       = ( ( 'Lambda_c+' == GABSID ) & ( GNINTREE( isGoodKaon, 1 ) > 0 ) & ( GNINTREE( isGoodP, 1 ) > 0 ) & ( GNINTREE( isGoodPiC, 1 ) > 0 ) )"
#                          , "isGoodLc2595   = ( ( 'Lambda_c(2595)+' == GABSID ) & ( GNINTREE( isGoodLc, 1 ) > 0) & ( GNINTREE ( isGoodPi2595, 1 ) > 1 ) )"
#                          , "isGoodB        = ( ( 'B+' == GABSID ) & ( GNINTREE( isGoodLc2595, 1 ) > 0 ) )" ]
# EndInsertPythonCode
#
Alias      MyLambda_c(2593)+       Lambda_c(2593)+
Alias      Myanti-Lambda_c(2593)-       anti-Lambda_c(2593)-
ChargeConj MyLambda_c(2593)+       Myanti-Lambda_c(2593)-
#
Alias       MyLambda_c+        Lambda_c+
Alias  Myanti-Lambda_c-        anti-Lambda_c-
ChargeConj      MyLambda_c+    Myanti-Lambda_c-
#
Alias  MyH_30     A0
Alias  Myanti-H_30    A0
ChargeConj MyH_30   Myanti-H_30
#
Decay B+sig
    1.000   MyLambda_c(2593)+   MyH_30    PHSP;
Enddecay
CDecay B-sig
#
Decay MyLambda_c(2593)+
    1.000      MyLambda_c+ pi+ pi-             PHSP;
Enddecay
CDecay Myanti-Lambda_c(2593)-
#
Decay MyLambda_c+
    1.000       p+  K-  pi+      PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
End
