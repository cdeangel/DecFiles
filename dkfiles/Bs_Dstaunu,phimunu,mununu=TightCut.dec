# EventType: 13874252
#
# Descriptor: {[[B_s0]nos -> (D_s- -> (phi(1020) -> K+ K-) mu- anti-nu_mu) (tau+ -> mu+ nu_mu anti-nu_tau) nu_tau]cc, [[B_s0]os ->(D_s+ -> (phi(1020) -> K+ K-) mu+ nu_mu) (tau- -> mu- anti-nu_mu nu_tau) anti-nu_tau]cc}
#
# NickName: Bs_Dstaunu,phimunu,mununu=TightCut
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Gauss.Configuration import *
# from Configurables import LoKi__GenCutTool
#
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = "([(Beauty) ==>  ^K+ ^K- ^mu+ ^mu- nu_mu~ nu_mu {X} {nu_tau~} {nu_tau}]CC) "
# tightCut.Preambulo += ["from LoKiCore.functions import in_range", "from GaudiKernel.SystemOfUnits import MeV"]
# tightCut.Cuts      =    {
#     '[mu-]cc'     : "(in_range(0.01, GTHETA, 0.4)) & (GP > 2900 * MeV)",
#     '[K-]cc'      : "(in_range(0.01, GTHETA, 0.4)) & (GP > 2900 * MeV)"
#   }
# EndInsertPythonCode
#
# Documentation: Bs -> Ds/Ds* tau nu and Bs -> Ds/Ds* mu nu, where Ds decays to phi(1020)/KK mu nu
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Hanae Tilquin
# Email: hanae.tilquin@cern.ch
# Date: 20210811
# CPUTime: < 1 min
#
Alias      MyPhi      phi
ChargeConj MyPhi      MyPhi
#
Alias      MyD_s+     D_s+
Alias      MyD_s-     D_s-
ChargeConj MyD_s+     MyD_s-
#
Alias      MyD_s*+    D_s*+
Alias      MyD_s*-    D_s*-
ChargeConj MyD_s*+    MyD_s*-
#
Alias      Mytau+     tau+
Alias      Mytau-     tau-
ChargeConj Mytau+     Mytau-
#
Decay B_s0sig
  0.0252   MyD_s-     mu+       nu_mu          ISGW2;
  0.0252   MyD_s-     Mytau+    nu_tau         ISGW2;
  0.0540   MyD_s*-    mu+       nu_mu          ISGW2;
  0.0540   MyD_s*-    Mytau+    nu_tau         ISGW2;
Enddecay
CDecay anti-B_s0sig
#
Decay Mytau+
  1.000    mu+        nu_mu     anti-nu_tau    TAULNUNU;
Enddecay
CDecay Mytau-
#
Decay MyD_s-
  0.500    MyPhi      mu-       anti-nu_mu     ISGW2;
  0.500    K+    K-   mu-       anti-nu_mu     PHSP;
Enddecay
CDecay MyD_s+
#
Decay MyD_s*-
  0.935    MyD_s-     gamma                    VSP_PWAVE;
  0.058    MyD_s-     pi0                      VSS;
Enddecay
CDecay MyD_s*+
#
Decay MyPhi
  1.000    K+         K-                       VSS;
Enddecay
#
End
