# This is the decay file for the decay B+ => (D*+ => (D0 => K- pi+) pi+) pi- mu+ nu_mu
#
# EventType: 12875003
#
# Descriptor: [B+ => (D*+ => (D0 => K- pi+) pi+) pi- anti-nu_mu mu+]cc
#
# NickName: Bu_Dstpimunu,Kpi=cocktail
#
# Cuts: ListOfDaughtersInLHCb
#
# ExtraOptions: D0muInAcc
#
# Documentation: Sum of D*- pi+ mu+ anti-nu_mu ; D*+ forced into D0 pi+; D0 forced into Kpi, 
# D0 and muon in LHCb acceptance.
# EndDocumentation 
#
# CPUTime: < 1 min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Mark Smith
# Email: mark.smith@{nospam}cern.ch
# Date: 20190927
#
##############
#
Alias      MyD0         D0
Alias      MyAntiD0     anti-D0
ChargeConj MyD0         MyAntiD0
#
Alias      MyD*-        D*-
Alias      MyD*+        D*+
ChargeConj MyD*-        MyD*+
#
Alias      MyD_10         D_10
Alias      MyAntiD_10     anti-D_10
ChargeConj MyD_10         MyAntiD_10
#
Alias      MyD'_10         D'_10
Alias      MyAntiD'_10     anti-D'_10
ChargeConj MyD'_10         MyAntiD'_10
#
Alias      MyD_2*0         D_2*0
Alias      MyAntiD_2*0     anti-D_2*0
ChargeConj MyD_2*0         MyAntiD_2*0
#
Decay B-sig
  0.003268   MyD_10      mu-  anti-nu_mu      PHOTOS  ISGW2;               
  0.0013     MyD'_10     mu-  anti-nu_mu      PHOTOS  ISGW2;               
  0.001065   MyD_2*0     mu-  anti-nu_mu      PHOTOS  ISGW2;                              
  0.000938   MyD*+  pi-  mu-  anti-nu_mu      PHOTOS  GOITY_ROBERTS;              
Enddecay
CDecay B+sig
#
SetLineshapePW MyD_10 MyD*+ pi- 2
SetLineshapePW MyAntiD_10 MyD*- pi+ 2
#
SetLineshapePW MyD_2*0 MyD*+ pi- 2
SetLineshapePW MyAntiD_2*0 MyD*- pi+ 2
#
Decay MyD0
  1.000   K-  pi+                             PHOTOS PHSP;
Enddecay
CDecay MyAntiD0
#
Decay MyD*+
  0.6770    MyD0  pi+                         PHOTOS VSS;
Enddecay
CDecay MyD*-
#
Decay MyD'_10
  0.500    MyD*+ pi-                          PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyAntiD'_10
#
Decay MyD_10
  0.400    MyD*+ pi-                          PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay
CDecay MyAntiD_10
#
Decay MyD_2*0
  0.173    MyD*+ pi-                          PHOTOS TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
Enddecay
CDecay MyAntiD_2*0
#
End
#
