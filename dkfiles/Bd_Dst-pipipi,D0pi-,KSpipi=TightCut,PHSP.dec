# EventType: 11268110
#
# Descriptor: {[[B0]nos -> (D*(2010)- -> (D~0 -> (KS0 -> pi+ pi-) pi+ pi-) pi-) pi+ pi- pi+]cc, [[B0]os -> ( D*(2010)+ -> (D0 => (KS0 -> pi- pi+) pi- pi+) pi+) pi- pi+ pi-]cc}
#
# NickName: Bd_Dst-pipipi,D0pi-,KSpipi=TightCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^[Beauty ==> ^(D*(2010)- ==> ^(D~0 => ^(KS0 ==> ^pi+ ^pi-) ^pi+ ^pi-) ^pi-) ^pi+ ^pi- ^pi+]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'inAcc        = (in_range (0.005, GTHETA, 0.400))',
#     'goodB        = (GP > 55000 * MeV) & (GPT > 5000 * MeV) & (GTIME > 0.135 * millimeter)',
#     'goodD        = (GP > 25000 * MeV) & (GPT > 2500 * MeV)',
#     'goodKS       = (GFAEVX(abs(GVZ), 0) < 2500.0 * millimeter)',
#     'goodDDaugPi  = (GNINTREE (("pi+" == GABSID) & (GP > 2000 * MeV) & inAcc, 4) > 3.5)',
#     'goodKsDaugPi = (GNINTREE (("pi+" == GABSID) & (GP > 2000 * MeV) & inAcc, 4) > 1.5)',
#     'goodBachPia  = (GNINTREE (("pi+" == GABSID) & (GP > 2000 * MeV) & (GPT > 100 * MeV) & inAcc, 4) > 4.5)',
#     'goodBachPic  = (GNINTREE (("pi+" == GABSID) & (GP > 2000 * MeV) & (GPT > 300 * MeV) & inAcc, 4) > 1.5)',
# ]
# tightCut.Cuts      =    {
#     '[B0]cc'         : 'goodB  & goodBachPia & goodBachPic',
#     '[D0]cc'         : 'goodD  & goodDDaugPi',
#     '[KS0]cc'        : 'goodKS & goodKsDaugPi',
#     '[pi+]cc'        : 'inAcc',
#     '[D*(2010)+]cc'  : 'inAcc'
#     }
# EndInsertPythonCode
#
#
# Documentation: B decay to D*+/- pi pi pi, D* -> D0 pi, D0 -> KS pi pi. Decay products in acceptance and tight cuts 
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: <1min
# Responsible: George Lovell
# Email: george.lovell@cern.ch
# Date: 20190405
#
Alias      MyD*-       D*-
Alias      MyD*+       D*+
ChargeConj MyD*+       MyD*-
#
Alias      MyD0        D0
Alias Myanti-D0   anti-D0
ChargeConj        MyD0       Myanti-D0
#
Alias      MyK_S0      K_S0
ChargeConj MyK_S0      MyK_S0
#
Alias      Mya_1-     a_1-
Alias      Mya_1+     a_1+
ChargeConj Mya_1+     Mya_1-
#
Alias Myf_2          f_2
ChargeConj Myf_2 Myf_2
#
#

Decay B0sig
0.70   MyD*- Mya_1+                      SVV_HELAMP 0.200 0.0 0.866 0.0 0.458 0.0; 
0.13   MyD*-  Myf_2  pi+                PHSP;
0.12   MyD*-  rho0  pi+                  PHSP;
0.05   MyD*-  pi+ pi- pi+                PHSP;
Enddecay
CDecay anti-B0sig
#
Decay Myanti-D0
  1.000    MyK_S0       pi+       pi-       PHSP;
Enddecay
CDecay MyD0
#
Decay Mya_1+
  1.000   rho0 pi+                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-
#
Decay MyD*-
  1.000        Myanti-D0 pi-                    VSS;
Enddecay
CDecay MyD*+
#
Decay Myf_2
  1.0000  pi+ pi-                               TSS ;
Enddecay
#
Decay MyK_S0
  1.000    pi+ pi-        PHSP;
Enddecay
#
End

