# EventType: 15166517
#
# Descriptor: [Lambda_b0 => (D*(2007)0 -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) (pi0 -> gamma gamma)) p+ pi-]cc
#
# NickName: Lb_Dst0ppi,D0pi0,KSpipi=LbsqDalitz,DDalitz,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut 
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalPlain.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalPlain.TightCut
#tightCut.Decay = '^[Beauty => (D*(2007)0 -> ^(D0 => ^(KS0 => ^pi+ ^pi-) ^pi+ ^pi-) pi0) ^p+ ^pi-]CC'
#tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = in_range(0.005, GTHETA, 0.400) & in_range(1.8, GETA, 5.2)',
#    'inY          = in_range(1.8, GY, 4.8)',
#    'goodH        = (GP > 1000 * MeV) & (GPT > 98 * MeV) & inAcc',
#    'goodB0       = (GP > 25000 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.05 * millimeter) & inY',
#    'goodD0       = (GP > 10000 * MeV) & (GPT > 500 * MeV) & inY',
#    'goodKS       = (GP > 4000 * MeV) & (GPT > 250 * MeV)',
#    'goodBDaugPi  = (GNINTREE( ("pi+" == GABSID) & (GP > 2000 * MeV), 1) > 0.5)',
#    'goodBDaugP   = (GNINTREE( ("p+" == GABSID) & (GP > 2000 * MeV), 1) > 0.5)',
#    'goodKsDaugPi = (GNINTREE( ("pi+" == GABSID) & (GP > 1750 * MeV), 1) > 1.5)'
#]
#tightCut.Cuts = {
#    '[p+]cc'          : 'goodH',
#    '[pi+]cc'         : 'goodH',
#    'Beauty'          : 'goodB0 & goodBDaugPi & goodBDaugP', 
#    '[D0]cc'          : 'goodD0',
#    '[KS0]cc'         : 'goodKS & goodKsDaugPi'
#    }
#EndInsertPythonCode
#
# Documentation: Lb decay with flat square Dalitz model, Dst0 forced to D0 pi0, D0 decay with KSpipi model, tight cuts 
# EndDocumentation
#
# CPUTime: <2min
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Yuya Shimizu 
# Email: yuya.shimizu@cern.ch 
# Date: 20201202
#

Alias MyD*0       D*0
Alias Myanti-D*0  anti-D*0
Alias MyD0        D0
Alias Myanti-D0   anti-D0
Alias MyKS        K_S0
ChargeConj MyD*0  Myanti-D*0
ChargeConj MyD0   Myanti-D0
ChargeConj MyKS   MyKS

Decay Lambda_b0sig
  1.0   MyD*0       p+         pi-  FLATSQDALITZ;
Enddecay
CDecay anti-Lambda_b0sig

Decay MyD*0
  1.0   MyD0        pi0             VSS;
Enddecay
CDecay Myanti-D*0

Decay MyD0
  1.0   MyKS        pi+        pi-  D_DALITZ;
Enddecay
CDecay Myanti-D0

Decay MyKS
  1.0   pi+         pi-             PHSP;
Enddecay

End
