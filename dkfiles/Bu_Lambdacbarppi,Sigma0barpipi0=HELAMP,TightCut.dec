# EventType: 12165723
# 
# Descriptor: [ B+ -> (anti-Lambda_c- -> (Sigma~0 -> (Lambda~0 -> p~- pi+) gamma) pi- pi0) p+ pi+]cc 
# 
# NickName: Bu_Lambdacbarppi,Sigma0barpipi0=HELAMP,TightCut
#
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[B+ ==> (Lambda_c~- => (Sigma~0 => (Lambda~0 => ^p~- ^pi+) gamma) ^pi- pi0) ^p+ ^pi+]CC"
# tightCut.Preambulo += [
# "from GaudiKernel.SystemOfUnits import MeV",
# "InAcc = in_range ( 0.005 , GTHETA , 0.400 )",
# "goodpi  = ( GP > 1000 * MeV ) & ( GPT > 100 * MeV ) & InAcc",
# "goodp   = ( GP > 5000 * MeV ) & ( GPT > 200 * MeV ) & InAcc"
# ]
# tightCut.Cuts = {
# '[pi-]cc' : "goodpi",
# '[p+]cc'  : "goodp"
# }
#
# EndInsertPythonCode
# 
# Documentation:
#                 Lambda0 forced into p pi.
#                 Lambda0 -> p pi helicity amplitude from 2019 combination in PRL 123, 182301.
#                 Lc -> Lambda pi helicity amplitude set to -0.86 (higher than current PDG to compensate for the fact that BESIII dominates and used larger alpha(L->ppi) than here) #Copied from Lc_Lambdapi=HELAMP,TightCut.dec
#                 All charged final state tracks are required to be within the LHCb acceptance.
#
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Liang Sun
# Email: lsun@cern.ch
# Date: 20230324
#
#

Alias      MySigma      Sigma0
Alias      Myanti-Sigma anti-Sigma0
ChargeConj Myanti-Sigma MySigma
Alias       MyLambda      Lambda0
Alias       MyantiLambda  anti-Lambda0
ChargeConj  MyLambda      MyantiLambda
Alias       MyLambda_c+   Lambda_c+
Alias       Myanti-Lambda_c- anti-Lambda_c-
ChargeConj  MyLambda_c+ Myanti-Lambda_c-
Alias      mypi0         pi0
ChargeConj mypi0         mypi0

#
Decay B+sig
  1.000     Myanti-Lambda_c-  p+  pi+         PHSP; 
Enddecay
CDecay B-sig
#
Decay MyLambda
  1.000     p+            pi-                HELAMP 0.9276 0.0 0.3735 0.0;
Enddecay
CDecay MyantiLambda
#
Decay MyLambda_c+
  1.000     MySigma            pi+   mypi0    PHSP;
Enddecay
CDecay Myanti-Lambda_c-

Decay MySigma
  1.000    MyLambda	    gamma                   PHSP;
Enddecay
CDecay Myanti-Sigma

Decay mypi0
  1.000   gamma          gamma        PHSP;
Enddecay
#
End
#

