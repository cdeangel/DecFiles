# EventType: 13106101
#
# Descriptor: [B_s0 -> (KS0 -> pi+ pi-) pi- pi+ pi- pi+]CC
#
# NickName: Bs_Kspipipipi=PHSP,PartRecCut
#
# Cuts: 'LoKi::GenCutTool/TightCut'
# Documentation:
# B_s0 decay K0s pi- pi+ pi+ pi- in flat PHSP, Tight cuts adapted for partially reconstructed decays in B2Kspipipi AmAn
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# Responsible: Pablo Baladron Rodriguez
# Email: pablo.baladron.rodriguez@cern.ch
# Date: 20211016
# CPUTime: < 1 min
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = '^[B_s0 => (KS0 => pi+ pi-) pi- pi+ pi- pi+]CC'
# tightCut.Preambulo += [ "from GaudiKernel.SystemOfUnits import GeV, mrad"
#                          , "inAcc = in_range(1.9, GETA, 5.0)" 
#                          , "isGoodPi      = ( ( GPT > 0.5*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodPiKs      = ( ( GP > 2.*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodKs      = ( ( 'KS0' == GABSID ) & (GNINTREE( isGoodPiKs, 1 ) > 1 ))"
#                          , "isGoodB        = ( ( 'B_s0' == GABSID ) & ( GNINTREE( isGoodPi, 1 ) > 2 ) & ( GNINTREE( isGoodKs, 1 ) > 0 ))" ]
# tightCut.Cuts	= {
#	'[B_s0]cc' : 'isGoodB'}
# EndInsertPythonCode
#
Alias myK_S0      K_S0
ChargeConj myK_S0 myK_S0
#
Decay B_s0sig
  1.000     myK_S0       pi-    pi+    pi-     pi+     PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay myK_S0
1.000     pi+  pi-               PHSP;
Enddecay
#
End

