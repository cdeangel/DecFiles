# EventType: 12197050
# NickName: Bu_D0D0pi,K3Pi,KPi=sqDalitz23,TightCut
# Descriptor: [B+ -> ( D0 => K- pi+ pi+ pi- ) ( D~0 => K+ pi- ) pi+]cc
#
#Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '[B+ => (D0 ==> ^K- ^pi+ ^pi+ ^pi-)   (D~0 ==> ^K+ ^pi-) ^pi+]CC'
#tightCut.Preambulo += [
#    'GVZ = LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = (in_range(0.005, GTHETA, 0.400) & in_range ( 1.8 , GETA , 5.2))',
#    'goodK        = (GP > 1.3 * GeV) & (GPT >  80 * MeV)',
#    'goodPi       = (GP > 1.3 * GeV) & (GPT >  80 * MeV)',
#]
#tightCut.Cuts = {
#    '[K+]cc'   : 'inAcc & goodK',
#    '[pi+]cc'  : 'inAcc & goodPi'
#    }
#EndInsertPythonCode
# 
#
# Documentation: Decay file for B+- -> D0 D0bar pi+- , D0 of 'same-sign' as B goes to K3pi. B decay forced flat in 2-3 sq Dalitz plot.
# EndDocumentation
#
# Date:   20230322
#
# Responsible: Yajing Wei
# Email: yajing.wei@cern.ch
# PhysicsWG: B2OC
# CPUTime: < 1 min
#
# Tested: Yes

Alias My_D0   			D0
Alias My_anti-D0 		anti-D0
Alias My_D0_Kpi   		D0
Alias My_anti-D0_Kpi 	anti-D0
Alias My_D0_K3pi   		D0
Alias My_anti-D0_K3pi 	anti-D0

ChargeConj My_anti-D0 		My_D0
ChargeConj My_anti-D0_Kpi 	My_D0_Kpi
ChargeConj My_anti-D0_K3pi 	My_D0_K3pi

Decay My_D0_Kpi
  1.0 K- pi+ 		 		PHSP;
Enddecay
CDecay My_anti-D0_Kpi

Decay My_D0_K3pi
  1.0 K- pi+ pi+ pi-		LbAmpGen DtoKpipipi_v2;
Enddecay
CDecay My_anti-D0_K3pi

Decay B+sig
  1.0 My_anti-D0_Kpi pi+ My_D0_K3pi  	FLATSQDALITZ;
Enddecay
CDecay B-sig

End
