# EventType: 26197078
#
# Descriptor: [Sigma_c*+ -> (Sigma_c++ -> (Lambda_c+ -> p+ K- pi+) pi+) (D*(2010)- -> (D~0 -> K+ pi-) pi-)]cc
#
# NickName: Pc5600,Sigma_c++Dst-,Lcpi,pkpi=TightCut,InAcc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Sample: SignalRepeatedHadronization
#
# ParticleValue: "Sigma_c*+ 486 4214 1.0 5.600 6.591074e-23 Sigma_c*+ 4214 0.00" , "Sigma_c*~- 487 -4214 -1.0 5.600 6.591074e-23 anti-Sigma_c*- -4214 0.00"
#
# Documentation: Pc decay to Sigma_c++ D*- in PHSP model with daughters in LHCb Acceptance
# Sigma_c*+ used for the generation.
#
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal     = generation.SignalRepeatedHadronization
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay  = '[Sigma_c*+ => (Sigma_c++ => ^(Lambda_c+ ==> ^p+ ^K- ^pi+) pi+) (D*(2010)- => ^(D~0 => ^K+ ^pi-) pi-)]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV',
#     'GY           =  LoKi.GenParticles.Rapidity () ## to be sure ' ,
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         ' ,
#     'inEta        =  in_range ( 1.95  , GETA   , 5.050 )         ' ,
#     'fastTrack    =  ( GPT > 220 * MeV ) & ( GP  > 3.0 * GeV )   ' ,
#     'goodTrack    =  inAcc & inEta                               ' ,
#     'goodLc       =  ( GPT > 0.9 * GeV )   ' ,
#     'goodD0bar    =  ( GPT > 0.9 * GeV )   ' ,
# ]
# tightCut.Cuts     =    {
#     '[Lambda_c+]cc'  : 'goodLc   ' ,
#     '[K-]cc'         : 'goodTrack & fastTrack' ,
#     '[pi+]cc'        : 'goodTrack & fastTrack' ,
#     '[p+]cc'         : 'goodTrack & fastTrack & ( GP > 9 * GeV ) ',
#     '[D~0]cc'        : 'goodD0bar' ,
#     }
# EndInsertPythonCode
#
# PhysicsWG:   Onia
# Tested:      Yes
# Responsible: Gary Robertson
# Email:       gary.robertson@ed.ac.uk
# Date:        20200313
# CPUTime:     <1min
#
#
Alias      MySigma_c++           Sigma_c++
Alias      Myanti-Sigma_c-- anti-Sigma_c--
ChargeConj MySigma_c++    Myanti-Sigma_c--
#
Alias      MyDst-        D*-
Alias      MyDst+        D*+
ChargeConj MyDst-     MyDst+
#
Alias      MyD0bar       anti-D0
Alias      MyD0          D0
ChargeConj MyD0bar     MyD0
#
Alias            MyLambda_c+        Lambda_c+
Alias       anti-MyLambda_c-   anti-Lambda_c-
ChargeConj       MyLambda_c+ anti-MyLambda_c-
#
Decay Sigma_c*+sig
  1.000          MySigma_c++     MyDst-  PHSP;
Enddecay
CDecay anti-Sigma_c*-sig
#
Decay MySigma_c++
  1.000          MyLambda_c+     pi+     PHSP;
Enddecay
CDecay Myanti-Sigma_c--
#
Decay MyLambda_c+
  1.000          p+      K-      pi+     PHSP;
Enddecay
CDecay anti-MyLambda_c-
#
Decay MyDst-
  1.000        MyD0bar   pi-             PHSP;
Enddecay
CDecay MyDst+
#
Decay MyD0bar
  1.000          K+      pi-     PHSP;
Enddecay
CDecay MyD0
#
End
#
