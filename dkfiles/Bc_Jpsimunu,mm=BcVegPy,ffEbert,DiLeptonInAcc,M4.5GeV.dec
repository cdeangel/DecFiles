# EventType: 14543043
#
# Descriptor: [B_c+ => (J/psi(1S) => mu+ mu-) mu+ nu_mu]cc
#
# NickName: Bc_Jpsimunu,mm=BcVegPy,ffEbert,DiLeptonInAcc,M4.5GeV
#
# Production: BcVegPy
#
# Cuts: LoKi::GenCutToolWithDecay/TightCut
#
# Documentation: Bc decay to Jpsi(to mu+ mu-), mu+ and nu_mu with form factor model by Ebert et al [PhysRevD.68.094020]. Radiative mode included. The algorithm BcVegPy is weighted. The oppositely charged non-Jpsi muon combinations in the decay are required to be in acceptance and have a minimal mass.
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutToolWithDecay
# from Gauss.Configuration import *
# gen = Generation()
# gen.Special.addTool ( LoKi__GenCutToolWithDecay , 'TightCut' )
# gen.Special.CutTool = 'LoKi::GenCutToolWithDecay/TightCut'
# #
# tightCut = gen.Special.TightCut
# tightCut.SignalPID = 'B_c+'
# tightCut.Decay = '^[B_c+ => (J/psi(1S) => mu+ ^mu-) ^mu+ nu_mu]CC'
# tightCut.Preambulo += [
#	  "CS = LoKi.GenChild.Selector",
#     "inAcc   = in_range ( 0.010 , GTHETA , 0.400 ) ",
#     "Dileptonthreshold = GMASS(CS('[B_c+ => (J/psi(1S) => mu+ ^mu-) mu+ nu_mu]CC'), CS('[B_c+ => (J/psi(1S) => mu+ mu-) ^mu+ nu_mu]CC')) > 4.5 * GeV "
#     ]
# tightCut.Cuts = {
#     '[mu+]cc' : ' inAcc ',
#	  '[B_c+]cc': ' Dileptonthreshold'
#     }
# 
# EndInsertPythonCode
#
#
# PhysicsWG: RD 
# Tested: Yes
# Responsible: Titus Mombächer 
# Email: titus.mombacher@cern.ch
# Date: 20191105
# CPUTime: <1min
#
Alias      MyJ/psi  J/psi
ChargeConj MyJ/psi  MyJ/psi
#
Decay B_c+sig
  1.000         MyJ/psi   mu+   nu_mu   PHOTOS BC_VMN 2;
Enddecay
CDecay B_c-sig
#
Decay MyJ/psi
  1.000         mu+       mu-           PHOTOS VLL;
Enddecay
#
End


