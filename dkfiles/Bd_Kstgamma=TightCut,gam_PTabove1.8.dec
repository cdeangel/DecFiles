# EventType: 11102204
#
# Descriptor: [Beauty => (K*(892)0 => K+ pi-) gamma]cc 
#
# NickName: Bd_Kstgamma=TightCut,gam_PTabove1.8
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation()
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# gen.SignalRepeatedHadronization.setProp('MaxNumberOfRepetitions', 5000)
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[Beauty => (K*(892)0 => ^K+ ^pi-) ^gamma]CC'
# tightCut.Cuts      =    {
#     '[K+]cc'         : ' inAcc' , 
#     '[pi+]cc'        : ' inAcc' , 
#     'gamma'          : ' goodPhoton'
#     }
#
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inAcc          = in_range ( 0.005 , GTHETA , 0.400 ) " ,
#     "inEcalX       =   abs ( GPX / GPZ ) < 4.5  / 12.5",
#     "inEcalY       =   abs ( GPY / GPZ ) < 3.5  / 12.5",
#     "inEcalHole    = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 )",
#     "InEcal        = inEcalX & inEcalY & ~inEcalHole ",
#     "goodPhoton     = ( GPT > 1.8 * GeV ) & InEcal"
#     ]
#
# EndInsertPythonCode
#
# Documentation: for KstG, gamma PT > 1.8 GeV, inAcceptance
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20201119
#
Alias      MyK*0   K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0 Myanti-K*0
#
Decay B0sig
  1.000        MyK*0    gamma     SVP_HELAMP 1.0 0.0 1.0 0.0;
Enddecay
CDecay anti-B0sig
#
Decay MyK*0
  1.000        K+       pi-      VSS;
Enddecay
CDecay Myanti-K*0
End
