# EventType: 12997624
#
# Descriptor: {[B+ -> (D*- -> pi- (anti-D0 -> K+ pi-)) D_s+ ... ]cc}
# NickName: B+_DstXc,Xc2hhhNneutrals_cocktail_Kpih,upto5prongs=DecProdCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen = Generation().SignalRepeatedHadronization
# gen.addTool( LoKi__GenCutTool, "TightCut" )
# tightCut = gen.TightCut
# tightCut.Decay = "[(Beauty & LongLived) --> ^(D*(2010)+ => ^(D0 => K- pi+) pi+) ...]CC"
# tightCut.Cuts = {
#     "(Beauty & LongLived)" : "good_B",
#     "[D*(2010)+]cc" : "good_Dstar",
#     "[D0]cc" : "good_D0",
# }
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import MeV, GeV",
#     "inAcc = ( GPZ > 0 ) & ( GPT > 40 * MeV ) & ( GP > 1.6 * GeV ) & in_range ( 1.8, GETA, 5.0 ) & in_range ( 0.005, GTHETA, 0.400 )",
#     "nPi = GCOUNT(('pi+' == GABSID) & inAcc, HepMC.descendants)",
#     "nK  = GCOUNT(('K-'  == GABSID) & inAcc, HepMC.descendants)",
#     "good_slow_pion = ('pi+' == GABSID) & (GPT >  40 * MeV) & inAcc",
#     "good_D0_pion   = ('pi+' == GABSID) & (GPT > 140 * MeV) & inAcc",
#     "good_D0_kaon   = ('K+'  == GABSID) & (GPT > 140 * MeV) & inAcc",
#     "good_D0        = ('D0'  == GABSID) & (GPT > 1.5 * GeV) & ( GP > 15. * GeV ) & GCHILDCUT(good_D0_kaon, 'Charm => ^(K+|K-) (pi+|pi-)') & GCHILDCUT(good_D0_pion, 'Charm => (K+|K-) ^(pi+|pi-)')",
#     "good_Dstar     = ('D*(2010)+' == GABSID) & GCHILDCUT(good_slow_pion, 'Charm => Charm ^(pi+|pi-)')",
#     "good_B         = (nK >= 1) & (nPi >= 3)",
# ]
# EndInsertPythonCode
#
# Documentation: B+ -> D**(-> D* pi) Ds X cocktail, where Ds decays to final states with at least 1 charged pion and 1 charged Kaon, and may come from D_s*, D_s*(2317), D_s(2457) or D_s(2536). The D* must come from D_1, D_1' or D_2*.
# Background for B2XTauNu analyses.
# Myphi = 0.1524*phi
# Myomega = 0.9073*omega
# Myeta = 0.2714*eta
# Myetap = 0.8037*eta'
# Mya1+ = 0.5*a_1+
# EndDocumentation
#
# CPUTime: <1 min 
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Bo Fang
# Email: bo.fang@cern.ch
# Date: 20220414
#
Alias           Myphi           phi
ChargeConj      Myphi           Myphi
#
Alias           Myomega         omega
ChargeConj      Myomega         Myomega
#
Alias           Myeta           eta
ChargeConj      Myeta           Myeta
#
Alias           Myetap          eta'
ChargeConj      Myetap          Myetap
#
Alias           Myf0KK         f_0
ChargeConj      Myf0KK         Myf0KK
#
Alias           MyphiKK        phi
ChargeConj      MyphiKK        MyphiKK
#
Alias           MyD_s+          D_s+
Alias           MyD_s-          D_s-
ChargeConj      MyD_s+          MyD_s-
#
Alias           MyD_s*+         D_s*+
Alias           MyD_s*-         D_s*-
ChargeConj      MyD_s*+         MyD_s*-
#
Alias           MyD_s*(2317)+   D_s0*+
Alias           MyD_s*(2317)-   D_s0*-
ChargeConj      MyD_s*(2317)+   MyD_s*(2317)-
#
#
Alias           MyD_s*(2457)+   D_s1+
Alias           MyD_s*(2457)-   D_s1-
ChargeConj      MyD_s*(2457)+   MyD_s*(2457)-
#
Alias           MyD_s*(2536)+   D'_s1+
Alias           MyD_s*(2536)-   D'_s1-
ChargeConj      MyD_s*(2536)+   MyD_s*(2536)-
#
Alias           MyMainD*+       D*+
Alias           MyMainD*-       D*-
ChargeConj      MyMainD*+       MyMainD*-
#
Alias           MyD0            D0
Alias           anti-MyD0       anti-D0
ChargeConj      MyD0            anti-MyD0
#
Alias           MyK*0           K*0
Alias           Myanti-K*0      anti-K*0
ChargeConj      MyK*0           Myanti-K*0
#
#
Alias           MyD'_10         D'_10
Alias           Myanti-D'_10    anti-D'_10
ChargeConj      MyD'_10         Myanti-D'_10
#
Alias           MyD_10          D_10
Alias           Myanti-D_10     anti-D_10
ChargeConj      MyD_10          Myanti-D_10
#
Alias           MyD_2*0         D_2*0
Alias           Myanti-D_2*0    anti-D_2*0
ChargeConj      MyD_2*0         Myanti-D_2*0
#
#

Decay B+sig
####    B+ contributions
 0.000316          Myanti-D'_10         MyD_s+          SVS;
 0.000484          Myanti-D'_10         MyD_s*+         SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
 0.00006           Myanti-D'_10         MyD_s*(2317)+   SVS;
 0.00028           Myanti-D'_10         MyD_s*(2457)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.00002           Myanti-D'_10         MyD_s*(2536)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
#
 0.001            Myanti-D_10         MyD_s+          SVS;
 0.000847         Myanti-D_10         MyD_s*+         SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
 0.00018          Myanti-D_10         MyD_s*(2317)+   SVS;
 0.00088          Myanti-D_10         MyD_s*(2457)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.00006          Myanti-D_10         MyD_s*(2536)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
# below 3 times higher compared to B0 -> D2*- Ds X to account for ratio to D* pi
 0.000152         Myanti-D_2*0         MyD_s+          STS;
 0.000248         Myanti-D_2*0         MyD_s*+         PHSP;
 0.000028         Myanti-D_2*0         MyD_s*(2317)+   STS;
 0.000135         Myanti-D_2*0         MyD_s*(2457)+   PHSP;
 0.000009         Myanti-D_2*0         MyD_s*(2536)+   PHSP;
#### Add some NR D*pi contribution accounting for higher mass D** states and NR
 0.000126         MyMainD*-       pi+     MyD_s+       PHSP;
 0.000094         MyMainD*-       pi+     MyD_s*+      PHSP;
Enddecay
CDecay B-sig

Decay MyD_s+

 0.0065         K+              pi-     pi+     D_DALITZ;
#0.0564         0.6650*anti-K*0      K*+        SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0(#taken from DECAY.DEC);
 0.0375         Myanti-K*0      K*+             SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.054          K*+             anti-K0         SVS;  
 0.0258         Myanti-K*0      K+              SVS;  
 0.0114         Myf0KK          pi+             PHSP;  
#0.045          0.492*phi       pi+             SVS;
#0.0559         0.492*phi       rho+            SVV;
 0.02214        MyphiKK         pi+             SVS;  
 0.0275         MyphiKK         rho+            SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;  
#0.0016         K+          0.2714*eta          PHSP;
#0.00087        0.9073*omega       K+           SVS;
#0.00265        K+          0.8037*etap         PHSP;
 0.000434       K+              Myeta           PHSP;
 0.000786       Myomega         K+              SVS;
 0.002130       K+              Myetap          PHSP;
 0.00045        K*+             rho0            SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.0003         MyK*0           rho+            SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;

Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
 0.942          MyD_s+  gamma                   VSP_PWAVE;
 0.058          MyD_s+  pi0                     PHSP;
Enddecay
CDecay MyD_s*-
#
Decay MyD_s*(2317)+
 1.000          MyD_s+  pi0                     PHSP;
Enddecay
CDecay MyD_s*(2317)-
#
Decay MyD_s*(2457)+
 0.18           MyD_s+          gamma           VSP_PWAVE;
 0.48           MyD_s*+         pi0             PHSP;
 0.043          MyD_s+          pi+     pi-     PHSP;
 0.022          MyD_s+          pi0     pi0     PHSP;
 0.04           MyD_s*(2317)+   gamma           VSP_PWAVE;
Enddecay
CDecay MyD_s*(2457)-
#
Decay MyD_s*(2536)+
 0.25           MyD_s+          pi+     pi-     PHSP;
 0.125          MyD_s+          pi0     pi0     PHSP;
 0.1            MyD_s*+         gamma           PHSP;
Enddecay
CDecay MyD_s*(2536)-
#
Decay MyMainD*+
 1.000          MyD0    pi+                     VSS;
Enddecay
CDecay MyMainD*-
#
SetLineshapePW D_10 D*0 pi0 2
SetLineshapePW D_10 D*+ pi- 2
SetLineshapePW anti-D_10 anti-D*0 pi0 2
SetLineshapePW anti-D_10 D*- pi+ 2
#
SetLineshapePW D_2*0 D*0 pi0 2
SetLineshapePW D_2*0 D*+ pi- 2
SetLineshapePW anti-D_2*0 anti-D*0 pi0 2
SetLineshapePW anti-D_2*0 D*- pi+ 2
#
Decay MyD_10
 1.000          MyMainD*+       pi-         VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay
CDecay Myanti-D_10
#
Decay MyD'_10
 1.000          MyMainD*+       pi-         VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Myanti-D'_10
#
Decay MyD_2*0
 1.000          MyMainD*+       pi-         TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
Enddecay
CDecay Myanti-D_2*0
#
Decay MyD0
 1.000          K-      pi+                     PHSP;
Enddecay
CDecay anti-MyD0
#
Decay Myeta
 0.2292         pi-     pi+     pi0             ETA_DALITZ;
 0.0422         pi-     pi+     gamma           PHSP;
Enddecay
#
Decay Myomega
 0.892          pi-     pi+     pi0             OMEGA_DALITZ;
 0.0153         pi-     pi+                     PHSP;
Enddecay
#
Decay Myetap
 0.295          rho0            gamma           SVP_HELAMP 1.0 0.0 1.0 0.0;
 0.425          eta             pi+     pi-     PHSP;
#0.0252         0.9073*omega    gamma           PHSP;
#0.224          0.2714*eta      pi0     pi0     PHSP;
 0.0229         Myomega         gamma           PHSP;
 0.0608         Myeta           pi0     pi0     PHSP;
Enddecay
#
Decay Myphi
 0.1524         pi+     pi-     pi0             PHI_DALITZ;
Enddecay
#
Decay MyK*0
 0.6650         K+      pi-                     VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Myf0KK
 1.000          K+      K-                      PHSP;
Enddecay
#
Decay MyphiKK
 1.000          K+      K-                      PHSP;
Enddecay
#
End
