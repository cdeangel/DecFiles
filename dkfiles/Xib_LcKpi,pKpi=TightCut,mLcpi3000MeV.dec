# EventType: 16165932
#
# Descriptor: [Xi_b- -> (Lambda_c+ -> p+ K- pi+) K- pi-]cc
#
# NickName: Xib_LcKpi,pKpi=TightCut,mLcpi3000MeV
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation() 
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/mLcpiCut"
# evtgendecay.addTool( LoKi__GenCutTool ,'mLcpiCut')
# evtgendecay.mLcpiCut.Decay = '[^(Xi_b- => (Lambda_c+ => p+ K- pi+) K- pi-)]CC'
# evtgendecay.mLcpiCut.Cuts  = {'[Xi_b-]cc' : ' mLcpiCut '}
# evtgendecay.mLcpiCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "mLcpiCut   = ( GMASS(CS('[(Xi_b- => ^(Lambda_c+ => p+ K- pi+) K- pi-)]CC'), CS('[(Xi_b- => (Lambda_c+ => p+ K- pi+) K- ^pi-)]CC'))  < 3000 * MeV ) " ]  
#
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay   = '[Xi_b- => (Lambda_c+ => ^p+ ^K- ^pi+) ^K- ^pi-]CC'
# tightCut.Cuts    =    {
#     '[K+]cc'     : "inAcc",
#     '[pi-]cc'    : "inAcc",
#     '[p+]cc'     : "inAcc"}
# tightCut.Preambulo += [
#     "inAcc   = in_range ( 0.005 , GTHETA , 0.400 ) " ]
#
# EndInsertPythonCode
#
# Documentation: Search for new decay mode. m(Lcpi) < 3 GeV.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: <1min
# Responsible: Marco Pappagallo
# Email:  marco.pappagallo@cern.ch
# Date: 20200221
#
Alias MyLambda_c+     Lambda_c+
Alias Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+    Myanti-Lambda_c-
#
Decay Xi_b-sig
  1.0    MyLambda_c+   K-  pi-                  PHSP;
Enddecay
CDecay anti-Xi_b+sig
#
Decay MyLambda_c+
  1.000         p+      K-      pi+     PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
End

