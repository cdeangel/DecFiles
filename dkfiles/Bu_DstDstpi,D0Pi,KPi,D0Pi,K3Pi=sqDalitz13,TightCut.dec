# EventType: 12199037
# NickName: Bu_DstDstpi,D0Pi,KPi,D0Pi,K3Pi=sqDalitz13,TightCut
# Descriptor: [B+ -> (D*(2010)+ => (D0 => K- pi+) pi+) (D*(2010)- => (D~0 => K+ pi- pi- pi+) pi-) pi+]cc
#
# Documentation: Decay file for B+- -> D*+- D*-+ pi+-, D0 of 'opposite-sign' as B goes to K3pi. B decay forced flat in 1-3 sq Dalitz plot.
# EndDocumentation
#
#Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '[B+ => (D*(2010)+ ==> (D0 ==> ^K- ^pi+) ^pi+) (D*(2010)- ==> (D~0 ==> ^K+ ^pi- ^pi- ^pi+) ^pi-) ^pi+]CC'
#tightCut.Preambulo += [
#    'GVZ = LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = (in_range(0.005, GTHETA, 0.400) & in_range ( 1.8 , GETA , 5.2))',
#    'goodK        = (GP > 1.3 * GeV) & (GPT >  80 * MeV)',
#    'goodPi       = (GP > 1.3 * GeV) & (GPT >  80 * MeV)',
#]
#tightCut.Cuts = {
#    '[K+]cc'   : 'inAcc & goodK',
#    '[pi+]cc'  : 'inAcc & goodPi'
#    }
#EndInsertPythonCode
# 
# Date:   20230322
#
# Responsible: Yajing Wei
# Email: yajing.wei@cern.ch
# PhysicsWG: B2OC
# CPUTime: < 1 min
#
# Tested: Yes

Alias My_D0             D0
Alias My_anti-D0        anti-D0
Alias My_D0_K3Pi        D0
Alias My_anti-D0_K3Pi   anti-D0
Alias My_D*+            D*+
Alias My_D*-            D*-
Alias My_D*+_K3Pi       D*+
Alias My_D*-_K3Pi       D*-

ChargeConj My_anti-D0        My_D0
ChargeConj My_anti-D0_K3Pi   My_D0_K3Pi 
ChargeConj My_D*-            My_D*+
ChargeConj My_D*-_K3Pi       My_D*+_K3Pi

Decay My_D0
  1.0 K- pi+  PHSP;
Enddecay
CDecay My_anti-D0

Decay My_D0_K3Pi
  1.0  K-  pi+   pi+   pi-  LbAmpGen DtoKpipipi_v2;
Enddecay
CDecay My_anti-D0_K3Pi

Decay My_D*+
  1.0 My_D0 pi+  PHSP;
Enddecay
CDecay My_D*-

Decay My_D*+_K3Pi
  1.0 My_D0_K3Pi pi+  PHSP;
Enddecay
CDecay My_D*-_K3Pi

Decay B+sig
  1.0 My_D*+ pi+ My_D*-_K3Pi FLATSQDALITZ;
Enddecay
CDecay B-sig

End
