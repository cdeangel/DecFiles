# EventType: 15502010
#
# Descriptor: [Lambda_b0 -> pi+ pi- H_30 ]cc
#
# NickName: Lambda0_PsiDMpipi=FullGenEvtCut,mPsiDM=940MeV
#
# Cuts: None
# FullEventCuts: LoKi::FullGenEventCut/LbtopipiDM
# 
# Documentation:
#    Decay a L0 to K pi and a redefined H_30 for our need, acting the latter as stable Dark Matter candidate with a mass of 0.94 GeV.
# EndDocumentation
#
# PhysicsWG: Exotica
# Tested: Yes
# CPUTime: 1 min
# Responsible: Saul Lopez 
# Email: saul.lopez.solino@cern.ch
# Date: 20210927
#
#
# InsertPythonCode:
# from Configurables import LHCb__ParticlePropertySvc, LoKi__GenCutTool
# LHCb__ParticlePropertySvc().Particles = [
# "H_30     89       36      0.0     0.940000        1.000000e+16    A0      36      0.00"
# ]
# ## Generator level cuts:
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool( LoKi__FullGenEventCut, "LbtopipiDM" )
# tracksInAcc = Generation().LbtopipiDM
# tracksInAcc.Code = " count ( isGoodLb ) > 0 "
# ### - HepMC::IteratorRange::descendants   4
# tracksInAcc.Preambulo += [ "from GaudiKernel.SystemOfUnits import GeV, mrad" 
#                          , "inAcc = in_range(1.9, GETA, 5.0)"
#                          , "isGoodPi   = ( ( GPT > 0.4*GeV ) & inAcc & ( 'pi+' == GABSID) )"
#                          , "isGoodLb   = ( ( 'Lambda_b0' == GABSID ) & ( GNINTREE( isGoodPi, 1 ) > 1 ) )" ]
# EndInsertPythonCode
#
Alias  MyH_30     A0
Alias  Myanti-H_30    A0
ChargeConj MyH_30   Myanti-H_30
#
Decay Lambda_b0sig
    1.000   MyH_30    pi+    pi-    PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
End
