# EventType: 15574033
# 
# Descriptor: [Lambda_b0 -> (D0 -> K- mu+ nu_mu) p+ e- anti-nu_e]cc
# 
# NickName: Lb_D0penu,Kmunu=DecProdCut,HighVisMass,EvtGenCut
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalPlain.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(Lambda_b0 => (D0 => K- mu+ nu_mu) e- nu_e~ p+)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[Lambda_b0]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += ["visMass  = ( ( GMASS ( 'mu+' == GABSID , 'e-' == GABSID, 'p+' == GABSID, 'K-' == GABSID ) ) > 4500 * MeV ) " ]
#
# EndInsertPythonCode
#
# Documentation: Decfile created as bg for Lb2Lambda1520mue analysis.
# Adapted from "Lb_D0penu,D0=Kenu,TightCut.dec" with cuts similar to "Lb_Lcenu,L0munu=DecProdCut,HighVisMass,EvtGenCut.dec".
# Semileptonic Lambda_b decay into D0 p e nu. D0 decays to K- mu nu.
# Generator level cut applied to have a visible mass larger than 4.5 GeV.
# Double semileptonic background for Lb->Lambda(1520)emu.
# EndDocumentation
#
# CPUTime: 1 min
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Dan Thompson   
# Email: dan.thompson@cern.ch
# Date: 20211125
#
#
#
Alias MyD0       D0
Alias Myanti-D0  anti-D0
ChargeConj MyD0  Myanti-D0
#
###
Decay Lambda_b0sig
  1.000    MyD0   p+   e-  anti-nu_e    PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyD0
  1.000    K-  mu+  nu_mu               ISGW2;
Enddecay
CDecay Myanti-D0
#
#
End
#
