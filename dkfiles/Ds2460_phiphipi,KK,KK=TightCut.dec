# EventType: 27105080
#
# Descriptor: [D_s1(2460)+ -> (phi(1020) -> K+ K- ) (phi(1020) -> K+ K- ) pi+]cc
#
# NickName:  Ds2460_phiphipi,KK,KK=TightCut
#
# ParticleValue: "D_s1(2460)+           172       20433   1.0      2.45950000      6.582100e-22                     D_s1+       20433      0.005", " D_s1(2460)-           176      -20433  -1.0      2.45950000      6.582100e-22                     D_s1-      -20433      0.005"
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#    Weak decays of Ds2460 to phiphipi final state.
#    Daughters in LHCb.
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalPlain 
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '^[D_s1(2460)+ => ^(phi(1020) -> ^K+ ^K- ) ^(phi(1020) -> ^K+ ^K- ) ^pi+]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV',
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )               ' ,
#     'fastTrack    =  ( GPT > 200 * MeV ) & ( GP  > 1.9 * GeV )         ' , 
#     'goodTrack    =  inAcc &  fastTrack                                ' ,     
#     'goodDs       =  ( GPT > 0.9 * GeV )         ' ,
#     'Bancestors   =  GNINTREE ( GBEAUTY , HepMC.ancestors )            ' , 
#     'notFromB     =  0 == Bancestors                                   ' 
# ]
#
# tightCut.Cuts     =    {
#     '[D_s1(2460)+]cc': 'goodDs & notFromB ' ,
#     '[K+]cc'  : 'goodTrack ' , 
#     '[pi+]cc' : 'goodTrack '
#     }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min 
# Responsible: Marco Pappagallo
# Email: marco.pappagallo@cern.ch
# Date: 20170205
#
Alias       Myphi   phi
ChargeConj  Myphi   Myphi
Decay D_s1+sig
  1.000        Myphi   Myphi       pi+             PHSP;
Enddecay
CDecay D_s1-sig
#
Decay Myphi
  1.000         K+         K-          VSS;
Enddecay
#
#
End
