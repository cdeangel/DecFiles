# EventType: 13574612
# Descriptor: [B_s0 -> (D*_s+ -> gamma (D_s+ -> K+ K- pi+)) (D*_s- -> gamma (D_s- -> eta mu- anti-nu_mu))]cc
# NickName: Bs_DsstDsst,DsgammaDsgamma,KKpimunuX=cocktail,mu3hInAcc
# Cuts: BeautyTo2CharmTomu3h
# CPUTime: 2 min
# Documentation: Force the Bs to decay to a combination of DsDs, Ds*Ds and Ds*Ds*.
#                In each case, force one Ds to decay to KKpi, then force the other Ds to 
#                decay semileptonically. 
#                For background study of semileptonic Bs->(Ds->KKpi)MuNu decays.
#                Requires that the mu from charm and the 3h from other charm are in acc.
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Stephen Ogilvy
# Email: stephen.ogilvy@cern.ch
# Date: 20170920
#
Alias      MyD_s+_1 D_s+
Alias      MyD_s-_1 D_s-
ChargeConj MyD_s+_1 MyD_s-_1

Alias      MyD_s+_2 D_s+
Alias      MyD_s-_2 D_s-
ChargeConj MyD_s+_2 MyD_s-_2

Alias      MyD_s*+_1 D_s*+
Alias      MyD_s*-_1 D_s*-
ChargeConj MyD_s*+_1 MyD_s*-_1

Alias      MyD_s*+_2 D_s*+
Alias      MyD_s*-_2 D_s*-
ChargeConj MyD_s*+_2 MyD_s*-_2

# ---------------
Decay B_s0sig
  0.01350     MyD_s+_1     MyD_s-_2        PHSP;
  0.01860     MyD_s*+_1    MyD_s*-_2       SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.00645     MyD_s*+_1    MyD_s-_2        SVS;
  0.00645     MyD_s*+_2    MyD_s-_1        SVS;
Enddecay
CDecay anti-B_s0sig
# -----------------
Decay MyD_s*+_1
  0.935     MyD_s+_1  gamma      VSP_PWAVE;
  0.058     MyD_s+_1  pi0        VSS;
Enddecay
CDecay MyD_s*-_1
# ---------------
Decay MyD_s*+_2
  0.942     MyD_s+_2  gamma      VSP_PWAVE;
  0.058     MyD_s+_2  pi0        VSS;
Enddecay
CDecay MyD_s*-_2
# ---------------
Decay MyD_s+_1
  0.055     K+    K-     pi+          PHOTOS D_DALITZ;
Enddecay
CDecay MyD_s-_1
#
Decay MyD_s+_2
  0.018309605 phi     mu+     nu_mu                           PHOTOS  ISGW2; 
  0.022845082 eta     mu+     nu_mu                           PHOTOS  ISGW2; 
  0.008186726 eta'    mu+     nu_mu                           PHOTOS  ISGW2; 
  0.002058115 anti-K0 mu+     nu_mu                           PHOTOS  ISGW2; 
  0.000762265 anti-K*0 mu+     nu_mu                          PHOTOS  ISGW2; 
  0.005560000 mu+     nu_mu                                   PHOTOS  SLN; 
Enddecay
CDecay MyD_s-_2
#
End

