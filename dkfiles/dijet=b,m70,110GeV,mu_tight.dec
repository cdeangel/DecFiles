# EventType: 49000091
# 
# Descriptor: pp -> bbbar (->muX)
# NickName: dijet=b,m70,110GeV,mu_tight
# Cuts: None
# FullEventCuts: LoKi::FullGenEventCut/twoToTwoWithMuonInAcc
# Production: Pythia8
# 
# InsertPythonCode:
# # Switch off all Pythia 8 options.
# from Gaudi.Configuration import importOptions
# importOptions( "$DECFILESROOT/options/SwitchOffAllPythiaProcesses.py" )
#
# # Pythia 8 options.
# from Configurables import Pythia8Production
# Generation().Special.addTool( Pythia8Production )
# Generation().Special.Pythia8Production.Commands += [
#     "HardQCD::hardbbbar = on",     # Hard process.
#     "PhaseSpace:mHatMin = 70.0",   # Minimum invariant mass.
#     "PhaseSpace:mHatMax = 110.0"]  # Maximum invariant mass.
# Generation().PileUpTool = "FixedLuminosityForRareProcess"
# 
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool(LoKi__FullGenEventCut, 'twoToTwoWithMuonInAcc')
# cutTool = Generation().twoToTwoWithMuonInAcc
# cutTool.Code = '(count(MuonInAcc)>0) & (count(out1)==1) & (count(out2)==1)'
# cutTool.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns, GeV, mrad',
#     'GPT = LoKi.GenParticles.TransverseMomentum()',
#     'isMuon = (GABSID == 13)',
#     'minPT = ((GTHETA < 350.0*mrad) & (GPT > 5*GeV))',
#     'MuonInAcc = ((isMuon) & (minPT))',
#     'out1 = ((GBARCODE == 5) & (GTHETA<270.0*mrad) & (GTHETA>24.0*mrad))',
#     'out2 = ((GBARCODE == 6) & (GTHETA<270.0*mrad) & (GTHETA>24.0*mrad))'
#     ]
# 
# # Keep 2 -> 2 hard process in MCParticles.
# from Configurables import GenerationToSimulation
# GenerationToSimulation("GenToSim").KeepCode = (
#     "( GBARCODE >= 1 ) & ( GBARCODE <= 6 )")
# EndInsertPythonCode
# 
# 
# Documentation:
# b-dijet production, hard process in acceptance,
# hard process mass between 70 and 110 GeV,
# muon with pt > 5GeV required in acceptance.
# EndDocumentation
# 
# PhysicsWG: Exotica
# Tested: Yes
# CPUTime: 3 min
# Responsible: William Barter
# Email: william.barter@cern.ch
# Date: 20210721
#
End
