# EventType: 13574212
#
# Descriptor: [B_s0 -> (D_s*- -> (D_s- -> (phi -> K+ K-) mu- anti-nu_mu) gamma) mu+ nu_mu]cc
#
# NickName: Bs_Dsstmunu,Dsgamma,phimunu=DecProdCut,HighVisMass,EvtGenDecayWithCut
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
# #
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(B_s0 => (D*_s- => (D_s- => (phi(1020) => K+ K-) mu- nu_mu~) gamma) mu+ nu_mu)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[B_s0]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += [
#     "visMass = ( ( GMASS ( 'mu+' == GID , 'mu-' == GID, 'K+' == GID, 'K-' == GID ) ) > 4200 * MeV )",
# ]
# EndInsertPythonCode
#
# Documentation: background for the R_phi LFU test
# selected to have a visible mass larger than 4.2 GeV using EvtGenDecayWithCutTool
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Jan-Marc Basels
# Email: jan-marc.basels@cern.ch
# Date: 20190826
# CPUTime: < 1 min
#

Alias      My_phi  phi
ChargeConj My_phi  My_phi

Alias      MyD_s*-         D_s*-
Alias      MyD_s*+         D_s*+
ChargeConj MyD_s*-         MyD_s*+

Alias      MyD_s-         D_s-
Alias      MyD_s+         D_s+
ChargeConj MyD_s-         MyD_s+


Decay B_s0sig
  #HQET2 parameter as for B->D*lnu, taken from Spring 2019 HFLAV averages:
  #https://hflav-eos.web.cern.ch/hflav-eos/semi/spring19/html/ExclusiveVcb/exclBtoDstar.html
  1.000    MyD_s*- mu+ nu_mu         PHOTOS HQET2 1.122 0.921 1.270 0.852; #rho^2 (ha1 unchanged) R1 and R2 as of HFLAG Spring 2019, normalisation factor ha1 has no impact on kinematics
Enddecay
CDecay anti-B_s0sig
#
Decay MyD_s*-
  1.000    MyD_s-  gamma             PHOTOS VSP_PWAVE;
Enddecay
CDecay MyD_s*+
#
Decay MyD_s-
  1.000    My_phi mu- anti-nu_mu     PHOTOS ISGW2;
Enddecay
CDecay MyD_s+
#
Decay My_phi
  1.000    K+ K-                     PHOTOS VSS;
Enddecay
#
End
#
