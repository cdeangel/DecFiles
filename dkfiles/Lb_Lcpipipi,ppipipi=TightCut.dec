# EventType: 15166462
#
# Descriptor: [Lambda_b0 -> (Lambda_c+ -> p+ pi- pi+ pi0) pi- pi+ pi-]cc
#
# NickName: Lb_Lcpipipi,ppipipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalPlain.addTool(LoKi__GenCutTool ,'TightCut')
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay   = '[^(Lambda_b0 ==> ^(Lambda_c+ ==> p+ pi- pi+ pi0) pi- pi+ pi-)]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, centimeter',
#     'inAcc    = in_range ( 1.80 , GETA , 5.10 )' ,
#     'goodProton = ( GPT > 0.45 * GeV ) & in_range ( 8 * GeV, GP, 160 * GeV ) & inAcc ' ,
#     'goodPionFromLc = ( GPT > 0.45 * GeV ) & ( GP > 2.9 * GeV ) & inAcc ' ,
#     'goodPionFromLb = ( GPT > 0.10 * GeV ) & ( GP > 2.9 * GeV ) & inAcc ' ,
#     'goodLc = (GNINTREE(("p+" == GABSID) & goodProton, HepMC.children) > 0) & (GNINTREE(("pi+" == GABSID) & goodPionFromLc, HepMC.children) > 1) ',
#     'goodLb = (GFAEVX(GVZ, 0) - GFAPVX(GVZ, 0) > 0.8 * millimeter) & (GPT > 0.19 * GeV) & (GNINTREE(("pi+"==GABSID) & goodPionFromLb, HepMC.children) > 2)'
#     ]
# tightCut.Cuts      =    {
#     '[Lambda_b0]cc'  : 'goodLb',
#     '[Lambda_c+]cc'  : 'goodLc'
#     }
# EndInsertPythonCode
#
# CPUTime: 16min
#
# Documentation: Lb->Lc3pi decay with Lc->p pi pi pi background channel for Lc -> p pi mu nu channel.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Han Gao
# Email: han.gao@cern.ch
# Date: 20230103
#
Alias      MyLambda_c+             Lambda_c+
Alias      Myanti-Lambda_c-        anti-Lambda_c-
ChargeConj MyLambda_c+             Myanti-Lambda_c-
#
#
Decay Lambda_b0sig
  1.0   MyLambda_c+        pi-   pi+  pi- PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
  1.0 p+       pi-      pi+ pi0 PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
End
