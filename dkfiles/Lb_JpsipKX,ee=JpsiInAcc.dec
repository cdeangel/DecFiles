# EventType: 15454001
#
# Descriptor: {[Lambda_b0 -> (J/psi -> e+ e-) p+ K- X]cc, [Lambda_b0 -> (psi(2S) -> e+ e-) p+ K- X]cc}
#
# NickName: Lb_JpsipKX,ee=JpsiInAcc
#
# Cuts: SelectedDaughterInLHCb
#
# ExtraOptions: CharmoniumInAcc
#
# Documentation: muons from J/psi or Psi(2S) in acceptance
# Partially reconstructed background for Lb_JpsipK with Jpsi to electrons
# EndDocumentation
#
# CPUTime: <1 min
# PhysicsWG: RD
# Tested: Yes
# Responsible: Vitalii Lisovskyi
# Email:   vitalii.lisovskyi@cern.ch
# Date: 20170301
#

Alias MyJ/psi J/psi
ChargeConj MyJ/psi MyJ/psi

Alias Mychi_c0 chi_c0
ChargeConj Mychi_c0 Mychi_c0

Alias Mychi_c1 chi_c1
ChargeConj Mychi_c1 Mychi_c1

Alias Mychi_c2 chi_c2
ChargeConj Mychi_c2 Mychi_c2

Alias Myeta eta
ChargeConj Myeta Myeta

Alias Mypsi(2S) psi(2S)
ChargeConj Mypsi(2S) Mypsi(2S)


Decay Myeta
 1.0000  gamma      gamma                  PHSP ;
Enddecay

Decay MyJ/psi
 1.0000  e+        e-                    PHOTOS VLL ;
Enddecay

Decay Mychi_c0
 1.0000  gamma      MyJ/psi                PHSP ;
Enddecay

Decay Mychi_c1
 1.0000  MyJ/psi    gamma                 VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ;
Enddecay

Decay Mychi_c2
 1.0000  gamma      MyJ/psi                PHSP ;
Enddecay

Decay Mypsi(2S)
 0.4763  MyJ/psi    pi+        pi-         VVPIPI ;
 0.2346  MyJ/psi    pi0        pi0         VVPIPI ;
 0.1741  mu+        mu-                    PHOTOS VLL ;
 0.0516  gamma      Mychi_c1               PHSP ;
 0.0306  MyJ/psi    Myeta                  PARTWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
 0.0281  gamma      Mychi_c2               PHSP ;
 0.0028  gamma      Mychi_c0               PHSP ;
 0.0018  MyJ/psi    pi0                    PARTWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
Enddecay

Decay Lambda_b0sig
 0.5543  MyJ/psi    p+  K-               PHSP ;
 0.1147  Mypsi(2S)  p+  K-               PHSP ;
 0.1109  MyJ/psi    p+  K-  pi0          PHSP ;
 0.0831  MyJ/psi    p+  K-  pi+  pi-     PHSP ;
 0.0458  Mychi_c1   p+  K-               PHSP ;
 0.0259  Mychi_c2   p+  K-               PHSP ;
 0.0229  Mypsi(2S)  p+  K-  pi0          PHSP ;
 0.0172  Mypsi(2S)  p+  K-  pi+  pi-     PHSP ;
 0.0092  Mychi_c1   p+  K-  pi0          PHSP ;
 0.0069  Mychi_c1   p+  K-  pi+  pi-     PHSP ;
 0.0052  Mychi_c2   p+  K-  pi0          PHSP ;
 0.0039  Mychi_c2   p+  K-  pi+  pi-     PHSP ;
Enddecay
CDecay anti-Lambda_b0sig

End
