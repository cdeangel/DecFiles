# EventType: 12297292
# NickName: Bu_DsstDsstK,KKPi,KKPi=PHSP,TightCut
# Descriptor: [B+ -> (D_s*+ -> {gamma (D_s+ => K+ K- pi+)}) (D_s*- -> {gamma (D_s- => K- K+ pi-)}) K+]cc
#
# Documentation: Decay file for B+- -> D_s*+- D_s*-+ K+- with B decay flat in dalitz plot
# EndDocumentation
# CPUTime: < 1 min
#
#Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '^[B+ -> ^(D*_s+ => ^(D_s+ => ^K+ ^K- ^pi+)) ^(D*_s- => ^(D_s- => ^K- ^K+ ^pi-)) ^K+]CC'
#tightCut.Preambulo += [
#    'GVZ = LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = (in_range(0.005, GTHETA, 0.400) & in_range ( 1.8 , GETA , 5.2))',
#    'goodB        = (GP > 25000 * MeV) & (GPT > 1500 * MeV)',
#    'goodD        = (GP > 8000 * MeV) & (GPT > 400 * MeV)',
#    'goodK        = in_range( 1.3 * GeV , GP , 200 * GeV) & (GPT >  80 * MeV)',
#    'goodPi       = in_range( 1.3 * GeV , GP , 200 * GeV) & (GPT >  80 * MeV)',
#]
#tightCut.Cuts = {
#    '[B+]cc'   : 'goodB',
#    '[D*_s+]cc'   : 'goodD',
#    '[D_s+]cc' : 'goodD',
#    '[K+]cc'   : 'inAcc & goodK',
#    '[pi+]cc'  : 'inAcc & goodPi',
#    }
#EndInsertPythonCode
# 
# Date:   20210105
#
# Responsible: Huanhuan Liu
# Email: hhliu@ucas.ac.cn
# PhysicsWG: B2OC
#
# Tested: Yes

Alias My_D1_s-    D_s-
Alias My_D1_s+    D_s+
ChargeConj  My_D1_s-    My_D1_s+

Alias My_D2_s-    D_s-
Alias My_D2_s+    D_s+
ChargeConj  My_D2_s-    My_D2_s+

Alias My_D1_s*+            D_s*+
Alias My_D1_s*-            D_s*-
ChargeConj My_D1_s*-   My_D1_s*+

Alias My_D2_s*+            D_s*+
Alias My_D2_s*-            D_s*-
ChargeConj My_D2_s*-   My_D2_s*+

Decay B+sig
  1.0 My_D1_s*+ My_D2_s*- K+ PHSP;
Enddecay
CDecay B-sig

Decay My_D1_s*+
0.935   My_D1_s+  gamma          VSP_PWAVE;
0.058   My_D1_s+  pi0            VSS;
0.007   My_D1_s+  e+ e-           PHSP;
Enddecay
CDecay My_D1_s*-

Decay My_D2_s*+
0.935   My_D2_s+  gamma          VSP_PWAVE;
0.058   My_D2_s+  pi0            VSS;
0.007   My_D2_s+  e+ e-           PHSP;
Enddecay
CDecay My_D2_s*-

Decay My_D1_s+
  1.0   K+  K-   pi+   D_DALITZ;
Enddecay
CDecay My_D1_s-

Decay My_D2_s-
  1.0   K-  K+   pi-   D_DALITZ;
Enddecay
CDecay My_D2_s+

End
