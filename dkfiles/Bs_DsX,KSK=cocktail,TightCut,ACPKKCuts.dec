# EventType: 13774300
#
# Descriptor: [ B_s0 --> (D_s- => (K_S0 => pi+ pi-) K-) ... ]CC
# NickName: Bs_DsX,KSK=cocktail,TightCut,ACPKKCuts
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Inclusive D_s => (K_S0 => pi pi) K produced in B_s decays, with cuts optimized for ACPKK
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# signal     = Generation().SignalRepeatedHadronization
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '[ [B_s0]cc --> ^(D_s- => ^(KS0 => pi+ pi-) ^K-) ... ]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV, GeV, micrometer ',
#     'import math',
#     'LL_KS0      = ( GNINTREE( ("pi+" == GABSID ) & in_range( 2 , GETA , 4.5 ) ) > 1.5 ) & ( GFAEVX(GVZ,0) > -100.0 * mm ) & ( GFAEVX(GVZ,0) < 500.0 * mm ) & ( GFAEVX(abs(GVX),0) < 40.0 * mm ) & ( GFAEVX(abs(GVY),0) < 40.0 * mm ) & GVEV',
#     'HarmCuts_Kp   = in_range (2 , GETA ,  4.5) & (GPT > 1.4 *GeV) & (GP > 4.9 *GeV)',
#     'HarmCuts_KS0  = in_range (2 , GETA ,  4.5)',
#     'HarmCuts_Dp  =  in_range (2 , GETA ,  4.5) & ( GPT > 2.9 *GeV )',
# ]		   
# tightCut.Cuts      =    {
#     '[K-]cc'         : 'HarmCuts_Kp',
#     'KS0'             : 'LL_KS0 & HarmCuts_KS0',
#     '[D_s-]cc'          : 'HarmCuts_Dp',
# }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Federico Betti
# Email: federico.betti@cern.ch
# Date: 20200910
# CPUTime: 3 min
#
#
Alias        MyK0s   K_S0
ChargeConj   MyK0s   MyK0s
#
Alias      MyD_s-     D_s-
Alias      MyD_s+     D_s+
ChargeConj MyD_s+     MyD_s-
#
Alias      MyD_s*+    D_s*+
Alias      MyD_s*-    D_s*-
ChargeConj MyD_s*+    MyD_s*-
#
Alias      MyD_s1+    D_s1+
Alias      MyD_s1-    D_s1-
ChargeConj MyD_s1-    MyD_s1+
#
Alias      MyD_s0*+   D_s0*+
Alias      MyD_s0*-   D_s0*-
ChargeConj MyD_s0*+   MyD_s0*-
#
Decay B_s0sig
  0.0210   MyD_s-     mu+    nu_mu	 HQET2 1.131 1.; #rho^2 v1; HFLAG Spring 2019; v1 set to dumb value (does not affect kinematics); BR taken from DECAY.DEC;
  0.0490   MyD_s*-    mu+    nu_mu   	 HQET2 1.122 0.910 1.270 0.852; #rho^2 ha1 R1 R2; HFLAG Spring 2019; ha1 does not affect kinematics; BR taken from DECAY.DEC;
  0.0040   MyD_s1-    mu+    nu_mu   	  ISGW2; #BR taken from DECAY.DEC;
  0.0040   MyD_s0*-   mu+    nu_mu   	  ISGW2; #BR taken from DECAY.DEC;
  0.0210   MyD_s-     e+    nu_e   	 HQET2 1.131 1.; #rho^2 v1; HFLAG Spring 2019; v1 set to dumb value (does not affect kinematics); BR taken from DECAY.DEC;
  0.0490   MyD_s*-    e+    nu_e   	 HQET2 1.122 0.910 1.270 0.852; #rho^2 ha1 R1 R2; HFLAG Spring 2019; ha1 does not affect kinematics; BR taken from DECAY.DEC;
  0.0040   MyD_s1-    e+    nu_e   	  ISGW2; #BR taken from DECAY.DEC;
  0.0040   MyD_s0*-   e+    nu_e   	  ISGW2; #BR taken from DECAY.DEC;
  0.00797  MyD_s-     tau+   nu_tau    ISGW2;
  0.016    MyD_s*-    tau+   nu_tau    ISGW2;
  0.00127  MyD_s1-    tau+   nu_tau    ISGW2;
  0.0022   MyD_s0*-   tau+   nu_tau    ISGW2;
  0.003    MyD_s-     pi+      		 PHSP;
  0.0069   rho+       MyD_s-  		 SVS;
  0.0061   MyD_s-     pi+   pi+   pi-  	 PHSP;
  0.000227 MyD_s-     K+ 		 PHSP;
  0.000227 MyD_s+     K-		 PHSP;
  0.00032  MyD_s-     K+   pi+   pi- 	 PHSP;
  0.0022   MyD_s+     D_s- 		 PHSP;
  0.0022   D_s+       MyD_s- 		 PHSP;
  0.00028  MyD_s-     D+		 PHSP;
  0.002    MyD_s*-    pi+		 SVS;
  0.000133 MyD_s*-    K+ 		 SVS;
  0.000133 MyD_s*+    K-		 SVS;
  0.0096   MyD_s*-    rho+ 		 SVV_HELAMP  1.0 0.0 1.0 0.0 1.0 0.0;
  0.003475 MyD_s*-    D_s+		 SVS; 
  0.003475 D_s*-      MyD_s+		 SVS; 
  0.003475 MyD_s*+    D_s-		 SVS; 
  0.003475 D_s*+      MyD_s-		 SVS;
  0.0072   MyD_s*-    D_s*+     	 SVV_HELAMP  1.0 0.0 1.0 0.0 1.0 0.0;
  0.0072   D_s*-      MyD_s*+     	 SVV_HELAMP  1.0 0.0 1.0 0.0 1.0 0.0;
Enddecay
CDecay anti-B_s0sig
#
Decay MyD_s*+
  0.935   MyD_s+  gamma                VSP_PWAVE;
  0.058   MyD_s+  pi0                  VSS;
  0.0067  MyD_s+  e+ e-		       PHSP;
Enddecay
CDecay MyD_s*-
#
Decay MyD_s1+
# from PDG 2019
  0.48	MyD_s*+	pi0	 PARTWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.18	MyD_s+ gamma	 VSP_PWAVE;
  0.043	MyD_s+ pi+ pi-	 PHSP;
  0.08	MyD_s*+ gamma	 PHSP;
  0.037	MyD_s0*+ gamma	 PHSP;
Enddecay
CDecay MyD_s1-
#
Decay MyD_s0*+
  1.000   MyD_s+   pi0                  PHSP;
Enddecay
CDecay MyD_s0*-
#
Decay MyD_s+
  1.000        MyK0s         K+               PHSP;
Enddecay
CDecay MyD_s-
#
Decay MyK0s
  1.000        pi+           pi-              PHSP;
Enddecay
#
End
#
