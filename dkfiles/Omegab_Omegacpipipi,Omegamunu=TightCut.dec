# EventType: 16577130
#
# Descriptor: [Xi_b- -> (Omega_c0 ->  (Omega- -> (Lambda0 -> p+ pi-) K-) mu+ nu_mu) pi- pi+ pi-]cc
#
# NickName:  Omegab_Omegacpipipi,Omegamunu=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# ParticleValue: "Xi_b- 122 5132 -1.0 6.0452 1.64e-012 Xi_b- 5132 0.000000e+000", "Xi_b~+ 123 -5132 1.0 6.0452 1.64e-012 anti-Xi_b+ -5132 0.000000e+000"
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalRepeatedHadronization.addTool(LoKi__GenCutTool ,'TightCut')
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay   = '[^(Xi_b- ==> ^(Omega_c0 ==> ^(Omega- ==> ^(Lambda0 ==> p+ pi-) K-) mu+ nu_mu) pi- pi+ pi-)]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, centimeter',
#     'inAcc    = in_range ( 1.80 , GETA , 5.10 )' ,
#     'goodLmd = ( GPT > 0.75 * GeV ) & ( GP > 4.95 * GeV ) & (GFAEVX( GVZ , 9999 * mm ) < 2500 * mm)',
#     'goodKaonFromOmega  = ( GPT > 0.5 * GeV ) & ( GP > 3.0 * GeV )  & inAcc ' ,
#     'goodMuonFromOmegac = ( GPT > 0.5 * GeV ) & ( GP > 3.0 * GeV ) & inAcc ' ,
#     'goodPionFromOmegab = ( GPT > 0.15 * GeV ) & inAcc ' ,
#     'goodOmega  = (GNINTREE(("K+" == GABSID) & goodKaonFromOmega, HepMC.children) > 0 )',
#     'goodOmegac = (GNINTREE(("mu+" == GABSID) & goodMuonFromOmegac, HepMC.children) > 0 )',
#     'goodOmegab = (GNINTREE(("pi+"==GABSID) & goodPionFromOmegab, HepMC.children) > 2)'
#     ]
# tightCut.Cuts      =    {
#     '[Xi_b-]cc'  : 'goodOmegab',
#     '[Omega_c0]cc'  : 'goodOmegac',
#     '[Omega-]cc'    : 'goodOmega',
#     '[Lambda0]cc'    : 'goodLmd'
#     }
# EndInsertPythonCode
#
# CPUTime: 16min
#
# Documentation: Omegab->Omegacpipipi decay with Omegac->Omega mu nu
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Ying Liu 
# Email: ying.liu@cern.ch
# Date: 20230103
#
Alias      MyOmega_c0             Omega_c0
Alias      Myanti-Omega_c0        anti-Omega_c0
ChargeConj MyOmega_c0             Myanti-Omega_c0
#
#
Alias      MyOmega     Omega-
Alias      Myanti-Omega anti-Omega+
ChargeConj Myanti-Omega MyOmega
#
Alias      MyLambda      Lambda0
Alias      Myanti-Lambda anti-Lambda0
ChargeConj Myanti-Lambda MyLambda
# 
#
Decay Xi_b-sig
  1.0   MyOmega_c0  pi- pi+ pi- PHSP;
Enddecay
CDecay anti-Xi_b+sig 
#
Decay MyOmega_c0
  1.0  MyOmega  mu+ nu_mu  PHSP;
Enddecay
CDecay Myanti-Omega_c0
#
Decay MyOmega
  1.0   MyLambda   K-    PHSP;
Enddecay
CDecay Myanti-Omega
#
Decay MyLambda
  1.0   p+   pi-         PHSP;
Enddecay
CDecay Myanti-Lambda
#
End
