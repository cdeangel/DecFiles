# EventType: 26184400
#
# Descriptor: [Sigma_c0 -> (Lambda_c+ -> p+ e- e+  pi0) pi-]cc
#
# NickName: Sc0_Lcpi,peepi,phsp=TightCut
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Sigma_c0 -> Lambda_c+pi-, Lambda_c -> p e e pi PHSP MODEL, 
# GL cuts, double arrows in Decay
# EndDocumentation
#
#
# PhysicsWG:   Charm
# Tested:      Yes
# Responsible: Marcin Chrzaszcz, Jolanta Brodzicka
# Email:       mchrzasz@cern.ch, jolanta.brodzicka@cern.ch
# Date:        20190725
# 
# CPUTime: 6min
#
# InsertPythonCode:
#
#
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[ Sigma_c0 => ^( Lambda_c+ => ^p+ ^e- ^e+ ^( pi0 -> ^gamma ^gamma )) pi- ]CC'
# tightCut.Cuts      =    {
#     '[e+]cc'       : ' goodElectron & inAcc & inCaloAcc ',
#     '[p+]cc'        : ' goodProton & inAcc ', 
#     '[pi0]cc'       : ' goodPi0 & inAcc ', 
#     '[gamma]cc'     : ' goodPi0Gamma & inCaloAcc ',
#     '[Lambda_c+]cc' : ' goodLambdac & ~GHAS (GBEAUTY, HepMC.ancestors)' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter, micrometer',
#     'inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) ', 
#     'inCaloAcc   = ( in_range(0.000, abs(GPX/GPZ), 0.300) & in_range(0.000, abs(GPY/GPZ), 0.250) & (GPZ > 0) ) ',
#     'goodElectron   = ( GPT > 0.2 * GeV ) & ( GP > 0.2 * GeV ) ',
#     'goodProton = ( GPT > 0.2 * GeV ) & ( GP > 0.2 * GeV ) ', 
#     'goodPi0 = ( GPT > 0.1 * GeV ) ',
#     'goodPi0Gamma = ( GPT > 0.05 * GeV ) ',
#     'goodLambdac  = ( GTIME > 50 * micrometer ) ' ]
#
# EndInsertPythonCode
#


Alias MyLambda_c+                   Lambda_c+
Alias Myanti-Lambda_c-              anti-Lambda_c-
ChargeConj MyLambda_c+              Myanti-Lambda_c-
#
Decay Sigma_c0sig
  1.000    MyLambda_c+  pi-                     PHSP;
Enddecay
CDecay anti-Sigma_c0sig
#
Decay MyLambda_c+
  1.00000         p+      e-      e+    pi0     PHSP;
Enddecay
CDecay Myanti-Lambda_c-

End
#
