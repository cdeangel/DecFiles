# EventType: 12245073
#
# Descriptor: [B+ -> (J/psi -> mu+ mu-) (K_1+ -> (K*0 -> K+ pi-) pi+)]cc
#
# NickName: Bu_JpsiKpipi,mm=DecProdCut,K1cocktail
#
# Cuts: DaughtersInLHCb
#
# CPUTime: < 1 min
#
# Documentation: B+ -> J/psi(1S) K+ pi+ pi- phsp decay, J/psi -> mu+ mu-, and Kpipi as a cocktail, all daughters in acceptance
# EndDocumentation
# 
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Biplab Dey
# Email: biplab.dey@.cern.ch
# Date: 20180527


Alias       MyJ/psi      J/psi
ChargeConj  MyJ/psi      MyJ/psi
#
Alias      Myrho0     rho0
ChargeConj Myrho0     Myrho0
#
Alias      MyK_1-     K_1-
Alias      MyK_1+     K_1+
ChargeConj MyK_1+     MyK_1-
#
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#

LSNONRELBW MyK_1+
BlattWeisskopf MyK_1+ 0.0
Particle MyK_1+ 1.272 0.15
ChangeMassMin MyK_1+ 0.5
ChangeMassMax MyK_1+ 2.5

LSNONRELBW MyK_1-
BlattWeisskopf MyK_1- 0.0
Particle MyK_1- 1.272 0.15
ChangeMassMin MyK_1- 0.5
ChangeMassMax MyK_1- 2.5

LSNONRELBW Myrho0
BlattWeisskopf Myrho0 0.0
Particle Myrho0 0.775 0.15
ChangeMassMin Myrho0 0.2
ChangeMassMax Myrho0 3.3

LSNONRELBW MyK*0
BlattWeisskopf MyK*0 0.0
Particle MyK*0 0.896 0.06
ChangeMassMin MyK*0 0.5
ChangeMassMax MyK*0 3.5

LSNONRELBW Myanti-K*0
BlattWeisskopf Myanti-K*0 0.0
Particle Myanti-K*0 0.896 0.06
ChangeMassMin Myanti-K*0 0.5
ChangeMassMax Myanti-K*0 3.5


Decay B+sig
  0.34   MyJ/psi K+ pi+ pi-         PHSP;
  0.66   MyJ/psi MyK_1+             PHSP;
Enddecay
CDecay B-sig
#
Decay MyK_1+
  0.5  K+ Myrho0            PHSP;
  0.5  MyK*0 pi+            PHSP;
Enddecay
CDecay MyK_1-
#
Decay MyK*0
  1.000 K+   pi-                  PHSP;
Enddecay
CDecay Myanti-K*0
#
Decay Myrho0
  1.000 pi+  pi-            PHSP;
Enddecay
#
Decay MyJ/psi
  1.0000   mu+   mu-        PHOTOS PHSP;
Enddecay
#
End
