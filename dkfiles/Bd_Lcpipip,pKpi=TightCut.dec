# EventType: 11166004
#
# Descriptor: [B0 -> Myanti-Lambda_c- p+ pi+ pi-]cc
# 
# NickName: Bd_Lcpipip,pKpi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[B0 ==> (Lambda_c~- ==> ^p~- ^K+ ^pi-) ^p+ ^pi- ^pi+]CC"
# tightCut.Preambulo += [
# "from GaudiKernel.SystemOfUnits import MeV",
# "InAcc = in_range ( 0.005 , GTHETA , 0.400 ) & in_range ( 1.8 , GETA , 5.2 )",
# "goodKpi = ( GP > 2500 * MeV ) & ( GPT > 200 * MeV) & InAcc",
# "goodp = ( GP > 9000 * MeV ) & ( GPT > 200 * MeV ) & InAcc"
#]
#tightCut.Cuts = {
#'[pi-]cc' : "goodKpi",
#'[K-]cc' : "goodKpi",
#'[p+]cc' : "goodp"
#}
#
# EndInsertPythonCode
#
# Documentation: This is the decay file for the decay B0 -> (anti-Lambda_c- -> p~- K+ pi-) p+ pi- pi+.
# The B0 is forced to decay hadronically to Lambda_c+ p- pi- pi+.
# The Lambda_c is forced to the p+ K- pi+ final state, through several intermediate resonances.
# With tight generator level cut.
# EndDocumentation
#
# CPUTime: < 1 min
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Qiuchan Lu
# Email: qiuchan.lu@cern.ch
# Date: 20210325
#

# Define Lambda_c
Alias      MyLambda_c+      Lambda_c+
Alias      Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+      Myanti-Lambda_c-

# Define Lambda(1520)0
Alias      MyLambda(1520)0       Lambda(1520)0
Alias      Myanti-Lambda(1520)0  anti-Lambda(1520)0
ChargeConj MyLambda(1520)0       Myanti-Lambda(1520)0

# Define K*0
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0

# Define Delta++
Alias      MyDelta++      Delta++
Alias      Myanti-Delta-- anti-Delta--
ChargeConj MyDelta++      Myanti-Delta--

# Define B0 decay
Decay B0sig
  1.000 Myanti-Lambda_c- p+  pi- pi+ PHSP;
Enddecay
CDecay anti-B0sig

# Define Lambda_c+ decay
# Resonant contributions taken from 2012 PDG
Decay MyLambda_c+
  0.02800 p+              K-         pi+ PHSP;
  0.01600 p+              Myanti-K*0     PHSP;
  0.00860 MyDelta++       K-             PHSP;
  0.01800 MyLambda(1520)0 pi+            PHSP;
Enddecay
CDecay Myanti-Lambda_c-

#Define Lambda(1520)0 decay
Decay MyLambda(1520)0
  1.000 p+ K- PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0 

# Define K*0 decay
Decay MyK*0
  1.000 K+ pi- VSS;
Enddecay
CDecay Myanti-K*0

#Define Delta++ decay
Decay MyDelta++
  1.000 p+ pi+ PHSP;
Enddecay
CDecay Myanti-Delta--

End
