# EventType: 12666000
#
# Descriptor: {[ B+ ==> (tau+ -> pi+ pi- pi+ anti-nu_tau) nu_tau (D- -> K+ pi- pi-) ]cc}
#
# NickName: Bu_Dststtaunu,D-=cocktail,3pinu3pipi0nu,tauolababar,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal = generation.SignalRepeatedHadronization
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut = signal.TightCut
# tightCut.Decay = '[ ( (Beauty & LongLived) --> (D- => K+ pi- pi-) pi- pi+ ...) ]CC'
# tightCut.Preambulo += [
#      'from GaudiKernel.SystemOfUnits import MeV'
#     ,'inAcc = ( 0 < GPZ )  &  ( 200 * MeV < GPT ) & ( 1200 * MeV < GP ) & in_range ( 1.8 , GETA , 5.0 ) & in_range ( 0.005 , GTHETA , 0.400 )'
#     ,"goodDp = GINTREE(( 'D-'  == GABSID ) & (GP>8000*MeV) & (GPT>1000*MeV) & ( GNINTREE(( 'K-' == GABSID ) & ( GPT > 1400*MeV ) & ( GP > 4000*MeV ) & inAcc, HepMC.descendants) == 1 ) & ( GNINTREE(( 'pi+' == GABSID ) & ( GPT > 200*MeV ) & ( GP > 1600*MeV ) & inAcc, HepMC.descendants) == 2 ))"
#     ,"nPiB = GCOUNT(('pi+' == GABSID) & inAcc & ( GNINTREE ( ('KS0' == GABSID) | ('KL0' == GABSID), HepMC.parents)==0 ), HepMC.descendants)"
#     ,"goodB = ( ( GNINTREE(  ( 'D-'==GABSID ) , HepMC.descendants) == 1 ) & ( nPiB >= 5) )"
# ]
# tightCut.Cuts = {
#     '[D-]cc': 'goodDp',
#     '[B+]cc': 'goodB'
#     }
# EndInsertPythonCode
#
# Documentation: Sum of B -> D** tau nu modes. D** -> D*+ X, D* -> D0 pi, D0 -> K pi. Cuts for B -> D* tau nu, tau-> 3pi #analysis.
# EndDocumentation
#
# PhysicsWG: B2SL
# CPUTime: 10 min
# Tested: Yes
# Responsible: Ricardo Vazquez Gomez
# Email: rvazquez@cern.ch
# Date: 20210421
#

##############
#
# Tauola steering options
# # The following forces the tau to decay into 3 charged pions (not pi+2pi0)
Define TauolaCurrentOption 1
Define TauolaBR1 1.0
#
Alias      Mytau+         tau+
Alias      Mytau-         tau-
ChargeConj Mytau+         Mytau-
#
Alias      MyD0         D0
Alias      MyAntiD0     anti-D0
ChargeConj MyD0         MyAntiD0
#
Alias         MyD-              D-
Alias         MyD+              D+
ChargeConj    MyD-              MyD+
#
Alias      MyD*-        D*-
Alias      MyD*+        D*+
ChargeConj MyD*-        MyD*+
#
Alias      MyD*0        D*0
Alias      MyAntiD*0    anti-D*0
ChargeConj MyD*0        MyAntiD*0
#
Alias      MyD_10         D_10
Alias      MyAntiD_10     anti-D_10
ChargeConj MyD_10         MyAntiD_10
#
Alias      MyD_1+         D_1+
Alias      MyD_1-         D_1-
ChargeConj MyD_1+         MyD_1-
#
Alias      MyD_0*+         D_0*+
Alias      MyD_0*-         D_0*-
ChargeConj MyD_0*+         MyD_0*-
#
Alias      MyD_0*0         D_0*0
Alias      MyAntiD_0*0     anti-D_0*0
ChargeConj MyD_0*0         MyAntiD_0*0
#
Alias      MyD'_10         D'_10
Alias      MyAntiD'_10     anti-D'_10
ChargeConj MyD'_10         MyAntiD'_10
#
Alias      MyD'_1+         D'_1+
Alias      MyD'_1-         D'_1-
ChargeConj MyD'_1+         MyD'_1-
#
Alias      MyD_2*+         D_2*+
Alias      MyD_2*-         D_2*-
ChargeConj MyD_2*+         MyD_2*-
#
Alias      MyD_2*0         D_2*0
Alias      MyAntiD_2*0     anti-D_2*0
ChargeConj MyD_2*0         MyAntiD_2*0
#
Decay B-sig
                
  0.002072   MyD_0*0     Mytau-  anti-nu_tau     PHOTOS  ISGW2;               
  0.0027   MyD'_10     Mytau-  anti-nu_tau     PHOTOS  ISGW2;               
  0.003   MyD_10      Mytau-  anti-nu_tau     PHOTOS  ISGW2;               
  0.001   MyD_2*0     Mytau-  anti-nu_tau     PHOTOS  ISGW2;               
     
Enddecay
CDecay B+sig
#
SetLineshapePW MyD_1+ MyD*+ pi0 2
SetLineshapePW MyD_1- MyD*- pi0 2
SetLineshapePW MyD_10 MyD*+ pi- 2
SetLineshapePW MyAntiD_10 MyD*- pi+ 2
#
SetLineshapePW MyD_2*+ MyD*+ pi0 2
SetLineshapePW MyD_2*- MyD*- pi0 2
SetLineshapePW MyD_2*0 MyD*+ pi- 2
SetLineshapePW MyAntiD_2*0 MyD*- pi+ 2
#
Decay Mytau-
  0.6666        TAUOLA 5;
  0.3333        TAUOLA 8;
Enddecay
CDecay Mytau+
#
Decay MyD+
  1.000  K-  pi+ pi+                           D_DALITZ;
Enddecay
CDecay MyD-
#
Decay MyD0
  1.000   K-  pi+                              PHOTOS PHSP;
Enddecay
CDecay MyAntiD0
#
CDecay MyAntiD*0
#
Decay MyD*+
  0.950  MyD+   pi0                VSS;
  0.050  MyD+   gamma              VSP_PWAVE;
Enddecay
CDecay MyD*-
#
Decay MyD_0*+ 
  0.0271     MyD*+ pi0 pi0                     PHOTOS PHSP;
  0.0542     MyD*+ pi+ pi-                     PHOTOS PHSP;
Enddecay
CDecay MyD_0*-
#
Decay MyD_0*0
  0.0542     MyD*+ pi- pi0                     PHOTOS PHSP;
Enddecay
CDecay MyAntiD_0*0
#
Decay MyD'_1+
  0.1693    MyD*+ pi0                          PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyD'_1-
#
Decay MyD'_10
  0.3385    MyD*+ pi-                          PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyAntiD'_10
#
Decay MyD_1+
  0.1354    MyD*+ pi0                          PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
  0.1146    MyD_0*0 pi+                        PHOTOS PHSP;
  0.0903   MyD_0*+ pi0                         PHOTOS PHSP;
Enddecay
CDecay MyD_1-
#
Decay MyD_10
  0.2708   MyD*+ pi-                           PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
  0.1806    MyD_0*+ pi-                        PHOTOS PHSP;
  0.0573    MyD_0*0 pi0                        PHSP;
Enddecay
CDecay MyAntiD_10
#
Decay MyD_2*+
  0.0587    MyD*+ pi0                          PHOTOS TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
  0.0647    MyD_0*0 pi+                        PHOTOS PHSP;
  0.0509    MyD_0*+ pi0                        PHOTOS PHSP;
  0.0027     MyD*+ pi0 pi0                     PHOTOS PHSP;
  0.0054    MyD*+ pi+ pi-                      PHOTOS PHSP;
Enddecay
CDecay MyD_2*-
#
Decay MyD_2*0
  0.1173    MyD*+ pi-                          PHOTOS TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
  0.1019    MyD_0*+ pi-                        PHOTOS PHSP;
  0.0323    MyD_0*0 pi0                        PHSP;
  0.0054     MyD*+ pi- pi0                     PHOTOS PHSP;
Enddecay
CDecay MyAntiD_2*0
#
End
#
