# EventType: 12199003
# NickName: Bu_Dst+D-pi+,D0Pi,K3Pi,KPiPi=sqDalitz,TightCut
# Descriptor: [B+ -> (D*(2010)+ => (D0 => K- pi+ pi+ pi-) pi+) (D- => K+ pi- pi-) pi+]cc
#
# Documentation: Decay file for B+ -> D*+ D- pi+, with D0 goes to K3Pi. B decay forced flat in square dalitz plot.
# EndDocumentation
#
#Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '[B+ => (D*(2010)+ ==> (D0 ==> ^K- ^pi+ ^pi+ ^pi-) ^pi+) (D- ==>  ^K+ ^pi- ^pi-) ^pi+]CC'
#tightCut.Preambulo += [
#    'GVZ = LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = (in_range(0.005, GTHETA, 0.400) & in_range ( 1.8 , GETA , 5.2))',
#    'goodK        = (GP > 1.3 * GeV) & (GPT >  80 * MeV)',
#    'goodPi       = (GP > 1.3 * GeV) & (GPT >  80 * MeV)',
#]
#tightCut.Cuts = {
#    '[K+]cc'   : 'inAcc & goodK',
#    '[pi+]cc'  : 'inAcc & goodPi'
#    }
#EndInsertPythonCode
# 
# Date:   20230322
#
# Responsible: Yajing Wei
# Email: yajing.wei@cern.ch
# PhysicsWG: B2OC
# CPUTime: < 1 min
#
# Tested: Yes

Alias My_D0   D0
Alias My_anti-D0 anti-D0
Alias My_D+   D+
Alias My_D-   D-
Alias My_D*+  D*+
Alias My_D*-  D*-

ChargeConj My_anti-D0 My_D0
ChargeConj My_D- My_D+
ChargeConj My_D*- My_D*+

Decay My_D-
  1.0 K+ pi- pi-   D_DALITZ;
Enddecay
CDecay My_D+

Decay My_D0
  1.0 K- pi+ pi+ pi-   LbAmpGen DtoKpipipi_v2;
Enddecay
CDecay My_anti-D0

Decay My_D*+
  1.0 My_D0 pi+  PHSP;
Enddecay
CDecay My_D*-

Decay B+sig
  1 My_D*+ My_D- pi+ FLATSQDALITZ;
Enddecay
CDecay B-sig

End
