# EventType: 28196052
#
# Descriptor: chi_c2(1P) => ( D+ ==> K- pi+ pi+) ( D- ==> K+ pi- pi- ) 
#
# ParticleValue: "chi_c2(1P) 765 445 0.0 3.773 -0.0273 chi_c2 445 0.5"
#
# NickName: psi3770_D+D-=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Decay of psi(3770) state into D+D- final state 
#  - since production of psi(3770) in pythia is *extremely* slow process, 
#    psi(3770) is generated as  chi_c2(1P) (it speedups the process with a factor close to 100)
#    with mass/width from PDG. 
#     - mass  is 3.773+-0.35 MeV
#     - width is 27.2+-1.0 MeV  
#  - tight cuts for the D+ mesons and all final state stable particles are applied 
#  - two very nice tricks by Michael Wilkinson are used: 
#    - only charmonium production is activated for Pythia8
#    - D+ lifetime cut is applied via EvtGenDecayWithCutTool
#  - CPU performance is  <1.5seconds/event  (538 seconds/400 events) 
#  - integrated efficiency for generator-level cuts is (2.79+-0.19)% as reported in GeneratorLog.xml
#  - efficiency for D+ lifetime cuts     is (61.59+-0.45)% (must be 62.81% == exp(-75um/311.78um)**2)
#  - efficiency for generator-level cuts is ( 5.60+-0.27)% 
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool, ToolSvc, EvtGenDecayWithCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal     = generation.SignalPlain
#
# signal.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool ( EvtGenDecayWithCutTool )
# evtgen = ToolSvc().EvtGenDecayWithCutTool 
# 
# evtgen.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgen.CutTool   = "LoKi::GenCutTool/CharmLongLived"
# evtgen.addTool( LoKi__GenCutTool , 'CharmLongLived' )
# long_lived =  evtgen.CharmLongLived 
# long_lived.Decay      = ' Meson => ^D+ ^D- '
# long_lived.Preambulo += [ 'from GaudiKernel.SystemOfUnits import micrometer ' ]
# long_lived.Cuts       = { '[D+]cc' : ' ( 75 * micrometer < GTIME ) ' }
# # Generator efficiency histos (must be flat here)
# long_lived.XAxis = ( "GPT/GeV" , 0.0 , 25.0 , 25 )
# long_lived.YAxis = ( "GY"      , 2.0 ,  4.5 , 10 )
#
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut            = signal.TightCut
# tightCut.Decay      = 'Meson => ^( D+ ==> ^K- ^pi+ ^pi+) ^( D- ==> ^K+ ^pi- ^pi-)'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV',
#     'inAcc          =  in_range ( 0.005 , GTHETA , 0.400 )                     ' ,
#     'inEta          =  in_range ( 1.95  , GETA   , 5.050 )                     ' ,
#     'fastTrack      =  ( GPT > 220 * MeV ) & ( GP > 3.0 * GeV )                ' , 
#     'goodTrack      =  inAcc & inEta & fastTrack                               ' ,
#     'inY            =  in_range ( 1.9   , GY     , 4.6   )                     ' ,
#     'goodCharm      =  inY & ( GPT > 0.9 * GeV ) & ( 75 * micrometer < GTIME ) ' ]
# tightCut.Cuts       =    {
#     '[D+]cc'    : 'goodCharm' , ## lifetime cuy is reapplied again 
#     '[K+]cc'    : 'goodTrack' ,
#     '[pi+]cc'   : 'goodTrack' ,
#     }
# # Generator efficiency histos:
# tightCut.XAxis = ( "GPT/GeV" , 0.0 , 25.0 , 25 )
# tightCut.YAxis = ( "GY"      , 2.0 ,  4.5 , 10 )
# # 
# # -- modify Pythia8 to only generate from Charmonium processes -- #
# from Configurables import Generation, MinimumBias, Pythia8Production, Inclusive, SignalPlain, SignalRepeatedHadronization, Special
#
# Pythia8TurnOffMinbias  = [ "SoftQCD:all     = off" ]
# Pythia8TurnOffMinbias += [ "Bottomonium:all = off" ]
# Pythia8TurnOffMinbias += [ "Charmonium:all  =  on" ]
#
# gen = Generation()
# gen.addTool( MinimumBias , name = "MinimumBias" )
# gen.MinimumBias.ProductionTool = "Pythia8Production"
# gen.MinimumBias.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.MinimumBias.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( Inclusive , name = "Inclusive" )
# gen.Inclusive.ProductionTool = "Pythia8Production"
# gen.Inclusive.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.Inclusive.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( SignalPlain , name = "SignalPlain" )
# gen.SignalPlain.ProductionTool = "Pythia8Production"
# gen.SignalPlain.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.SignalPlain.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( SignalRepeatedHadronization , name = "SignalRepeatedHadronization" )
# gen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
# gen.SignalRepeatedHadronization.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.SignalRepeatedHadronization.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( Special , name = "Special" )
# gen.Special.ProductionTool = "Pythia8Production"
# gen.Special.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.Special.Pythia8Production.Commands += Pythia8TurnOffMinbias
# # -- END  -- #
# EndInsertPythonCode
#
# CPUTime: < 1 min
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Vanya BELYAEV
# Email: Ivan.Belyaev@itep.ru
# Date: 20180917
#

Alias      MyD+               D+
Alias      MyD-               D-
ChargeConj MyD+             MyD-

#
Decay  chi_c2sig
  1.0  MyD+ MyD-    PHSP     ;
Enddecay
#
Decay  MyD+
  1.0  K-  pi+  pi+ D_DALITZ ;
Enddecay
CDecay MyD-
#
End

