# EventType: 20462000
#
# Descriptor: pp -> {(Z0 -> {mu+ mu-)) c}cc
#
# NickName: Zcharm=mumu,charged,InAcc
#
# Cuts: None
# FullEventCuts: LoKi::FullGenEventCut/ZcjetCut
#
# InsertPythonCode:
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool( LoKi__FullGenEventCut, "ZcjetCut" )
# zccut = Generation().ZcjetCut
# zccut.Code = " ( (count ( isGoodc ) > 0) &(count(isGoodZ) > 0))  "
# zccut.Preambulo += [
#      "from GaudiKernel.SystemOfUnits import  GeV, mrad"
#    , "isGoodc     = ((GCHARM) & (GINTREE((GCHARM) & (GTHETA < 420.0*mrad ))) &  (GINTREE(('Z0' == GABSID))) )"
#    , "isGoodZ     = (('Z0' == GABSID ) & (GNINTREE(('mu+' == GABSID) & ( GTHETA < 420.0*mrad ) )>1))"
#    ]
# Generation().Inclusive.Pythia8Production.Commands = Generation().Special.Pythia8Production.Commands
# EndInsertPythonCode
#
# ExtraOptions: Zmumuqjet
# CPUTime: < 1 min
# Documentation: Z+charm production, 2 muon (from the Z) in LHCb acceptance and c hadrons decauoing to suitable final states, Pythia8 
# EndDocumentation
#
# PhysicsWG: EW
# Tested: Yes
# Responsible: Albert Bursche
# Email: bursche@cern.ch
# Date: 20220201
#
Decay D0
  0.25   K- pi+ PHSP;
  0.25   K+ pi- PHSP;
  0.25   pi- pi+ PHSP;
  0.25   K- K+ PHSP;
Enddecay
Decay D+
  0.5    K- pi+ pi+  PHSP;
  0.5    K- K+ pi+  PHSP;
Enddecay
Decay D_s+
  0.5    K- pi+ pi+  PHSP;
  0.5    K- K+ pi+  PHSP;
Enddecay
Decay Lambda_c+
  1.0    p+ K- pi+ PHSP;
Enddecay
Decay Xi_c0
  1.0    p+ K- K- pi+ PHSP;
Enddecay
Decay Sigma_c0
  1.0    Lambda_c+ pi- PHSP;
Enddecay
Decay Sigma_c++
  1.0    Lambda_c+ pi+ PHSP;
Enddecay
Decay D*+
  1.0    D0 pi+ VSS;
Enddecay
Decay J/psi
  1.0   mu+ mu-  PHOTOS   VLL;
Enddecay
CDecay D_s-
CDecay anti-D0
CDecay D-
CDecay anti-Lambda_c-
CDecay anti-Xi_c0
CDecay anti-Sigma_c0
CDecay anti-Sigma_c--
CDecay D*-
End
#
