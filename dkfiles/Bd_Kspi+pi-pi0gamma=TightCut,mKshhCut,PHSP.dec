# EventType: 11104711
#
# Descriptor: [Beauty -> pi+ pi- (KS0 -> pi+ pi-) (pi0 -> gamma gamma) gamma]cc
#
# NickName: Bd_Kspi+pi-pi0gamma=TightCut,mKshhCut,PHSP 
#
# Cuts: LoKi::GenCutTool/TightCut 
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation() 
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/mKshhCut"
# evtgendecay.addTool( LoKi__GenCutTool ,'mKshhCut')
# evtgendecay.mKshhCut.Decay = '[^(Beauty => pi+ pi- KS0 pi0 gamma)]CC'
# evtgendecay.mKshhCut.Cuts  = {'[B0]cc' : ' mKshhCut '}
# evtgendecay.mKshhCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "mKshhCut   = (GMASS(CS('[(Beauty => ^pi+ pi- KS0 pi0 gamma)]CC'),CS('[(Beauty => pi+ ^pi- KS0 pi0 gamma)]CC'), CS('[(Beauty => pi+ pi- ^KS0 pi0 gamma)]CC')) < 2.0 * GeV)"]
#
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[^(Beauty => ^pi+ ^pi- (KS0 => ^pi+ ^pi-) pi0 gamma)]CC'
# tightCut.Cuts      =    {
#     '[pi-]cc'       : ' inAcc' , 
#     '[B0]cc'        : ' gammaPTCut ' } 
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "gammaPTCut = ( ( (GCHILD(GPT, CS('[(Beauty => pi+ pi- KS0 pi0 ^gamma)]CC')) > 1.5 * GeV) & (in_range ( 0.005 , GCHILD(GTHETA, CS('[(Beauty => pi+ pi- KS0 pi0 ^gamma)]CC')), 0.400 ))) | ( (GCHILD(GPT,CS('[(Beauty => pi+ pi- KS0 (pi0 => ^gamma gamma) gamma)]CC')) > 1.5 * GeV) & ( in_range ( 0.005 , GCHILD(GTHETA,CS('[(Beauty => pi+ pi- KS0 (pi0 => ^gamma gamma) gamma)]CC')) , 0.400 ) )) | ( (GCHILD(GPT,CS('[(Beauty => pi+ pi- KS0 (pi0 => gamma ^gamma) gamma)]CC')) > 1.5 * GeV) & ( in_range ( 0.005 , GCHILD(GTHETA,CS('[(Beauty => pi+ pi- KS0 (pi0 => gamma ^gamma) gamma)]CC')) , 0.400 ) ) ) )",
#     "inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) "]
#
# EndInsertPythonCode
#
# Documentation: Bkgd for Kspipigamma, all in PHSP and acc, at least one high PT photon in Acc and cut on m(Kspipi)
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20190106
#
Alias      MyK0s  K_S0
ChargeConj MyK0s  MyK0s
#
Alias      Mypi0        pi0
ChargeConj Mypi0        Mypi0
#
Decay B0sig
  1.000   pi+  pi-    MyK0s      Mypi0   gamma      PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyK0s
  1.000   pi+         pi-       PHSP;
Enddecay
#
Decay Mypi0
  1.000        gamma      gamma           PHSP;
Enddecay
#
End
