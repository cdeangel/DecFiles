# EventType: 16465003
# NickName: XibStar6450_LbK,Lcpi-MaxWidth100MeV=TightCut
# Descriptor: [Sigma_b- -> K- (Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) pi-)]cc
#
# Documentation:
#   Decay Xib**- -> Lb0 K- with Lb0 -> Lc+ pi- and Lc+ --> p K- pi+
#   Mass = 6.45 GeV, Width = 35 MeV, maxWidth 100 MeV   
# EndDocumentation
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# 
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay     = '[Sigma_b- ==> (Lambda_b0 ==> (Lambda_c+  --> ^p+ ^K- ^pi+) ^pi-) K- ]CC'
# tightCut.Cuts      =    {
#     '[K-]cc'   : ' goodKaon ' ,
#     '[p+]cc'   : ' goodProton ' ,
#     '[pi+]cc'  : ' goodpi  ' ,
#     '[pi-]cc'  : ' goodpi  ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import GeV',
#     'inAcc    = in_range ( 0.010 , GTHETA , 0.400 ) ' ,
#     'goodKaon = ( GPT > 0.09 * GeV ) & ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodProton = ( GPT > 0.09 * GeV ) & ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodpi  = ( GPT > 0.09 * GeV ) & ( GP > 1.5 * GeV ) & inAcc ' ]
#
# EndInsertPythonCode
#
#
# ParticleValue: " Sigma_b-   114   5112 -1.0  6.450  1.88000e-023       Sigma_b-   5112  100.00", " Sigma_b~+  115  -5112  1.0  6.450  1.880000e-023  anti-Sigma_b+  -5112  100.00"
#
# Email: sblusk@syr.edu
# PhysicsWG: Onia
# Tested: Yes
# CPUTime: <1min
# Responsible: Steve Blusk
# Date: 20210621
#

Alias MyLambda_b0       Lambda_b0
Alias Myanti-Lambda_b0  anti-Lambda_b0
ChargeConj MyLambda_b0  Myanti-Lambda_b0

Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
Alias      MyK*0          K*0
Alias      Myanti-K*0     anti-K*0
ChargeConj MyK*0          Myanti-K*0
#
Alias      MyLambda(1520)0       Lambda(1520)0
Alias      Myanti-Lambda(1520)0  anti-Lambda(1520)0
ChargeConj MyLambda(1520)0       Myanti-Lambda(1520)0

# Force Sb- (stand-in for Xib**-) to decay to Lb0 K-:
Decay Sigma_b-sig
1.000    MyLambda_b0         K-     PHSP;
Enddecay
CDecay anti-Sigma_b+sig

Decay MyLambda_b0
  1.000    MyLambda_c+        pi-                 PHSP;
Enddecay
CDecay Myanti-Lambda_b0

Decay MyLambda_c+
  0.02800         p+      K-      pi+          PHSP;
  0.01065         p+      Myanti-K*0           PHSP;
  0.00860         Delta++ K-                   PHSP;
  0.00414         MyLambda(1520)0 pi+          PHSP;
Enddecay
CDecay Myanti-Lambda_c-
# BR = 1
#
Decay MyK*0
  0.6657      K+  pi-                          VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyLambda(1520)0
  0.23   p+     K-                             PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
End



