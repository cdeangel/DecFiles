# EventType: 13863600
# 
# Descriptor: [B_s0 => (D_s- => K+ K- pi-) (tau+ -> pi+ pi+ pi- pi0 anti-nu_tau ) nu_tau]CC 
#
# NickName: Bs_DsststTauNu,KKPi,PiPiPiPi0=TightCut,tauolababar,pi0notreqinacc
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay     = '^[Beauty ==> ^(D_s- ==> ^K+ ^K- ^pi-) {X}{X}(tau+ ==> ^pi+ ^pi+ ^pi- pi0 nu_tau~) nu_tau]CC'
#tightCut.Preambulo += [
#'from GaudiKernel.SystemOfUnits import MeV, GeV',
#  "from LoKiCore.functions import in_range"  ,
#'inAcc = ( in_range( 0.010, GTHETA, 0.400) ) & ( in_range( 1.6, GETA, 5.6) )  ',
#'goodTrack  = ( GPT > 100*MeV ) &  ( inAcc )' ,
#"goodB = ( GPT > 1.5 *GeV ) & ( GP > 30 *GeV ) "
#]
#tightCut.Cuts      =    {
#' [B_s0]cc'   : 'goodB',
#'[D_s-]cc'   : 'GPT>1.4*GeV',
#'[pi+]cc' : 'goodTrack',
#'[K+]cc': ' goodTrack & (GPT > 1200 * MeV) ' 
#}
#EndInsertPythonCode
#
# Documentation:
# Bs decay to Ds(*,(*)) tau nu tau
# Ds decays into K+ K- and charged pion
# Tau decays in the 3 charged pions plus pi0  channel
# pi0 ia not forced into the acceptance
# EndDocumentation
#
# PhysicsWG:  B2SL
# Tested: Yes
# CPUTime: < 2 min
# Responsible: Carmen Giugliano
# Email: carmen.giugliano@cern.ch
# Date: 20211109
#
# Tauola steering options
Define TauolaCurrentOption 1
Define TauolaBR1 1.0

Alias      MyD_s+     D_s+
Alias      MyD_s-     D_s-
ChargeConj MyD_s+     MyD_s-

Alias      MyD_s*+    D_s*+
Alias      MyD_s*-    D_s*-
ChargeConj MyD_s*+    MyD_s*-
#
Alias      MyD'_s1+   D'_s1+
Alias      MyD'_s1-   D'_s1-
ChargeConj MyD'_s1-   MyD'_s1+
#
Alias      MyD_s0*+   D_s0*+
Alias      MyD_s0*-   D_s0*-
ChargeConj MyD_s0*+   MyD_s0*-

Alias         MyTau-   tau-
Alias         MyTau+   tau+
ChargeConj    MyTau-   MyTau+


#
# ---------------
# Decay of the Bs
# ---------------
Decay B_s0sig
  0.75    MyD_s0*-   MyTau+    nu_tau       PHOTOS  ISGW2;
  0.25    MyD'_s1-   MyTau+    nu_tau       PHOTOS  ISGW2;
Enddecay
CDecay anti-B_s0sig
# -----------------
# Decay of the Ds*+-
# -----------------
Decay MyD_s*+
  0.935   MyD_s+  gamma               PHOTOS VSP_PWAVE;
  0.058   MyD_s+  pi0                 PHOTOS VSS;
Enddecay
CDecay MyD_s*-
## -----------------
# Decay of the Ds0*+
# -----------------
Decay MyD_s0*+
  0.9   MyD_s+   pi0                 PHOTOS PHSP;
  0.1   MyD_s*+   gamma              PHOTOS PHSP;
Enddecay
CDecay MyD_s0*-
## -----------------
# Decay of the Ds1*+-
# -----------------
Decay MyD'_s1+
  0.5   MyD_s*+   gamma              PHOTOS PHSP;
  0.5   MyD_s+    pi+ pi-            PHOTOS PHSP;
Enddecay
CDecay MyD'_s1-
# -----------------
# Decay of the Ds+-
# -----------------
Decay MyD_s-
  1.000 K- K+ pi-                    PHOTOS D_DALITZ;
Enddecay
CDecay MyD_s+
# -----------------
# Decay of the Tau-
# -----------------
Decay MyTau-
  1.00        TAUOLA 8;
Enddecay
CDecay MyTau+
#
End
#
