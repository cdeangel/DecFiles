# EventType: 11166001
#
# Descriptor: [B0 -> (D*- -> (anti-D0 -> K+ pi- pi- pi+) pi-) pi+]cc
## See GenCutTool decay descriptor for something you might want to use with DaVinci
# NickName: Bd_Dstpi,Kpipipi=AMPGEN,TightCut
# Cuts: 'LoKi::GenCutTool/TightCut'
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# Generation().SignalRepeatedHadronization.addTool(LoKi__GenCutTool,'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = "([[B0]nos ==> (D*(2010)- ==> ([D~0]nos ==> ^K+ ^pi- ^pi- ^pi+) ^pi-) ^pi+]CC) || "\
#                  "([[B0]nos ==> (D*(2010)- ==> ([D~0]os  ==> ^K- ^pi+ ^pi+ ^pi-) ^pi-) ^pi+]CC) || "\
#                  "([[B0]os  ==> (D*(2010)+ ==> ([D0]nos  ==> ^K- ^pi+ ^pi+ ^pi-) ^pi+) ^pi-]CC) || "\
#                  "([[B0]os  ==> (D*(2010)+ ==> ([D0]os   ==> ^K+ ^pi- ^pi- ^pi+) ^pi+) ^pi-]CC)"
# tightCut.Preambulo += [
#   "from LoKiCore.functions import in_range"  ,
#   "from GaudiKernel.SystemOfUnits import GeV, MeV",
#   "inAcc = in_range(0.005,GTHETA,0.4)"
#  ]
# tightCut.Cuts = {
#   '[pi+]cc'  : "inAcc & (GPT > 100*MeV) & (GP>2*GeV)",
#   '[K+]cc'   : "inAcc & (GPT > 100*MeV) & (GP>2*GeV)",
# }
# EndInsertPythonCode
#
# Documentation: For D -> K3Pi mixing analysis
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Marian Stahl
# Email: marian.stahl@{nospam}cern.ch
# Date: 20191125
# CPUTime: < 1 min
#
Alias          MyD*+      D*+
Alias          MyD*-      D*-
ChargeConj     MyD*-      MyD*+

Alias          MyD0       D0
Alias          Myanti-D0  anti-D0
ChargeConj     MyD0       Myanti-D0

#
Decay B0sig
  1.000 MyD*- pi+ SVS;
Enddecay
CDecay anti-B0sig
#
Decay MyD*-
  1.000 Myanti-D0 pi- VSS;
Enddecay
CDecay MyD*+
#
## Disable PHOTOS for all AmpGen models
noPhotos
Decay Myanti-D0
  1.000 K+ pi- pi- pi+ LbAmpGen DtoKpipipi_v2;
Enddecay
CDecay MyD0
End
