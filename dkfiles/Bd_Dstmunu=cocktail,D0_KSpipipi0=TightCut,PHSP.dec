# EventType: 11576500
#
# Descriptor: [B0 -> (D*- -> (anti-D0 -> KS0 pi+ pi- pi0) pi-) mu+ nu_mu]cc
#
# NickName: Bd_Dstmunu=cocktail,D0_KSpipipi0=TightCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Inclusive decays of B0 into D*(2010)- with an accompanying muon; D0 forced into KS pi+ pi- pi0; tight cuts on muon and charged pions from D0;
# EndDocumentation 
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# signal = Generation().SignalRepeatedHadronization
# signal.addTool( LoKi__GenCutTool, 'TightCut' )
# tight_cut = signal.TightCut
# tight_cut.Decay = '^[Beauty --> (D*(2010)- => (D~0 => KS0 pi- pi+ pi0) pi-) mu+ ...]CC'
# tight_cut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV, GeV, mm',
#     'inAcc = in_range(0.005, GTHETA , 0.4)',
#     'mu_cuts = GINTREE(("mu-" == GABSID) & (GPT > 242 * MeV) & (GP > 0.97 * GeV) & inAcc)',
#     'pi_cuts = (3 < GNINTREE(("pi+" == GABSID) & (GPT >  97 * MeV) & (GP > 0.97 * GeV) & inAcc))',
#     'ks_cuts = GINTREE(("KS0" == GABSID) & (GPT > 242 * MeV) & (GP > 1.94 * GeV) & inAcc'
#                        ' & (GFAEVX(abs(GVZ), 0) < 2500 * mm)'
#                        ' & (GCHILD(GPT, 1) > 97 * MeV) & (GCHILD(GPT, 2) > 97 * MeV)'
#                        ' & (GCHILD(GP, 1) > 1.94 * GeV) & (GCHILD(GP, 2) > 1.94 * GeV))',
#     'inEcalX  = abs(GPX / GPZ) < 4.5 / 12.5 ',
#     'inEcalY  = abs(GPY / GPZ) < 3.5 / 12.5 ',
#     'pi0_cuts = GINTREE(("pi0" == GABSID) & (GP > 750 * MeV) & (GPT > 300 * MeV) & inEcalX & inEcalY)',
#     'd0_cuts  = GINTREE(("D0"  == GABSID) & (GPT > 485 * MeV) & (GP > 4.85 * GeV)'
#                         ' & pi_cuts & ks_cuts & pi0_cuts)',
#     'tag_pion_cuts = (GPT > 75 * MeV) & inAcc',
#     'dst_cuts = GINTREE(("D*(2010)-" == GABSID) & GCHILDCUT(tag_pion_cuts, "[D*(2010)+ =>  Charm ^pi+]CC"))',
#     'b_cuts   = (GPT > 1.45 * GeV) & (GP > 7 * GeV) & (GTIME > 0.15 * 0.45538 * mm)',
#     'all_cuts = mu_cuts & d0_cuts & dst_cuts & b_cuts'
#  ]
# tight_cut.Cuts = {
#   'Beauty': 'all_cuts'
# }
# EndInsertPythonCode
#
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min 
# Responsible: Tommaso Pajero
# Email: tommaso.pajero@cern.ch
# Date: 20210309

# -----------------------------------------------------------------------------
# ALIASES
# -----------------------------------------------------------------------------

Alias      MyD*+    D*+
Alias      MyD*-    D*-
ChargeConj MyD*+    MyD*-

Alias      Mytau+         tau+
Alias      Mytau-         tau-
ChargeConj Mytau+         Mytau-

Alias      MyD_10         D_10
Alias      MyAntiD_10     anti-D_10
ChargeConj MyD_10         MyAntiD_10

Alias      MyD_1+         D_1+
Alias      MyD_1-         D_1-
ChargeConj MyD_1+         MyD_1-

Alias      MyD_0*+         D_0*+
Alias      MyD_0*-         D_0*-
ChargeConj MyD_0*+         MyD_0*-

Alias      MyD_0*0         D_0*0
Alias      MyAntiD_0*0     anti-D_0*0
ChargeConj MyD_0*0         MyAntiD_0*0

Alias      MyD'_10         D'_10
Alias      MyAntiD'_10     anti-D'_10
ChargeConj MyD'_10         MyAntiD'_10

Alias      MyD'_1+         D'_1+
Alias      MyD'_1-         D'_1-
ChargeConj MyD'_1+         MyD'_1-

Alias      MyD_2*+         D_2*+
Alias      MyD_2*-         D_2*-
ChargeConj MyD_2*+         MyD_2*-

Alias      MyD_2*0         D_2*0
Alias      MyAntiD_2*0     anti-D_2*0
ChargeConj MyD_2*0         MyAntiD_2*0

Alias      MyD0     D0
Alias      MyantiD0 anti-D0
ChargeConj MyD0     MyantiD0

Alias      MyK_S0   K_S0
ChargeConj MyK_S0   MyK_S0

# -----------------------------------------------------------------------------
# DECAYS
# -----------------------------------------------------------------------------

# B0 (BRs in %)
#   - in HQET2 the order is rho^2 (ha1 unchanged) R1 R2;
#     HFLAG Spring 2019; normalisation factor ha1 has no impact on kinematics
Decay B0sig 
  5.05     MyD*-    mu+    nu_mu              HQET2 1.122 0.921 1.270 0.852;
  0.05640  MyD_0*-  mu+    nu_mu              ISGW2;
  0.06500  MyD'_1-  mu+    nu_mu              ISGW2;
  0.17494  MyD_1-   mu+    nu_mu              ISGW2;
  0.06198  MyD_2*-  mu+    nu_mu              ISGW2;
  0.0462   MyD*-    pi0    mu+    nu_mu       GOITY_ROBERTS;
  0.0645   MyD*-    pi0    pi0    mu+   nu_mu PHSP;
  0.2451   MyD*-    pi+    pi-    mu+   nu_mu PHSP;
  0.2604   MyD*-    Mytau+ nu_tau             ISGW2;
  0.0082   MyD_1-   Mytau+ nu_tau             ISGW2;
  0.0027   MyD_0*-  Mytau+ nu_tau             ISGW2;
  0.0056   MyD'_1-  Mytau+ nu_tau             ISGW2;
  0.0041   MyD_2*-  Mytau+ nu_tau             ISGW2;
Enddecay
CDecay anti-B0sig

# tau
Decay Mytau-
  1.    mu- nu_tau  anti-nu_mu  TAULNUNU;
Enddecay
CDecay Mytau+

# D* resonances
SetLineshapePW MyD_1+ MyD*+ pi0 2
SetLineshapePW MyD_1- MyD*- pi0 2
SetLineshapePW MyD_10 MyD*+ pi- 2
SetLineshapePW MyAntiD_10 MyD*- pi+ 2

SetLineshapePW MyD_2*+ MyD*+ pi0 2
SetLineshapePW MyD_2*- MyD*- pi0 2
SetLineshapePW MyD_2*0 MyD*+ pi- 2
SetLineshapePW MyAntiD_2*0 MyD*- pi+ 2

Decay MyD_0*+
  0.04     MyD*+ pi0 pi0                     PHSP;
  0.08     MyD*+ pi+ pi-                     PHSP;
Enddecay
CDecay MyD_0*-

Decay MyD_0*0
  0.08     MyD*+ pi- pi0                     PHSP;
Enddecay
CDecay MyAntiD_0*0

Decay MyD'_1+
  0.250    MyD*+ pi0                         VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyD'_1-

Decay MyD'_10
  0.500    MyD*+ pi-                         VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyAntiD'_10

Decay MyD_1+
  0.200     MyD*+ pi0                        VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
  0.0208    MyD_0*0 pi+                      PHSP;
  0.0156    MyD_0*+ pi0                      PHSP;
Enddecay
CDecay MyD_1-

Decay MyD_10
  0.400     MyD*+ pi-                        VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
  0.0312    MyD_0*+ pi-                      PHSP;
  0.0104    MyD_0*0 pi0                      PHSP;
Enddecay
CDecay MyAntiD_10

Decay MyD_2*+
  0.087     MyD*+ pi0                        TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
  0.0117    MyD_0*0 pi+                      PHSP;
  0.0088    MyD_0*+ pi0                      PHSP;
  0.004     MyD*+ pi0 pi0                    PHSP;
  0.008     MyD*+ pi+ pi-                    PHSP;
Enddecay
CDecay MyD_2*-

Decay MyD_2*0
  0.173       MyD*+ pi-                      TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
  0.0176      MyD_0*+ pi-                    PHSP;
  0.0059      MyD_0*0 pi0                    PHSP;
  0.008       MyD*+ pi- pi0                  PHSP;
Enddecay
CDecay MyAntiD_2*0

# signal particles
Decay MyD*+
  1.    MyD0  pi+               VSS;
Enddecay
CDecay MyD*-

Decay MyD0
  1.    MyK_S0  pi+  pi-  pi0   PHSP;
Enddecay
CDecay MyantiD0

Decay MyK_S0
  1.    pi+     pi-             PHSP;
Enddecay

End
