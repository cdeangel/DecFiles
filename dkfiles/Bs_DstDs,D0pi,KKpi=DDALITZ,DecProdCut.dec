# EventType: 13196042
#
# Descriptor: [B_s0 -> (D*(2010)- -> pi- (D~0 -> K+ pi-)) (D_s+ -> K- K+ pi+)]cc
#
#
# NickName: Bs_DstDs,D0pi,KKpi=DDALITZ,DecProdCut
#
# Cuts: DaughtersInLHCb
#
# Documentation:  Bs decay to D*Ds and Dalitz decay model for Ds decay.
# Daughters in LHCb.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Antje Moedden
# Email: antje.modden@cern.ch
# Date: 20170420
# CPUTime: < 1 min

# -------------------------
# DEFINE THE Ds+ AND Ds-
# -------------------------
Alias MyD_s+ D_s+
Alias MyD_s- D_s-
ChargeConj MyD_s+ MyD_s-

# -------------------------
# THEN DEFINE THE D*+ AND D*-
# -------------------------
Alias      MyD*+       D*+
Alias      MyD*-       D*-
ChargeConj MyD*+       MyD*-

# -------------------------
# THEN DEFINE THE D0 AND D~0
# -------------------------
Alias       MyD0        D0
Alias       Myanti-D0   anti-D0
ChargeConj  MyD0        Myanti-D0

# ---------------
# DECAY OF THE B0
# ---------------
Decay B_s0sig
        1.000         MyD*-    MyD_s+                   SVS;
Enddecay
CDecay anti-B_s0sig

# ---------------
# DECAY OF THE D*-
# ---------------
Decay MyD*-
  1.000        Myanti-D0 pi-                            VSS;
Enddecay
CDecay MyD*+

# ---------------
# DECAY OF THE D~0
# ---------------
Decay Myanti-D0
  1.000        K+        pi-                PHSP;
Enddecay
CDecay MyD0

# -----------------
# DECAY OF THE Ds-
# -----------------
Decay MyD_s-
  1.000     K+ K- pi-                           D_DALITZ;
Enddecay
CDecay MyD_s+
#
End
