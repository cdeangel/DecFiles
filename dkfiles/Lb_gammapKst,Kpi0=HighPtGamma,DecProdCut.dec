# EventType: 15102600
#
# Descriptor: [Lambda_b0 -> p+ (K*- -> K- pi0) gamma]cc
#
# NickName: Lb_gammapKst,Kpi0=HighPtGamma,DecProdCut
#
# Cuts: DaughtersInLHCb
#
# FullEventCuts: LoKi::FullGenEventCut/BRadiativeCut
#
# InsertPythonCode:
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool( LoKi__FullGenEventCut, "BRadiativeCut" )
# radCut = Generation().BRadiativeCut
# radCut.Code = " ( count ( isGoodB ) > 0 ) "
# radCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import  GeV, mrad"
#   , "NGoodGamma = GINTREE(('gamma' == GABSID) & (GPT >1.5*GeV))"
#   , "isGoodB    = (GBEAUTY & NGoodGamma & GDECTREE('[Lambda_b0 => p+ K*(892)- gamma]CC'))"
#    ]
# EndInsertPythonCode
#
#
# Documentation: Lambda_b0 forced into p K*- gamma, K*- forced into K-pi0, decay products in acceptance, decay with high gamma PT > 1.5 GeV 
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Carla Marin
# Email: carla.marin.benito@cern.ch
# Date:   20200407
# CPUTime: 1.5 min
#

Alias      MyK*-       K*-
Alias      MyK*+       K*+
ChargeConj MyK*-       MyK*+

Decay Lambda_b0sig
  1.000    p+          MyK*-        gamma   PHSP;
Enddecay
CDecay anti-Lambda_b0sig

Decay MyK*-
  1.000    K-          pi0          VSS;
Enddecay
CDecay MyK*+

#
End
#
