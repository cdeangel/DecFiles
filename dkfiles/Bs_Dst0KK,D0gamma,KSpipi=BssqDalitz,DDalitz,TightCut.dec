# EventType: 13166334
#
# Descriptor: {[[B_s0]nos => (D*(2007)~0 -> (D~0 -> (KS0 -> pi+ pi-) pi+ pi-) gamma) K+ K-]cc, [[B_s0]os => (D*(2007)0 -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) gamma) K- K+]cc}
#
# NickName: Bs_Dst0KK,D0gamma,KSpipi=BssqDalitz,DDalitz,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '^[Beauty => (D*(2007)~0 -> ^(D~0 => ^(KS0 => ^pi+ ^pi-) ^pi+ ^pi-) gamma) ^K+ ^K-]CC'
#tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = in_range(0.005, GTHETA, 0.400) & in_range(1.8, GETA, 5.2)',
#    'inY          = in_range(1.8, GY, 4.8)',
#    'goodH        = (GP > 1000 * MeV) & (GPT > 98 * MeV) & inAcc',
#    'goodB0       = (GP > 25000 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.05 * millimeter) & inY',
#    'goodD0       = (GP > 10000 * MeV) & (GPT > 500 * MeV) & inY',
#    'goodKS       = (GP > 4000 * MeV) & (GPT > 250 * MeV)',
#    'goodBDaugK   = (GNINTREE( ("K+" == GABSID) & (GP > 2000 * MeV), 1) > 1.5)',
#    'goodKsDaugPi = (GNINTREE( ("pi+" == GABSID) & (GP > 1750 * MeV), 1) > 1.5)'
#]
#tightCut.Cuts = {
#    '[pi+]cc'         : 'goodH',
#    '[K+]cc'          : 'goodH',
#    'Beauty'          : 'goodB0 & goodBDaugK', 
#    '[D0]cc'          : 'goodD0',
#    '[KS0]cc'         : 'goodKS & goodKsDaugPi'
#    }
#EndInsertPythonCode 
#
# Documentation: B_s0 decay with flat square Dalitz model, Dst0 forced to D0 gamma, D0 decay with KSpipi model, tight cuts 
# EndDocumentation
#
# CPUTime: <1min
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Yuya Shimizu 
# Email: yuya.shimizu@cern.ch 
# Date: 20201202
#

Alias MyD*0       D*0
Alias Myanti-D*0  anti-D*0
Alias MyD0        D0
Alias Myanti-D0   anti-D0
Alias MyKS        K_S0
ChargeConj MyD*0  Myanti-D*0
ChargeConj MyD0   Myanti-D0
ChargeConj MyKS   MyKS

Decay B_s0sig
  1.0   Myanti-D*0  K+         K-   FLATSQDALITZ;
Enddecay
CDecay anti-B_s0sig

Decay MyD*0
  1.0   MyD0        gamma           VSP_PWAVE;
Enddecay
CDecay Myanti-D*0

Decay MyD0
  1.0   MyKS        pi+        pi-  D_DALITZ;
Enddecay
CDecay Myanti-D0

Decay MyKS
  1.0   pi+         pi-             PHSP;
Enddecay

End
