# SUSY Les Houches Accord 2.beta - MSSM spectrum + Decays
# SPheno v3beta18
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101
# in case of problems send email to porod@ific.uv.es
# Created: 28.01.2009,  11:30
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v3beta18       # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    4    # MSSM with explicit R-parity violation
Block MINPAR  # Input parameters
    3    1.00000000E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block SMINPUTS  # SM parameters
         1     1.27934000E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16639000E-05  # G_mu [GeV^-2]
         3     1.17200000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.25000000E+00  # m_b(m_b), MSbar
         6     1.72700000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
Block EXTPAR  # 
   0    8.38116744E+02  # scale Q where the parameters below are defined
   1    1.66841512E+02  # M_1
   2    3.10394789E+02  # M_2
   3    8.98775913E+02  # M_3
  11   -7.43482585E+02  # A_t
  12   -1.14460073E+03  # A_b
  13   -3.37154389E+02  # A_l
  23    5.33465504E+02  # mu 
  31    6.53266211E+02  # M_(L,11)
  32    6.53256801E+02  # M_(L,22)
  33    6.50600222E+02  # M_(L,33)
  34    6.16512770E+02  # M_(E,11)
  35    6.16492682E+02  # M_(E,22)
  36    6.10806391E+02  # M_(E,33)
  41    1.00480334E+03  # M_(Q,11)
  42    1.00479959E+03  # M_(Q,22)
  43    8.87774342E+02  # M_(Q,33)
  44    9.81744043E+02  # M_(U,11)
  45    9.81740119E+02  # M_(U,22)
  46    7.25147096E+02  # M_(U,33)
  47    9.79032463E+02  # M_(D,11)
  48    9.79028630E+02  # M_(D,22)
  49    9.72370624E+02  # M_(D,33)
Block gauge Q=  8.38116744E+02  # (SUSY scale)
   1    3.61975577E-01  # g'(Q)^DRbar
   2    6.42996519E-01  # g(Q)^DRbar
   3    1.06667362E+00  # g3(Q)^DRbar
Block Yu Q=  8.38116744E+02  # (SUSY scale)
  1  1     8.66571196E-06   # Y_u(Q)^DRbar
  2  2     3.46628477E-03   # Y_c(Q)^DRbar
  3  3     8.66888099E-01   # Y_t(Q)^DRbar
Block Yd Q=  8.38116744E+02  # (SUSY scale)
  1  1     1.89450273E-04   # Y_d(Q)^DRbar
  2  2     3.24771851E-03   # Y_s(Q)^DRbar
  3  3     1.37234218E-01   # Y_b(Q)^DRbar
Block Ye Q=  8.38116744E+02  # (SUSY scale)
  1  1     2.86218747E-05   # Y_e(Q)^DRbar
  2  2     5.91807517E-03   # Y_mu(Q)^DRbar
  3  3     9.95404561E-02   # Y_tau(Q)^DRbar
Block Au Q=  8.38116744E+02  # (SUSY scale)
  1  1    -9.89778470E+02   # A_u(Q)^DRbar
  2  2    -9.89773368E+02   # A_c(Q)^DRbar
  3  3    -7.43482585E+02   # A_t(Q)^DRbar
Block Ad Q=  8.38116744E+02  # (SUSY scale)
  1  1    -1.23121896E+03   # A_d(Q)^DRbar
  2  2    -1.23121415E+03   # A_s(Q)^DRbar
  3  3    -1.14460073E+03   # A_b(Q)^DRbar
Block Ae Q=  8.38116744E+02  # (SUSY scale)
  1  1    -3.39289318E+02   # A_e(Q)^DRbar
  2  2    -3.39281771E+02   # A_mu(Q)^DRbar
  3  3    -3.37154389E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  8.38116744E+02  # soft SUSY breaking masses at Q
   1    1.66841512E+02  # M_1
   2    3.10394789E+02  # M_2
   3    8.98775913E+02  # M_3
  31    6.53266211E+02  # M_(L,11)
  32    6.53256801E+02  # M_(L,22)
  33    6.50600222E+02  # M_(L,33)
  34    6.16512770E+02  # M_(E,11)
  35    6.16492682E+02  # M_(E,22)
  36    6.10806391E+02  # M_(E,33)
  41    1.00480334E+03  # M_(Q,11)
  42    1.00479959E+03  # M_(Q,22)
  43    8.87774342E+02  # M_(Q,33)
  44    9.81744043E+02  # M_(U,11)
  45    9.81740119E+02  # M_(U,22)
  46    7.25147096E+02  # M_(U,33)
  47    9.79032463E+02  # M_(D,11)
  48    9.79028630E+02  # M_(D,22)
  49    9.72370624E+02  # M_(D,33)
Block RVKAPPA Q=  8.38116744E+02  # bilinear RP parameters at Q
         1     1.22585356E-01  # epsilon_1
         2    -1.22585356E-01  # epsilon_2
         3     1.22585356E-01  # epsilon_3
Block RVSNVEV Q=  8.38116744E+02  # sneutrino vevs at Q
         1    -5.53010957E-03  # v_L_1
         2     5.75388370E-03  # v_L_2
         3    -5.32550268E-03  # v_L_3
Block SPhenoRP  # additional RP parameters
         4     5.11253178E-03  # Lambda_1 = v_d epsilon_1 + mu v_L1
         5     1.14263248E-01  # Lambda_2 = v_d epsilon_2 + mu v_L2
         6     1.14263248E-01  # Lambda_3 = v_d epsilon_3 + mu v_L3
         7     2.26077095E-03  # m^2_atm [eV^2]
         8     7.79716542E-05  # m^2_sol [eV^2]
         9     9.27161364E-01  # tan^2 theta_atm
        10     5.32360480E-01  # tan^2 theta_sol
        11     1.93525452E-03  # U_e3^2
        15     2.41075714E+01  # v_d
        16     2.41075714E+02  # v_u
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         4     1.20000000E+00  # m_c(m_c), MSbar
         5     4.25000000E+00  # m_b(m_b), MSbar
         6     1.72700000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.03724262E+01  # W+
        25     1.14142275E+02  # leightest neutral scalar
   1000016     6.48603533E+02  # 2nd neutral scalar
   1000014     6.51365125E+02  # 3rd neutral scalar
   1000012     6.51374906E+02  # 4th neutral scalar
        35     8.33702956E+02  # 5th neutral scalar
   2000016     6.48603533E+02  # leightest pseudoscalar
   2000014     6.51365125E+02  # 2nd pseudoscalar
   2000012     6.51374906E+02  # 3rd pseudoscalar
        36     8.33760854E+02  # 4th pseudoscalar
   1000015     6.11733505E+02  # leightest charged scalar
   2000013     6.19090397E+02  # 2nd charged scalar
   2000011     6.19116976E+02  # 3rd charged scalar
   2000015     6.55146557E+02  # 4th charged scalar
   1000013     6.56475762E+02  # 5th charged scalar
   1000011     6.56479897E+02  # 6th charged scalar
        37     8.37549673E+02  # 7th charged scalar
   1000001     1.03893620E+03  # ~d_L
   2000001     1.01098826E+03  # ~d_R
   1000002     1.03610121E+03  # ~u_L
   2000002     1.01269344E+03  # ~u_R
   1000003     1.03893351E+03  # ~s_L
   2000003     1.01098316E+03  # ~s_R
   1000004     1.03610207E+03  # ~c_L
   2000004     1.01268495E+03  # ~c_R
   1000005     9.16868236E+02  # ~b_1
   2000005     1.00484825E+03  # ~b_2
   1000006     7.35852848E+02  # ~t_1
   2000006     9.54592451E+02  # ~t_2
   1000021     9.57118890E+02  # ~g
        12     4.74876041E-14  # nu_1
        14     8.83028365E-12  # nu_2
        16     4.83605713E-11  # nu_3
   1000022     1.63457719E+02  # chi_10
   1000023     3.08975821E+02  # chi_20
   1000025     5.40018837E+02  # chi_30
   1000035     5.54787128E+02  # chi_40
       -11     5.10999060E-04  # e+
       -13     1.05658000E-01  # mu+
       -15     1.77700000E+00  # tau+
   1000024     3.08968060E+02  # chi_1+
   1000037     5.55227467E+02  # chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.05869775E-01   # alpha
Block Hmix Q=  8.38116744E+02  # Higgs mixing parameters
   1    5.33465504E+02  # mu
   2    9.66890012E+00  # tan[beta](Q)
   3    2.44264289E+02  # v(Q)
   4    7.09698893E+05  # m^2_A(Q)
Block staumix  # stau mixing matrix
   1  1     1.84195583E-01   # R_sta(1,1)
   1  2     9.82889611E-01   # R_sta(1,2)
   2  1    -9.82889611E-01   # R_sta(2,1)
   2  2     1.84195583E-01   # R_sta(2,2)
Block stopmix  # stop mixing matrix
   1  1     3.64986818E-01   # R_st(1,1)
   1  2     9.31012687E-01   # R_st(1,2)
   2  1    -9.31012687E-01   # R_st(2,1)
   2  2     3.64986818E-01   # R_st(2,2)
Block sbotmix  # sbottom mixing matrix
   1  1     9.95981489E-01   # R_sb(1,1)
   1  2     8.95593256E-02   # R_sb(1,2)
   2  1    -8.95593256E-02   # R_sb(2,1)
   2  2     9.95981489E-01   # R_sb(2,2)
Block Nmix  # neutralino mixing matrix
   1  1    -9.94143582E-01   # N(1,1)  
   1  2     2.44504632E-02   # N(1,2)  
   1  3    -9.78110935E-02   # N(1,3)  
   1  4     3.89063511E-02   # N(1,4)  
   2  1    -5.04833065E-02   # N(2,1)  
   2  2    -9.65047841E-01   # N(2,2)  
   2  3     2.16933169E-01   # N(2,3)  
   2  4    -1.38109016E-01   # N(2,4)  
   3  1     3.98816772E-02   # N(3,1)  
   3  2    -5.84310858E-02   # N(3,2)  
   3  3    -7.01886724E-01   # N(3,3)  
   3  4    -7.08766737E-01   # N(3,4)  
   4  1     8.68298734E-02   # N(4,1)  
   4  2    -2.54304242E-01   # N(4,2)  
   4  3    -6.71362806E-01   # N(4,3)  
   4  4     6.90696684E-01   # N(4,4)  
Block Umix  # chargino U mixing matrix
   1  1    -9.49992334E-01   # U(1,1)
   1  2     3.12273222E-01   # U(1,2)
   2  1     3.12273222E-01   # U(2,1)
   2  2     9.49992334E-01   # U(2,2)
Block Vmix  # chargino V mixing matrix
   1  1    -9.79833869E-01   # V(1,1)
   1  2     1.99813884E-01   # V(1,2)
   2  1     1.99813884E-01   # V(2,1)
   2  2     9.79833869E-01   # V(2,2)
DECAY   2000001     1.20943827E+00   # ~d_R
#    BR                NDA      ID1      ID2
     4.54104365E-01    2     1000022         1   # BR(~d_R -> chi_10 d)
     8.25261366E-04    2     1000023         1   # BR(~d_R -> chi_20 d)
     3.93201422E-04    2     1000025         1   # BR(~d_R -> chi_30 d)
     1.77412229E-03    2     1000035         1   # BR(~d_R -> chi_40 d)
     5.42901790E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     1.18790016E+01   # ~d_L
#    BR                NDA      ID1      ID2
     1.45501342E-02    2     1000022         1   # BR(~d_L -> chi_10 d)
     2.73411106E-01    2     1000023         1   # BR(~d_L -> chi_20 d)
     8.26171779E-04    2     1000025         1   # BR(~d_L -> chi_30 d)
     1.35232111E-02    2     1000035         1   # BR(~d_L -> chi_40 d)
     5.43531259E-01    2    -1000024         2   # BR(~d_L -> chi_1- u)
     3.33069639E-02    2    -1000037         2   # BR(~d_L -> chi_2- u)
     1.20851154E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     1.21005144E+00   # ~s_R
#    BR                NDA      ID1      ID2
     4.53876543E-01    2     1000022         3   # BR(~s_R -> chi_10 s)
     9.97179591E-04    2     1000023         3   # BR(~s_R -> chi_20 s)
     4.27924281E-04    2     1000025         3   # BR(~s_R -> chi_30 s)
     1.78491649E-03    2     1000035         3   # BR(~s_R -> chi_40 s)
     3.43103097E-04    2    -1000024         4   # BR(~s_R -> chi_1- c)
     5.42543393E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     1.18785718E+01   # ~s_L
#    BR                NDA      ID1      ID2
     1.45504248E-02    2     1000022         3   # BR(~s_L -> chi_10 s)
     2.73403146E-01    2     1000023         3   # BR(~s_L -> chi_20 s)
     8.31956106E-04    2     1000025         3   # BR(~s_L -> chi_30 s)
     1.35308424E-02    2     1000035         3   # BR(~s_L -> chi_40 s)
     5.43512165E-01    2    -1000024         4   # BR(~s_L -> chi_1- c)
     3.33256323E-02    2    -1000037         4   # BR(~s_L -> chi_2- c)
     1.20845834E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.53483128E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.08530354E-02    2     1000022         5   # BR(~b_1 -> chi_10 b)
     1.78048028E-01    2     1000023         5   # BR(~b_1 -> chi_20 b)
     4.81444869E-03    2     1000025         5   # BR(~b_1 -> chi_30 b)
     1.02603510E-02    2     1000035         5   # BR(~b_1 -> chi_40 b)
     3.18349870E-01    2    -1000024         6   # BR(~b_1 -> chi_1- t)
     3.41172552E-01    2    -1000037         6   # BR(~b_1 -> chi_2- t)
     1.36501712E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W-)
DECAY   2000005     1.61023085E+00   # ~b_2
#    BR                NDA      ID1      ID2
     3.31592162E-01    2     1000022         5   # BR(~b_2 -> chi_10 b)
     1.95643949E-03    2     1000023         5   # BR(~b_2 -> chi_20 b)
     6.24178385E-02    2     1000025         5   # BR(~b_2 -> chi_30 b)
     6.67561908E-02    2     1000035         5   # BR(~b_2 -> chi_40 b)
     3.35112261E-03    2    -1000024         6   # BR(~b_2 -> chi_1- t)
     1.79257980E-01    2    -1000037         6   # BR(~b_2 -> chi_2- t)
     3.17635252E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     3.70329437E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W-)
DECAY   2000002     2.91199217E+00   # ~u_R
#    BR                NDA      ID1      ID2
     7.55821925E-01    2     1000022         2   # BR(~u_R -> chi_10 u)
     1.37330967E-03    2     1000023         2   # BR(~u_R -> chi_20 u)
     6.55895666E-04    2     1000025         2   # BR(~u_R -> chi_30 u)
     2.96085775E-03    2     1000035         2   # BR(~u_R -> chi_40 u)
     2.39188008E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     1.19159663E+01   # ~u_L
#    BR                NDA      ID1      ID2
     9.47907546E-03    2     1000022         2   # BR(~u_L -> chi_10 u)
     2.81316932E-01    2     1000023         2   # BR(~u_L -> chi_20 u)
     4.87717306E-04    2     1000025         2   # BR(~u_L -> chi_30 u)
     1.03697524E-02    2     1000035         2   # BR(~u_L -> chi_40 u)
     5.72567266E-01    2     1000024         1   # BR(~u_L -> chi_1+ d)
     1.29046034E-02    2     1000037         1   # BR(~u_L -> chi_2+ d)
     1.12874654E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     2.91397893E+00   # ~c_R
#    BR                NDA      ID1      ID2
     7.55162897E-01    2     1000022         4   # BR(~c_R -> chi_10 c)
     1.62883928E-03    2     1000023         4   # BR(~c_R -> chi_20 c)
     6.86516214E-04    2     1000025         4   # BR(~c_R -> chi_30 c)
     2.95425518E-03    2     1000035         4   # BR(~c_R -> chi_40 c)
     5.19410507E-04    2     1000024         3   # BR(~c_R -> chi_1+ s)
     2.39038355E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     1.19137435E+01   # ~c_L
#    BR                NDA      ID1      ID2
     9.51458923E-03    2     1000022         4   # BR(~c_L -> chi_10 c)
     2.81304288E-01    2     1000023         4   # BR(~c_L -> chi_20 c)
     4.91111699E-04    2     1000025         4   # BR(~c_L -> chi_30 c)
     1.03839278E-02    2     1000035         4   # BR(~c_L -> chi_40 c)
     5.72545408E-01    2     1000024         3   # BR(~c_L -> chi_1+ s)
     1.29232151E-02    2     1000037         3   # BR(~c_L -> chi_2+ s)
     1.12837459E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     5.71578143E+00   # ~t_1
#    BR                NDA      ID1      ID2
     2.19050052E-01    2     1000022         6   # BR(~t_1 -> chi_10 t)
     1.04775502E-01    2     1000023         6   # BR(~t_1 -> chi_20 t)
     1.40507495E-01    2     1000025         6   # BR(~t_1 -> chi_30 t)
     1.46991110E-02    2     1000035         6   # BR(~t_1 -> chi_40 t)
     2.54586537E-01    2     1000024         5   # BR(~t_1 -> chi_1+ b)
     2.66381301E-01    2     1000037         5   # BR(~t_1 -> chi_2+ b)
DECAY   2000006     1.66927882E+01   # ~t_2
#    BR                NDA      ID1      ID2
     1.44791441E-02    2     1000022         6   # BR(~t_2 -> chi_10 t)
     1.15397227E-01    2     1000023         6   # BR(~t_2 -> chi_20 t)
     1.31119383E-01    2     1000025         6   # BR(~t_2 -> chi_30 t)
     2.32068583E-01    2     1000035         6   # BR(~t_2 -> chi_40 t)
     2.55692499E-01    2     1000024         5   # BR(~t_2 -> chi_1+ b)
     9.71448783E-02    2     1000037         5   # BR(~t_2 -> chi_2+ b)
     9.78997040E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     5.61985777E-02    2     1000006        25   # BR(~t_2 -> ~t_1 S0_1)
DECAY   1000024     9.58032973E-03   # chi_1+
#    BR                NDA      ID1      ID2
     9.99990789E-01    2     1000022        24   # BR(chi_1+ -> chi_10 W+)
DECAY   1000037     2.89824353E+00   # chi_2+
#    BR                NDA      ID1      ID2
     1.11690086E-01    2     1000022        24   # BR(chi_2+ -> chi_10 W+)
     3.01825432E-01    2     1000023        24   # BR(chi_2+ -> chi_20 W+)
     3.06706683E-01    2     1000024        23   # BR(chi_2+ -> chi_1+ Z)
     2.79250809E-01    2     1000024        25   # BR(chi_2+ -> chi_1+ S0_1)
#    BR                NDA      ID1      ID2       ID3
     3.73162056E-04    3     1000022        -5         6   # BR(chi_2+ -> chi_10 b t)
     1.41814000E-04    3     1000023        -5         6   # BR(chi_2+ -> chi_20 b t)
DECAY   1000022     4.36837756E-13   # chi_10
#    BR                NDA      ID1      ID2
     7.20758272E-04    2         -11       -24   # BR(chi_10 -> e+ W-)
     7.20758272E-04    2          11        24   # BR(chi_10 -> e- W+)
     1.32119654E-01    2         -13       -24   # BR(chi_10 -> mu+ W-)
     1.32119654E-01    2          13        24   # BR(chi_10 -> mu- W+)
     1.44892120E-01    2         -15       -24   # BR(chi_10 -> tau+ W-)
     1.44892120E-01    2          15        24   # BR(chi_10 -> tau- W+)
     2.80557384E-01    2          16        23   # BR(chi_10 -> nu_3 Z)
     4.23102576E-04    2          14        25   # BR(chi_10 -> nu_2 S0_1)
     1.25294759E-01    2          16        25   # BR(chi_10 -> nu_3 S0_1)
#    BR                NDA      ID1      ID2       ID3
     1.46249511E-02    3          14        -5         5   # BR(chi_10 -> nu_2 b b)
     2.94146748E-03    3          12        15       -11   # BR(chi_10 -> nu_1 tau+ e-)
     2.94146748E-03    3          12       -15        11   # BR(chi_10 -> nu_1 e+ tau-)
     2.94952539E-03    3          12        15       -13   # BR(chi_10 -> nu_1 tau+ mu-)
     2.94952539E-03    3          12       -15        13   # BR(chi_10 -> nu_1 mu+ tau-)
     1.17292448E-02    3          12       -15        15   # BR(chi_10 -> nu_1 tau+ tau-)
DECAY   1000023     1.26917072E-02   # chi_20
#    BR                NDA      ID1      ID2
     9.19942993E-02    2     1000022        23   # BR(chi_20 -> chi_10 Z)
     9.07750742E-01    2     1000022        25   # BR(chi_20 -> chi_10 S0_1)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.84523901E+00   # chi_30
#    BR                NDA      ID1      ID2
     2.90525403E-01    2     1000024       -24   # BR(chi_30 -> chi_1+ W-)
     2.90525403E-01    2    -1000024        24   # BR(chi_30 -> chi_1- W+)
     1.13611585E-01    2     1000022        23   # BR(chi_30 -> chi_10 Z)
     2.73365173E-01    2     1000023        23   # BR(chi_30 -> chi_20 Z)
     2.02435934E-02    2     1000022        25   # BR(chi_30 -> chi_10 S0_1)
     1.15649607E-02    2     1000023        25   # BR(chi_30 -> chi_20 S0_1)
DECAY   1000035     3.30478681E+00   # chi_40
#    BR                NDA      ID1      ID2
     3.29417043E-01    2     1000024       -24   # BR(chi_40 -> chi_1+ W-)
     3.29417043E-01    2    -1000024        24   # BR(chi_40 -> chi_1- W+)
     2.00164811E-02    2     1000022        23   # BR(chi_40 -> chi_10 Z)
     1.62243322E-02    2     1000023        23   # BR(chi_40 -> chi_20 Z)
     8.71344016E-02    2     1000022        25   # BR(chi_40 -> chi_10 S0_1)
     2.17521413E-01    2     1000023        25   # BR(chi_40 -> chi_20 S0_1)
#    BR                NDA      ID1      ID2       ID3
     1.09445245E-04    3     1000024         5        -6   # BR(chi_40 -> chi_1+ b t)
     1.09445245E-04    3    -1000024        -5         6   # BR(chi_40 -> chi_1- b t)
DECAY   1000021     1.29671175E+00   # ~g
#    BR                NDA      ID1      ID2
     4.16834392E-01    2     1000006        -6   # BR(~g -> ~t_1 t^*)
     4.16834392E-01    2    -1000006         6   # BR(~g -> ~t_1 t)
     5.52501817E-02    2     1000005        -5   # BR(~g -> ~b_1 b^*)
     5.52501817E-02    2    -1000005         5   # BR(~g -> ~b_1 b)
     2.73320347E-03    2     1000006        -4   # BR(~g -> ~t_1 c^*)
     2.73320347E-03    2    -1000006         4   # BR(~g -> ~t_1 c)
     1.63663585E-04    2     1000023        21   # BR(~g -> chi_20 gluon)
     3.73943914E-04    2     1000025        21   # BR(~g -> chi_30 gluon)
     3.96931283E-04    2     1000035        21   # BR(~g -> chi_40 gluon)
#    BR                NDA      ID1      ID2       ID3
     2.89939433E-03    3     1000022        -2         2   # BR(~g -> chi_10 u u)
     2.89917759E-03    3     1000022        -4         4   # BR(~g -> chi_10 c c)
     1.81631147E-04    3     1000022        -6         6   # BR(~g -> chi_10 t t)
     8.77410057E-04    3     1000022        -1         1   # BR(~g -> chi_10 d d)
     8.77454497E-04    3     1000022        -3         3   # BR(~g -> chi_10 s s)
     7.35105677E-04    3     1000022        -5         5   # BR(~g -> chi_10 b b)
     2.87763057E-03    3     1000023        -2         2   # BR(~g -> chi_20 u u)
     2.87737323E-03    3     1000023        -4         4   # BR(~g -> chi_20 c c)
     1.35133625E-03    3     1000023        -6         6   # BR(~g -> chi_20 t t)
     2.70886324E-03    3     1000023        -1         1   # BR(~g -> chi_20 d d)
     2.70899319E-03    3     1000023        -3         3   # BR(~g -> chi_20 s s)
     4.61227593E-04    3     1000035        -6         6   # BR(~g -> chi_40 t t)
     5.61462812E-03    3     1000024         1        -2   # BR(~g -> chi_1+ d u)
     5.61462812E-03    3    -1000024        -1         2   # BR(~g -> chi_1- d u)
     5.61450091E-03    3     1000024         3        -4   # BR(~g -> chi_1+ s c)
     5.61450091E-03    3    -1000024        -3         4   # BR(~g -> chi_1- s c)
     2.00297800E-03    3     1000024         5        -6   # BR(~g -> chi_1+ b t)
     2.00297800E-03    3    -1000024        -5         6   # BR(~g -> chi_1- b t)
     1.06409549E-04    3     1000037         1        -2   # BR(~g -> chi_2+ d u)
     1.06409549E-04    3    -1000037        -1         2   # BR(~g -> chi_2- d u)
     1.06540148E-04    3     1000037         3        -4   # BR(~g -> chi_2+ s c)
     1.06540148E-04    3    -1000037        -3         4   # BR(~g -> chi_2- s c)
     3.09934871E-04    3     1000037         5        -6   # BR(~g -> chi_2+ b t)
     3.09934871E-04    3    -1000037        -5         6   # BR(~g -> chi_2- b t)
DECAY        25     1.83772192E-03   # S0_1      
#    BR                NDA      ID1      ID2
     4.58012559E-04    2          -3         3   # BR(S0_1 -> s s)
     8.14780425E-01    2          -5         5   # BR(S0_1 -> b b)
     4.94024202E-02    2          -4         4   # BR(S0_1 -> c c)
     4.52188639E-04    2          13       -13   # BR(S0_1 -> mu- mu+)
     1.27740409E-01    2          15       -15   # BR(S0_1 -> tau- tau+)
# writing decays into V V* as 3-body decays
#    BR                NDA      ID1      ID2       ID3
     1.43293329E-03    3          23        12       -12   # BR(S0_1 -> Z0 nu_i nu_i)
     2.50763326E-04    3          23        11       -11   # BR(S0_1 -> Z0 e- e+)
     2.50763326E-04    3          23        13       -13   # BR(S0_1 -> Z0 mu- mu+)
     2.14939994E-04    3          23        15       -15   # BR(S0_1 -> Z0 tau- tau+)
     1.00305330E-03    3          23         1        -1   # BR(S0_1 -> Z0 d d)
     1.00305330E-03    3          23         3        -3   # BR(S0_1 -> Z0 s s)
     1.00305330E-03    3          23         5        -5   # BR(S0_1 -> Z0 b b)
     1.00305330E-03    3          23         2        -2   # BR(S0_1 -> Z0 u u)
     1.00305330E-03    3          23         4        -4   # BR(S0_1 -> Z0 c c)
DECAY   1000016     5.31477381E+00   # S0_2      
#    BR                NDA      ID1      ID2
     2.78905614E-02    2          12   1000022   # BR(S0_2 -> nu_1 chi_10)
     4.99522351E-02    2          12   1000023   # BR(S0_2 -> nu_1 chi_20)
     6.29411745E-04    2          12   1000035   # BR(S0_2 -> nu_1 chi_40)
     4.33560717E-02    2          14   1000022   # BR(S0_2 -> nu_2 chi_10)
     7.76510968E-02    2          14   1000023   # BR(S0_2 -> nu_2 chi_20)
     9.78424935E-04    2          14   1000035   # BR(S0_2 -> nu_2 chi_40)
     7.65354538E-02    2          16   1000022   # BR(S0_2 -> nu_3 chi_10)
     1.37075655E-01    2          16   1000023   # BR(S0_2 -> nu_3 chi_20)
     1.59052596E-04    2          16   1000025   # BR(S0_2 -> nu_3 chi_30)
     1.72719053E-03    2          16   1000035   # BR(S0_2 -> nu_3 chi_40)
     2.89860494E-01    2          15   1000024   # BR(S0_2 -> tau- chi_1+)
     2.89860494E-01    2         -15  -1000024   # BR(S0_2 -> tau+ chi_1-)
     2.08789370E-03    2          15   1000037   # BR(S0_2 -> tau- chi_2+)
     2.08789370E-03    2         -15  -1000037   # BR(S0_2 -> tau+ chi_2-)
DECAY   1000014     5.34768085E+00   # S0_3      
#    BR                NDA      ID1      ID2
     2.36262775E-02    2          12   1000022   # BR(S0_3 -> nu_1 chi_10)
     4.24765656E-02    2          12   1000023   # BR(S0_3 -> nu_1 chi_20)
     5.57421394E-04    2          12   1000035   # BR(S0_3 -> nu_1 chi_40)
     5.31348664E-02    2          14   1000022   # BR(S0_3 -> nu_2 chi_10)
     9.55286603E-02    2          14   1000023   # BR(S0_3 -> nu_2 chi_20)
     1.14553557E-04    2          14   1000025   # BR(S0_3 -> nu_2 chi_30)
     1.25362581E-03    2          14   1000035   # BR(S0_3 -> nu_2 chi_40)
     7.09062407E-02    2          16   1000022   # BR(S0_3 -> nu_3 chi_10)
     1.27478973E-01    2          16   1000023   # BR(S0_3 -> nu_3 chi_20)
     1.52866896E-04    2          16   1000025   # BR(S0_3 -> nu_3 chi_30)
     1.67291083E-03    2          16   1000035   # BR(S0_3 -> nu_3 chi_40)
     2.90190388E-01    2          13   1000024   # BR(S0_3 -> mu- chi_1+)
     2.90190388E-01    2         -13  -1000024   # BR(S0_3 -> mu+ chi_1-)
     1.33265747E-03    2          13   1000037   # BR(S0_3 -> mu- chi_2+)
     1.33265747E-03    2         -13  -1000037   # BR(S0_3 -> mu+ chi_2-)
DECAY   1000012     5.34779553E+00   # S0_4      
#    BR                NDA      ID1      ID2
     9.61719634E-02    2          12   1000022   # BR(S0_4 -> nu_1 chi_10)
     1.72905336E-01    2          12   1000023   # BR(S0_4 -> nu_1 chi_20)
     2.07363857E-04    2          12   1000025   # BR(S0_4 -> nu_1 chi_30)
     2.26936309E-03    2          12   1000035   # BR(S0_4 -> nu_1 chi_40)
     5.12099753E-02    2          14   1000022   # BR(S0_4 -> nu_2 chi_10)
     9.20692236E-02    2          14   1000023   # BR(S0_4 -> nu_2 chi_20)
     1.10417814E-04    2          14   1000025   # BR(S0_4 -> nu_2 chi_30)
     1.20839820E-03    2          14   1000035   # BR(S0_4 -> nu_2 chi_40)
     2.85093074E-04    2          16   1000022   # BR(S0_4 -> nu_3 chi_10)
     5.12562208E-04    2          16   1000023   # BR(S0_4 -> nu_3 chi_20)
     2.90191622E-01    2          11   1000024   # BR(S0_4 -> e- chi_1+)
     2.90191622E-01    2         -11  -1000024   # BR(S0_4 -> e+ chi_1-)
     1.32985271E-03    2          11   1000037   # BR(S0_4 -> e- chi_2+)
     1.32985271E-03    2         -11  -1000037   # BR(S0_4 -> e+ chi_2-)
DECAY        35     2.21360666E+00   # S0_5      
#    BR                NDA      ID1      ID2
     2.68029922E-04    2          -3         3   # BR(S0_5 -> s s)
     4.78574527E-01    2          -5         5   # BR(S0_5 -> b b)
     1.67687196E-01    2          -6         6   # BR(S0_5 -> t t)
     6.87927945E-03    2     1000022   1000022   # BR(S0_5 -> chi_10 chi_10)
     3.54557617E-02    2     1000022   1000023   # BR(S0_5 -> chi_10 chi_20)
     1.13820348E-01    2     1000022   1000025   # BR(S0_5 -> chi_10 chi_30)
     1.81079597E-02    2     1000022   1000035   # BR(S0_5 -> chi_10 chi_40)
     3.38716502E-02    2     1000023   1000023   # BR(S0_5 -> chi_20 chi_20)
     2.59681015E-04    2          13       -13   # BR(S0_5 -> mu- mu+)
     7.34626688E-02    2          15       -15   # BR(S0_5 -> tau- tau+)
     7.13247750E-02    2    -1000024   1000024   # BR(S0_5 -> chi_1- chi_1+)
     2.83978455E-04    2          23        23   # BR(S0_5 -> Z0 Z0)
DECAY   2000016     5.31477381E+00   # P0_1      
#    BR                NDA      ID1      ID2
     2.78905614E-02    2          12   1000022   # BR(P0_1 -> nu_1 chi_10)
     4.99522351E-02    2          12   1000023   # BR(P0_1 -> nu_1 chi_20)
     6.29411745E-04    2          12   1000035   # BR(P0_1 -> nu_1 chi_40)
     4.33560717E-02    2          14   1000022   # BR(P0_1 -> nu_2 chi_10)
     7.76510968E-02    2          14   1000023   # BR(P0_1 -> nu_2 chi_20)
     9.78424935E-04    2          14   1000035   # BR(P0_1 -> nu_2 chi_40)
     7.65354538E-02    2          16   1000022   # BR(P0_1 -> nu_3 chi_10)
     1.37075655E-01    2          16   1000023   # BR(P0_1 -> nu_3 chi_20)
     1.59052596E-04    2          16   1000025   # BR(P0_1 -> nu_3 chi_30)
     1.72719053E-03    2          16   1000035   # BR(P0_1 -> nu_3 chi_40)
     2.89860494E-01    2          15   1000024   # BR(P0_1 -> tau- chi_1+)
     2.89860494E-01    2         -15  -1000024   # BR(P0_1 -> tau+ chi_1-)
     2.08789370E-03    2          15   1000037   # BR(P0_1 -> tau- chi_2+)
     2.08789370E-03    2         -15  -1000037   # BR(P0_1 -> tau+ chi_2-)
DECAY   2000014     5.34768085E+00   # P0_2      
#    BR                NDA      ID1      ID2
     2.36262767E-02    2          12   1000022   # BR(P0_2 -> nu_1 chi_10)
     4.24765640E-02    2          12   1000023   # BR(P0_2 -> nu_1 chi_20)
     5.57421374E-04    2          12   1000035   # BR(P0_2 -> nu_1 chi_40)
     5.31348674E-02    2          14   1000022   # BR(P0_2 -> nu_2 chi_10)
     9.55286620E-02    2          14   1000023   # BR(P0_2 -> nu_2 chi_20)
     1.14553559E-04    2          14   1000025   # BR(P0_2 -> nu_2 chi_30)
     1.25362583E-03    2          14   1000035   # BR(P0_2 -> nu_2 chi_40)
     7.09062407E-02    2          16   1000022   # BR(P0_2 -> nu_3 chi_10)
     1.27478972E-01    2          16   1000023   # BR(P0_2 -> nu_3 chi_20)
     1.52866896E-04    2          16   1000025   # BR(P0_2 -> nu_3 chi_30)
     1.67291083E-03    2          16   1000035   # BR(P0_2 -> nu_3 chi_40)
     2.90190388E-01    2          13   1000024   # BR(P0_2 -> mu- chi_1+)
     2.90190388E-01    2         -13  -1000024   # BR(P0_2 -> mu+ chi_1-)
     1.33265747E-03    2          13   1000037   # BR(P0_2 -> mu- chi_2+)
     1.33265747E-03    2         -13  -1000037   # BR(P0_2 -> mu+ chi_2-)
DECAY   2000012     5.34779553E+00   # P0_3      
#    BR                NDA      ID1      ID2
     9.61719643E-02    2          12   1000022   # BR(P0_3 -> nu_1 chi_10)
     1.72905338E-01    2          12   1000023   # BR(P0_3 -> nu_1 chi_20)
     2.07363859E-04    2          12   1000025   # BR(P0_3 -> nu_1 chi_30)
     2.26936311E-03    2          12   1000035   # BR(P0_3 -> nu_1 chi_40)
     5.12099743E-02    2          14   1000022   # BR(P0_3 -> nu_2 chi_10)
     9.20692219E-02    2          14   1000023   # BR(P0_3 -> nu_2 chi_20)
     1.10417812E-04    2          14   1000025   # BR(P0_3 -> nu_2 chi_30)
     1.20839818E-03    2          14   1000035   # BR(P0_3 -> nu_2 chi_40)
     2.85093156E-04    2          16   1000022   # BR(P0_3 -> nu_3 chi_10)
     5.12562356E-04    2          16   1000023   # BR(P0_3 -> nu_3 chi_20)
     2.90191622E-01    2          11   1000024   # BR(P0_3 -> e- chi_1+)
     2.90191622E-01    2         -11  -1000024   # BR(P0_3 -> e+ chi_1-)
     1.32985271E-03    2          11   1000037   # BR(P0_3 -> e- chi_2+)
     1.32985271E-03    2         -11  -1000037   # BR(P0_3 -> e+ chi_2-)
DECAY        36     2.70491150E+00   # P0_4      
#    BR                NDA      ID1      ID2
     2.19483300E-04    2          -3         3   # BR(P0_4 -> s s)
     3.91898257E-01    2          -5         5   # BR(P0_4 -> b b)
     1.50142835E-01    2          -6         6   # BR(P0_4 -> t t)
     7.81275610E-03    2     1000022   1000022   # BR(P0_4 -> chi_10 chi_10)
     5.15561373E-02    2     1000022   1000023   # BR(P0_4 -> chi_10 chi_20)
     2.21341352E-02    2     1000022   1000025   # BR(P0_4 -> chi_10 chi_30)
     6.96874277E-02    2     1000022   1000035   # BR(P0_4 -> chi_10 chi_40)
     7.97884004E-02    2     1000023   1000023   # BR(P0_4 -> chi_20 chi_20)
     2.12646438E-04    2          13       -13   # BR(P0_4 -> mu- mu+)
     6.01578737E-02    2          15       -15   # BR(P0_4 -> tau- tau+)
     1.65914088E-01    2    -1000024   1000024   # BR(P0_4 -> chi_1- chi_1+)
     4.72706238E-04    2          23        25   # BR(P0_4 -> Z0 S0_1)
DECAY   1000015     2.87498957E+00   # S^-_1     
#    BR                NDA      ID1      ID2
     9.25771924E-01    2          15   1000022   # BR(S^-_1 -> tau- chi_10)
     2.58478075E-02    2          15   1000023   # BR(S^-_1 -> tau- chi_20)
     8.97832502E-04    2          15   1000025   # BR(S^-_1 -> tau- chi_30)
     4.60116593E-04    2          15   1000035   # BR(S^-_1 -> tau- chi_40)
     8.79046004E-03    2    -1000024        12   # BR(S^-_1 -> chi_1- nu_1)
     1.36648571E-02    2    -1000024        14   # BR(S^-_1 -> chi_1- nu_2)
     2.41222057E-02    2    -1000024        16   # BR(S^-_1 -> chi_1- nu_3)
     1.30491355E-04    2    -1000037        14   # BR(S^-_1 -> chi_2- nu_2)
     2.30352848E-04    2    -1000037        16   # BR(S^-_1 -> chi_2- nu_3)
DECAY   2000013     2.76772077E+00   # S^-_2     
#    BR                NDA      ID1      ID2
     9.97856488E-01    2          13   1000022   # BR(S^-_2 -> mu- chi_10)
     1.47646244E-03    2          13   1000023   # BR(S^-_2 -> mu- chi_20)
     1.09370279E-04    2          13   1000025   # BR(S^-_2 -> mu- chi_30)
     3.39960170E-04    2          13   1000035   # BR(S^-_2 -> mu- chi_40)
     1.03664352E-04    2    -1000024        16   # BR(S^-_2 -> chi_1- nu_3)
DECAY   2000011     2.76721851E+00   # S^-_3     
#    BR                NDA      ID1      ID2
     9.98191379E-01    2          11   1000022   # BR(S^-_3 -> e- chi_10)
     1.36292864E-03    2          11   1000023   # BR(S^-_3 -> e- chi_20)
     1.06065647E-04    2          11   1000025   # BR(S^-_3 -> e- chi_30)
     3.39621661E-04    2          11   1000035   # BR(S^-_3 -> e- chi_40)
DECAY   2000015     5.16308455E+00   # S^-_4     
#    BR                NDA      ID1      ID2
     1.46517307E-01    2          15   1000022   # BR(S^-_4 -> tau- chi_10)
     2.95003508E-01    2          15   1000023   # BR(S^-_4 -> tau- chi_20)
     1.55812512E-03    2          15   1000025   # BR(S^-_4 -> tau- chi_30)
     3.34892136E-03    2          15   1000035   # BR(S^-_4 -> tau- chi_40)
     1.02842926E-01    2    -1000024        12   # BR(S^-_4 -> chi_1- nu_1)
     1.59869704E-01    2    -1000024        14   # BR(S^-_4 -> chi_1- nu_2)
     2.82214823E-01    2    -1000024        16   # BR(S^-_4 -> chi_1- nu_3)
     1.63149121E-03    2    -1000037        12   # BR(S^-_4 -> chi_2- nu_1)
     2.53615999E-03    2    -1000037        14   # BR(S^-_4 -> chi_2- nu_2)
     4.47703135E-03    2    -1000037        16   # BR(S^-_4 -> chi_2- nu_3)
DECAY   1000013     5.32314401E+00   # S^-_5     
#    BR                NDA      ID1      ID2
     1.30263277E-01    2          13   1000022   # BR(S^-_5 -> mu- chi_10)
     3.01567419E-01    2          13   1000023   # BR(S^-_5 -> mu- chi_20)
     1.76793834E-03    2          13   1000035   # BR(S^-_5 -> mu- chi_40)
     8.94530978E-02    2    -1000024        12   # BR(S^-_5 -> chi_1- nu_1)
     2.01056275E-01    2    -1000024        14   # BR(S^-_5 -> chi_1- nu_2)
     2.68357220E-01    2    -1000024        16   # BR(S^-_5 -> chi_1- nu_3)
     1.19432618E-03    2    -1000037        12   # BR(S^-_5 -> chi_2- nu_1)
     2.68438850E-03    2    -1000037        14   # BR(S^-_5 -> chi_2- nu_2)
     3.58295090E-03    2    -1000037        16   # BR(S^-_5 -> chi_2- nu_3)
DECAY   1000011     5.32382360E+00   # S^-_6     
#    BR                NDA      ID1      ID2
     1.30191356E-01    2          11   1000022   # BR(S^-_6 -> e- chi_10)
     3.01595871E-01    2          11   1000023   # BR(S^-_6 -> e- chi_20)
     1.76202878E-03    2          11   1000035   # BR(S^-_6 -> e- chi_40)
     3.63978550E-01    2    -1000024        12   # BR(S^-_6 -> chi_1- nu_1)
     1.93871732E-01    2    -1000024        14   # BR(S^-_6 -> chi_1- nu_2)
     1.07568110E-03    2    -1000024        16   # BR(S^-_6 -> chi_1- nu_3)
     4.85609992E-03    2    -1000037        12   # BR(S^-_6 -> chi_2- nu_1)
     2.58658338E-03    2    -1000037        14   # BR(S^-_6 -> chi_2- nu_2)
DECAY       -37     5.69643942E-01   # S^-_7     
#    BR                NDA      ID1      ID2
     1.62238567E-04    2          13        12   # BR(S^-_7 -> mu- nu_1)
     3.65036473E-04    2          13        14   # BR(S^-_7 -> mu- nu_2)
     4.87049100E-04    2          13        16   # BR(S^-_7 -> mu- nu_3)
     5.41559738E-02    2          15        12   # BR(S^-_7 -> tau- nu_1)
     8.41860036E-02    2          15        14   # BR(S^-_7 -> tau- nu_2)
     1.48611283E-01    2          15        16   # BR(S^-_7 -> tau- nu_3)
     3.52312298E-01    2    -1000024   1000022   # BR(S^-_7 -> chi_1- chi_10)
     3.57414532E-01    2    -1000037   1000022   # BR(S^-_7 -> chi_2- chi_10)
Block SPhenoLowEnergy  # low energy observables
    1    3.78211823E-04   # BR(b -> s gamma)
    2    1.75134248E-06   # BR(b -> s mu+ mu-)
    3    4.38966035E-05   # BR(b -> s nu nu)
    4    4.62251196E-09   # BR(Bs -> mu+ mu-)
    5    7.87030319E-05   # BR(B_u -> tau nu)
    6    2.36191751E-01   # |Delta(M_Bd)| [ps^-1] 
    7    1.70717576E+01   # |Delta(M_Bs)| [ps^-1] 
   10    1.07029735E-14   # Delta(g-2)_electron
   11    5.00603464E-10   # Delta(g-2)_muon
   12    1.33937170E-07   # Delta(g-2)_tau
   13    0.00000000E+00   # electric dipole moment of the electron
   14    0.00000000E+00   # electric dipole moment of the muon
   15    0.00000000E+00   # electric dipole moment of the tau
   16    0.00000000E+00   # Br(mu -> e gamma)
   17    0.00000000E+00   # Br(tau -> e gamma)
   18    0.00000000E+00   # Br(tau -> mu gamma)
   19    0.00000000E+00   # Br(mu -> 3 e)
   20    0.00000000E+00   # Br(tau -> 3 e)
   21    0.00000000E+00   # Br(tau -> 3 mu)
   30    1.08454575E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
