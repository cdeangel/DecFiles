#!/usr/bin/env python

from __future__ import print_function

import sys
from io import open

alias = {}
chargeConj = {}

with open(sys.argv[1], 'r', encoding='latin-1' ) as ff:
  for line in ff:
    if 'Alias' in line:
      elements = line.partition("Alias")[2].strip().split()
      alias[elements[0].strip()] = elements[1].strip()
    if 'ChargeConj' in line:
      elements = line.partition("ChargeConj")[2].strip().split()
      chargeConj[elements[0].strip()] = elements[1].strip()

#print(alias, chargeConj)
for key, value in chargeConj.items():
  if not value in alias.keys():
    print('Problem with charge conjugation of {0} in file {1}'.format(key, sys.argv[1]))


