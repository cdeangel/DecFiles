import sys, os

logfile = os.path.dirname(os.path.abspath(sys.argv[0])) + "/decparse.log"
enable_colours = True
overwrite = True
dkfilespath = os.path.dirname(os.path.abspath(sys.argv[0])) + "/../../dkfiles/"
obsoletepath = os.path.dirname(os.path.abspath(sys.argv[0])) + "/../../doc/"
partdictpath = os.path.dirname(os.path.abspath(
    sys.argv[0])) + "/particle_dictionary"

use_url = True
dec_url = "http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/"
obs_url = "https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles/raw/master/doc/table_obsolete.sql"
#cuts_url = "https://gitlab.cern.ch/lhcb/Gauss/tree/Sim09/Gen/GenCuts/src"
cuts_url = 'https://gitlab.cern.ch/api/v4/projects/2606/repository/tree?path=Gen%2FGenCuts%2Fsrc&ref=Sim09&per_page=500'
#table_url= "http://svn.cern.ch/guest/evtgen/tags/R01-01-00/evt.pdl"
table_url = "http://www2.warwick.ac.uk/fac/sci/physics/staff/academic/kreps/tmp/evt.pdl"

groups = [
    "Charm", "RD", "Exotica", "Onia", "QCD", "B2HQ", "B2Ch", "B2OC", "BnoC",
    "B2SL", "Calo", "IFT", "PID", "Alignment", "Tagging", "Sim"
]

groups_obsolete = [
    "Charm",
    "Exotics",
    "All",
    "EW",
    "GWT",
    "P&S",
    "RD",
    "GWL",
    "Semileptonics",
    "Calo",
    "BetaS",
    "BnoC",
]

terminators = [
    "B_TO_2BARYON_SCALAR", "B_TO_LAMBDA_PBAR_GAMMA", "FLATQ2", "TAUHADNU",
    "TAUVECTORNU", "VVP", "SLN", "ISGW2", "MELIKHOV", "SLPOLE", "PROPSLPOLE",
    "SLBKPOLE", "HQET", "HQET2", "ISGW", "BHADRONIC", "VSS", "VSS_MIX",
    "VSS_BMIX", "VSP_PWAVE", "GOITY_ROBERTS", "SVS", "TSS", "TVS_PWAVE",
    "SVV_HELAMP", "SVP_HELAMP", "SVP_CP", "VVS_PWAVE", "D_DALITZ",
    "LAMBDAC_PHH", "OMEGA_DALITZ", "ETA_DALITZ", "PHSP", "PHSPDECAYTIMECUT",
    "BTOXSGAMMA", "BTOXSLL", "BTOXSETAP", "SSS_CP", "SSS_CP_PNG", "STS_CP",
    "STS", "SSS_CPT", "SVS_CP", "SSD_CP", "SVS_NONCPEIGEN", "SVV_NONCPEIGEN",
    "SVV_CP", "SVV_CPLH", "SVS_CPLH", "SLL", "VLL", "TAULNUNU", "TAUSCALARNU",
    "KSTARNUNU", "BTOSLLBALL", "BTO2PI_CP_ISO", "BTOKPI_CP_ISO", "SVS_CP_ISO",
    "SINGLE", "VECTORISR", "PI0_DALITZ", "HELAMP", "PARTWAVE", "VVPIPI",
    "Y3STOY1SPIPIMOXHAY", "YMSTOYNSPIPICLEO", "BSQUARK", "PHI_DALITZ",
    "BTOPLNUBK", "BTOVLNUBALL", "VVPIPI_WEIGHTED", "VPHOTOVISRHI", "BTO4PI_CP",
    "BTO3PI_CP", "CB3PI-P00", "CB3PI-MPP", "BTOKPIPI_CP", "Lb2Lll",
    "RareLbToLll", "HypNonLepton", "SVVHELCPMIX", "SVPHELCPMIX", "LNUGAMMA",
    "KSTARSTARGAMMA", "VUB", "VUBHYBRID", "VUB_NLO", "VUB_BLNP",
    "VUB_BLNPHYBRID", "BTOKD3P", "KK_LAMBDAC_SL", "D_MULTIBODY", "DMIX",
    "D0GAMMADALITZ", "ETA2MUMUGAMMA", "BTOSLLALI", "BaryonPCR",
    "BTODDALITZCPK", "LAMBDAB2LAMBDAV", "PVV_CPLH", "SSD_DirectCP",
    "BC_PSI_NPI", "BC_BS_NPI", "BC_BSSTAR_NPI", "BC_SMN", "BC_VMN", "BC_TMN",
    "BC_VNPI", "SVP", "TVP", "X38722-+_PSI_GAMMA", "PYTHIA", "TAUOLA",
    "VTOSLL", "BToDiBaryonlnupQCD", "BC_VHAD", "BSTOGLLMNT", "BSTOGLLISRFSR",
    "BTOSLLMS", "BTOSLLMSEXT", "BQTOLLLL", "BQTOLLLLHYPERCP", "BUTOMMMN",
    "BLLNUL", "Lb2plnuLQCD", "Lb2plnuLCSR", "Lb2Baryonlnu", "FLATSQDALITZ",
    "PHSPFLATLIFETIME", "DToKpienu", "PSI2JPSIPIPI", "THREEBODYPHSP",
    "BNOCBPTO3HPI0", "PHOTOS", "LbAmpGen", "FOURBODYPHSP"
]

longlived = [
    "pi+",
    "pi-",
    "K+",
    "K-",
    "K_L0",
    "p+",
    "anti-p-",
    "n0",
    "anti-n0",
    "e+",
    "e-",
    "mu+",
    "mu-",
]

nickslation = {"B0": "Bd", "B+": "Bu", "D*+": "Dst"}

#Obsolete
descripslation = {
    "D*+": "D*(2010)+",
    #"K_S0" : "KS0"
}
